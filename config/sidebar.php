<?php

return [

    /*
    |--------------------------------------------------------------------------
    | View Storage Paths
    |--------------------------------------------------------------------------
    |
    | Most templating systems load templates from disk. Here you may specify
    | an array of paths that should be checked for your views. Of course
    | the usual Laravel view path has already been registered for you.
    |
    */

    'admin_menu' => [
    	[
			'icon' => 'fa fa-th-large',
			'title' => 'Инфоблок',
			'url' => '/dashboard',
		], 
    	[
			'icon' => 'fa fa-user',
			'title' => 'Пользователи',
			'url' => '/dashboard/users',
			'caret' => true,
			'sub_menu' => [
				[
					'url' => '/dashboard/users/create',
					'title' => 'Добавить пользователя'
				]
			],
		]
    ],

    'teacher_menu' => [
    	[
			'icon' => 'fa fa-th-large',
			'title' => 'Инфоблок',
			'url' => '/dashboard',
		]
    ],

    'student_menu' => [
    	[
			'icon' => 'fa fa-th-large',
			'title' => 'Инфоблок',
			'url' => '/dashboard',
		]
    ],

  	'menu' => [
  		[
			'icon' => 'fa fa-th-large',
			'title' => 'Dashboard',
			'url' => 'javascript:;',
			'caret' => true,
			'sub_menu' => [[
				'url' => '/demo/dashboard/v1',
				'title' => 'Dashboard v1'
			],[
				'url' => '/demo/dashboard/v2',
				'title' => 'Dashboard v2'
			],[
				'url' => '/demo/dashboard/v3',
				'title' => 'Dashboard v3'
			]]
		],[
			'icon' => 'fa fa-hdd',
			'title' => 'Email',
			'url' => 'javascript:;',
			'badge' => '10',
			'sub_menu' => [[
				'url' => '/demo/email/inbox',
				'title' => 'Inbox'
			],[
				'url' => '/demo/email/compose',
				'title' => 'Compose'
			],[
				'url' => '/demo/email/detail',
				'title' => 'Detail'
			]]
		],[
			'icon' => 'fab fa-simplybuilt',
			'title' => 'Widgets',
			'label' => 'NEW',
			'url' => '/demo/widget'
		],[
			'icon' => 'fa fa-gem',
			'title' => 'UI Elements',
			'url' => 'javascript:;',
			'label' => 'NEW',
			'caret' => true,
			'sub_menu' => [[
				'url' => '/demo/ui/general',
				'title' => 'General',
				'highlight' => true
			],[
				'url' => '/demo/ui/typography',
				'title' => 'Typography'
			],[
				'url' => '/demo/ui/tabs-accordions',
				'title' => 'Tabs & Accordions'
			],[
				'url' => '/demo/ui/unlimited-nav-tabs',
				'title' => 'Unlimited Nav Tabs'
			],[
				'url' => '/demo/ui/modal-notification',
				'title' => 'Modal & Notification',
				'highlight' => true
			],[
				'url' => '/demo/ui/widget-boxes',
				'title' => 'Widget Boxes'
			],[
				'url' => '/demo/ui/media-object',
				'title' => 'Media Object'
			],[
				'url' => '/demo/ui/buttons',
				'title' => 'Buttons',
				'highlight' => true
			],[
				'url' => '/demo/ui/icons',
				'title' => 'Icons'
			],[
				'url' => '/demo/ui/simple-line-icons',
				'title' => 'Simple Line Ioncs'
			],[
				'url' => '/demo/ui/ionicons',
				'title' => 'Ionicons'
			],[
				'url' => '/demo/ui/tree-view',
				'title' => 'Tree View'
			],[
				'url' => '/demo/ui/language-bar-icon',
				'title' => 'Language Bar & Icon'
			],[
				'url' => '/demo/ui/social-buttons',
				'title' => 'Social Buttons'
			],[
				'url' => '/demo/ui/intro-js',
				'title' => 'Intro JS'
			]]
		],[
			'img' => '/assets/img/logo/logo-bs4.png',
			'title' => 'Bootstrap 4',
			'url' => '/demo/bootstrap-4',
			'label' => 'NEW'
		],[
			'icon' => 'fa fa-list-ol',
			'title' => 'Form Stuff',
			'url' => 'javascript:;',
			'label' => 'NEW',
			'caret' => true,
			'sub_menu' => [[
				'url' => '/demo/form/elements',
				'title' => 'Form Elements',
				'highlight' => true
			],[
				'url' => '/demo/form/plugins',
				'title' => 'Form Plugins',
				'highlight' => true
			],[
				'url' => '/demo/form/slider-switcher',
				'title' => 'Form Slider + Switcher'
			],[
				'url' => '/demo/form/validation',
				'title' => 'Form Validation'
			],[
				'url' => '/demo/form/wizards',
				'title' => 'Wizards'
			],[
				'url' => '/demo/form/wizards-validation',
				'title' => 'Wizards + Validation'
			],[
				'url' => '/demo/form/wysiwyg',
				'title' => 'WYSIWYG'
			],[
				'url' => '/demo/form/x-editable',
				'title' => 'X-Editable'
			],[
				'url' => '/demo/form/multiple-file-upload',
				'title' => 'Multiple File Upload'
			],[
				'url' => '/demo/form/summernote',
				'title' => 'Summernote'
			],[
				'url' => '/demo/form/dropzone',
				'title' => 'Dropzone'
			]]
		],[
			'icon' => 'fa fa-table',
			'title' => 'Tables',
			'url' => 'javascript:;',
			'caret' => true,
			'sub_menu' => [[
				'url' => '/demo/table/basic',
				'title' => 'Basic'
			],[
				'url' => 'javascript:;',
				'title' => 'Managed Tables',
				'sub_menu' => [[
					'url' => '/demo/table/manage/default',
					'title' => 'Default'
				],[
					'url' => '/demo/table/manage/autofill',
					'title' => 'Autofill'
				],[
					'url' => '/demo/table/manage/buttons',
					'title' => 'Buttons'
				],[
					'url' => '/demo/table/manage/colreorder',
					'title' => 'ColReorder'
				],[
					'url' => '/demo/table/manage/fixed-column',
					'title' => 'Fixed Column'
				],[
					'url' => '/demo/table/manage/fixed-header',
					'title' => 'Fixed Header'
				],[
					'url' => '/demo/table/manage/keytable',
					'title' => 'KeyTable'
				],[
					'url' => '/demo/table/manage/responsive',
					'title' => 'Responsive'
				],[
					'url' => '/demo/table/manage/rowreorder',
					'title' => 'RowReorder'
				],[
					'url' => '/demo/table/manage/scroller',
					'title' => 'Scroller'
				],[
					'url' => '/demo/table/manage/select',
					'title' => 'Select'
				],[
					'url' => '/demo/table/manage/combine',
					'title' => 'Extension Combination'
				]]
			]]
		],[
			'icon' => 'fa fa-star',
			'title' => 'Frontend',
			'url' => 'javascript:;',
			'caret' => true,
			'sub_menu' => [[
				'url' => 'javascript:;',
				'title' => 'One Page Parallax'
			],[
				'url' => 'javascript:;',
				'title' => 'Blog'
			],[
				'url' => 'javascript:;',
				'title' => 'Forum'
			],[
				'url' => 'javascript:;',
				'title' => 'E-Commerce'
			]]
		],[
			'icon' => 'fa fa-envelope',
			'title' => 'Email Template',
			'url' => 'javascript:;',
			'caret' => true,
			'sub_menu' => [[
				'url' => '/demo/email-template/system',
				'title' => 'System Template'
			],[
				'url' => '/demo/email-template/newsletter',
				'title' => 'Newsletter Template'
			]]
		],[
			'icon' => 'fa fa-chart-pie',
			'title' => 'Chart',
			'url' => 'javascript:;',
			'label' => 'NEW',
			'caret' => true,
			'sub_menu' => [[
				'url' => '/demo/chart/flot',
				'title' => 'Flot Chart'
			],[
				'url' => '/demo/chart/morris',
				'title' => 'Morris Chart'
			],[
				'url' => '/demo/chart/js',
				'title' => 'Chart JS'
			],[
				'url' => '/demo/chart/d3',
				'title' => 'd3 Chart'
			],[
				'url' => '/demo/chart/apex',
				'title' => 'Apex Chart',
				'highlight' => true
			]]
		],[
			'icon' => 'fa fa-calendar',
			'title' => 'Calendar',
			'url' => '/demo/calendar'
		],[
			'icon' => 'fa fa-map',
			'title' => 'Map',
			'url' => 'javascript:;',
			'caret' => true,
			'sub_menu' => [[
				'url' => '/demo/map/vector',
				'title' => 'Vector Map'
			],[
				'url' => '/demo/map/google',
				'title' => 'Google Map'
			]]
		],[
			'icon' => 'fa fa-image',
			'title' => 'Gallery',
			'url' => 'javascript:;',
			'caret' => true,
			'sub_menu' => [[
				'url' => '/demo/gallery/v1',
				'title' => 'Gallery v1'
			],[
				'url' => '/demo/gallery/v2',
				'title' => 'Gallery v2'
			]]
		],[
			'icon' => 'fa fa-cogs',
			'title' => 'Page Options',
			'url' => 'javascript:;',
			'label' => 'NEW',
			'caret' => true,
			'sub_menu' => [[
				'url' => '/demo/page-option/page-blank',
				'title' => 'Blank Page'
			],[
				'url' => '/demo/page-option/page-with-footer',
				'title' => 'Page with Footer'
			],[
				'url' => '/demo/page-option/page-without-sidebar',
				'title' => 'Page without Sidebar'
			],[
				'url' => '/demo/page-option/page-with-right-sidebar',
				'title' => 'Page with Right Sidebar'
			],[
				'url' => '/demo/page-option/page-with-minified-sidebar',
				'title' => 'Page with Minified Sidebar'
			],[
				'url' => '/demo/page-option/page-with-two-sidebar',
				'title' => 'Page with Two Sidebar'
			],[
				'url' => '/demo/page-option/page-full-height',
				'title' => 'Full Height Content'
			],[
				'url' => '/demo/page-option/page-with-wide-sidebar',
				'title' => 'Page with Wide Sidebar'
			],[
				'url' => '/demo/page-option/page-with-light-sidebar',
				'title' => 'Page with Light Sidebar'
			],[
				'url' => '/demo/page-option/page-with-mega-menu',
				'title' => 'Page with Mega Menu'
			],[
				'url' => '/demo/page-option/page-with-top-menu',
				'title' => 'Page with Top Menu'
			],[
				'url' => '/demo/page-option/page-with-boxed-layout',
				'title' => 'Page with Boxed Layout'
			],[
				'url' => '/demo/page-option/page-with-mixed-menu',
				'title' => 'Page with Mixed Menu'
			],[
				'url' => '/demo/page-option/boxed-layout-with-mixed-menu',
				'title' => 'Boxed Layout with Mixed Menu'
			],[
				'url' => '/demo/page-option/page-with-transparent-sidebar',
				'title' => 'Page with Transparent Sidebar'
			],[
				'url' => '/demo/page-option/page-with-search-sidebar',
				'title' => 'Page with Search Sidebar',
				'highlight' => true
			]]
		],[
			'icon' => 'fa fa-gift',
			'title' => 'Extra',
			'url' => 'javascript:;',
			'label' => 'NEW',
			'caret' => true,
			'sub_menu' => [[
				'url' => '/demo/extra/timeline',
				'title' => 'Timeline'
			],[
				'url' => '/demo/extra/coming-soon',
				'title' => 'Coming Soon Page'
			],[
				'url' => '/demo/extra/search-result',
				'title' => 'Search Results'
			],[
				'url' => '/demo/extra/invoice',
				'title' => 'Invoice'
			],[
				'url' => '/demo/extra/error-page',
				'title' => '404 Error Page'
			],[
				'url' => '/demo/extra/profile',
				'title' => 'Profile Page'
			],[
				'url' => '/demo/extra/scrum-board',
				'title' => 'Scrum Board',
				'highlight' => true
			],[
				'url' => '/demo/extra/cookie-acceptance-banner',
				'title' => 'Cookie Acceptance Banner',
				'highlight' => true
			]]
		],[
			'icon' => 'fa fa-key',
			'title' => 'Login & Register',
			'url' => 'javascript:;',
			'caret' => true,
			'sub_menu' => [[
				'url' => '/demo/login/v1',
				'title' => 'Login'
			],[
				'url' => '/demo/login/v2',
				'title' => 'Login v2'
			],[
				'url' => '/demo/login/v3',
				'title' => 'Login v3'
			],[
				'url' => '/demo/register/v3',
				'title' => 'Register v3'
			]]
		],[
			'icon' => 'fa fa-cube',
			'title' => 'Version',
			'url' => 'javascript:;',
			'label' => 'NEW',
			'caret' => true,
			'sub_menu' => [[
				'url' => 'javascript:;',
				'title' => 'HTML'
			],[
				'url' => 'javascript:;',
				'title' => 'AJAX'
			],[
				'url' => 'javascript:;',
				'title' => 'ANGULAR JS'
			],[
				'url' => 'javascript:;',
				'title' => 'ANGULAR JS 5'
			],[
				'url' => 'javascript:;',
				'title' => 'LARAVEL'
			],[
				'url' => 'javascript:;',
				'title' => 'MATERIAL DESIGN'
			],[
				'url' => 'javascript:;',
				'title' => 'APPLE DESIGN',
				'highlight' => true
			],[
				'url' => 'javascript:;',
				'title' => 'TRANSPARENT DESIGN',
				'highlight' => true
			],[
				'url' => 'javascript:;',
				'title' => 'FACEBOOK DESIGN',
				'highlight' => true
			],[
				'url' => 'javascript:;',
				'title' => 'GOOGLE DESIGN',
				'highlight' => true
			]]
		],[
			'icon' => 'fa fa-medkit',
			'title' => 'Helper',
			'url' => 'javascript:;',
			'caret' => true,
			'sub_menu' => [[
				'url' => '/demo/helper/css',
				'title' => 'Predefined CSS Classes'
			]]
		],[
			'icon' => 'fa fa-align-left',
			'title' => 'Menu Level',
			'url' => 'javascript:;',
			'caret' => true,
			'sub_menu' => [[
				'url' => 'javascript:;',
				'title' => 'Menu 1.1',
				'sub_menu' => [[
					'url' => 'javascript:;',
					'title' => 'Menu 2.1',
					'sub_menu' => [[
						'url' => 'javascript:;',
						'title' => 'Menu 3.1',
					],[
						'url' => 'javascript:;',
						'title' => 'Menu 3.2'
					]]
				],[
					'url' => 'javascript:;',
					'title' => 'Menu 2.2'
				],[
					'url' => 'javascript:;',
					'title' => 'Menu 2.3'
				]]
			],[
				'url' => 'javascript:;',
				'title' => 'Menu 1.2'
			],[
				'url' => 'javascript:;',
				'title' => 'Menu 1.3'
			]]
		]
	]
];
