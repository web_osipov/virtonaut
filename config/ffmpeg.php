<?php

return [

    'path' => env('FFMPEG_PATH', '/var/packages/ffmpeg/target/bin/ffmpeg'),

    'transport' => env('FFMPEG_TRANSPORT', '-rtsp_transport tcp'),

    'stream' => [
        'acodec' => env('FFMPEG_STREAM_ACODEC', 'aac'),
        'vcodec' => env('FFMPEG_STREAM_ACODEC', 'copy'),
    ],

    'record' => [
        'acodec' => env('FFMPEG_RECORD_ACODEC', 'copy'),
        'vcodec' => env('FFMPEG_RECORD_ACODEC', 'copy'),
    ],

];