<!-- begin #sidebar -->
@php
$disign_settings = App\DesignSetting::all()->pluck('value', 'type')->toArray();
@endphp
<div id="sidebar" class="sidebar">
	<!-- begin sidebar scrollbar -->
	<div data-scrollbar="true" data-height="100%">
		<!-- begin sidebar user -->
		<ul class="nav">
			<li class="nav-profile">
				<a href="javascript:;" data-toggle="nav-profile">
					<div class="@if(!empty($disign_settings['nav_bg'])) cover @endif with-shadow" @if(!empty($disign_settings['nav_bg'])) style="background-image: url(/storage/{{ $disign_settings['nav_bg'] }})" @endif></div>
					<div class="image">
						<img src="{{ Auth::user()->icon_url }}" alt="" />
					</div>
					<div class="info">
						{{ Auth::user()->name }}
						@if(Auth::user()->institution)
						    <small>{{ Auth::user()->institution->short_name }}</small>
						@endif
					</div>
				</a>
			</li>
		</ul>
		<!-- end sidebar user -->
		<!-- begin sidebar nav -->
		<ul class="nav">
			<li class="nav-header">{{ __('Навигация') }}</li>
			<x-sidebar />
			<!-- begin sidebar minify button -->
			<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
			<!-- end sidebar minify button -->
		</ul>
		<!-- end sidebar nav -->
	</div>
	<!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>
<!-- end #sidebar -->

