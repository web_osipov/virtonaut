<!-- ================== BEGIN BASE JS ================== -->
<script src="/assets/js/vendor.min.js"></script>
<script src="/assets/js/app.min.js?ver={{ config('app.version') }}"></script>
<script src="/assets/js/theme/default.min.js"></script>
<script src="/assets/plugins/gritter/js/jquery.gritter.js"></script>
<script src="/assets/plugins/sweetalert/dist/sweetalert.min.js"></script>
<!-- ================== END BASE JS ================== -->


@if (session('license_expired'))
	<script>
		$(document).ready(function() {
			swal({
				title: '{{ __('Истек срок лицензии') }}',
				text: 'У вас истек срок лицензии "{!! session('license_expired')['name'] !!}"!',
				icon: 'error',
				buttons: {
					cancel: {
						text: 'Закрыть',
						value: null,
						visible: true,
						closeModal: true,
					},
					confirm: {
						text: 'Подробнее',
						value: true,
						visible: true,
						closeModal: false
					}
				}
			}).then(more => {
				if (!more) return;

				window.location.href = "{{ route('license.index') }}";
			});
		});
	</script>
@endif

@stack('scripts')