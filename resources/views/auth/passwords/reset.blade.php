@extends('layouts.empty', ['paceTop' => true])

@section('title', __('Восстановление пароля'))

@section('content')
	<!-- begin login-cover -->
	<div class="login-cover">
		<div class="login-cover-image" style="background-image: url(/storage/img/login-bg/login-bg-17.jpg)" data-id="login-cover-image"></div>
		<div class="login-cover-bg"></div>
	</div>
	<!-- end login-cover -->
	
	<!-- begin login -->
	<div class="login login-v2" data-pageload-addclass="animated fadeIn">
		<!-- begin brand -->
		<div class="login-header">
			<div class="brand">
				<span class="logo"></span> <b>Color</b> Admin
			</div>
			<div class="icon">
				<i class="fa fa-lock"></i>
			</div>
		</div>
		<!-- end brand -->
		<!-- begin login-content -->
		<div class="login-content">
			@if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
			<form action="{{ route('password.update') }}" method="POST" class="margin-bottom-0">
				@csrf

				<input type="hidden" name="token" value="{{ $token }}">
				
				<div class="form-group m-b-20">
					<input type="text" id="email" name="email" class="form-control form-control-lg @error('email') is-invalid @enderror" value="{{ $email ?? old('email') }}" placeholder="{{ __('E-Mail') }}" required autofocus/>
					@error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
				</div>

				<div class="form-group m-b-20">
					<input type="password" id="password" name="password" class="form-control form-control-lg @error('password') is-invalid @enderror" placeholder="{{ __('Пароль') }}" required autocomplete="new-password"/>
					@error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
				</div>

				<div class="form-group m-b-20">
					<input type="password" id="password-confirm" name="password_confirmation" class="form-control form-control-lg @error('password') is-invalid @enderror" placeholder="{{ __('Подтверждение пароля') }}" required autocomplete="new-password"/>
				</div>

				<div class="login-buttons">
					<button type="submit" class="btn btn-success btn-block btn-lg">{{ __('Изменить пароль') }}</button>
				</div>
				<div class="m-t-20">
					<a href="{{ route('login') }}">{{ __('Войти?') }}</a>
				</div>
			</form>
		</div>
		<!-- end login-content -->
	</div>
	<!-- end login -->
	
	<!-- begin login-bg -->
	<!--
	<ul class="login-bg-list clearfix">
		<li class="active"><a href="javascript:;" data-click="change-bg" data-img="/assets/img/login-bg/login-bg-17.jpg" style="background-image: url(/assets/img/login-bg/login-bg-17.jpg)"></a></li>
		<li><a href="javascript:;" data-click="change-bg" data-img="/assets/img/login-bg/login-bg-16.jpg" style="background-image: url(/assets/img/login-bg/login-bg-16.jpg)"></a></li>
		<li><a href="javascript:;" data-click="change-bg" data-img="/assets/img/login-bg/login-bg-15.jpg" style="background-image: url(/assets/img/login-bg/login-bg-15.jpg)"></a></li>
		<li><a href="javascript:;" data-click="change-bg" data-img="/assets/img/login-bg/login-bg-14.jpg" style="background-image: url(/assets/img/login-bg/login-bg-14.jpg)"></a></li>
		<li><a href="javascript:;" data-click="change-bg" data-img="/assets/img/login-bg/login-bg-13.jpg" style="background-image: url(/assets/img/login-bg/login-bg-13.jpg)"></a></li>
		<li><a href="javascript:;" data-click="change-bg" data-img="/assets/img/login-bg/login-bg-12.jpg" style="background-image: url(/assets/img/login-bg/login-bg-12.jpg)"></a></li>
	</ul>
-->
	<!-- end login-bg -->
@endsection

@push('scripts')
	<!-- <script src="/assets/js/demo/login-v2.demo.js"></script>-->
@endpush
