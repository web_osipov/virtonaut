@extends('layouts.empty', ['paceTop' => true])

@section('title', __('Вход'))

@section('content')
	<!-- begin login-cover -->
	<div class="login-cover">
		<div class="login-cover-image" style="background-image: url(/images/login-bg/login-bg-15.jpg)" data-id="login-cover-image"></div>
		<div class="login-cover-bg"></div>
	</div>
	<!-- end login-cover -->
	
	<!-- begin login -->
	<div class="login login-v2" data-pageload-addclass="animated fadeIn">
		<!-- begin brand -->
		<div class="team">
			<div class="image">
				<img src="/images/login-bg/Artboard.png" alt="#" />
			</div>
		</div>
		<!-- end brand -->
		<!-- begin login-content -->
		<div class="login-content">
			<form action="{{ route('login') }}" method="POST" class="margin-bottom-0">
				@csrf
				<div class="form-group m-b-20">
					<input type="text" id="email" name="email" class="form-control form-control-lg @error('email') is-invalid @enderror" value="{{ old('email') }}" placeholder="{{ __('Ваш логин') }}" required autofocus/>
					@error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
				</div>
				<div class="form-group m-b-20">
					<input type="password" id="password" name="password" class="form-control form-control-lg @error('password') is-invalid @enderror" placeholder="{{ __('Ваш пароль') }}" required autocomplete="current-password"/>
					@error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
				</div>
				<!--
				<div class="checkbox checkbox-css m-b-20">
					<input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }} /> 
					<label for="remember">
						{{ __('Запомнить меня') }}
					</label>
				</div>
				-->
				<div class="login-buttons">
					<button type="submit" class="btn btn-success btn-block btn-lg">{{ __('Войти') }}</button>
				</div>
				<div class="m-t-20">
					Вернуться на <a href="https://virtonaut.ru">главную</a> страницу.
				</div>
				@if (Route::has('password.request'))
					<div class="m-t-20">
						<a href="{{ route('password.request') }}">{{ __('Забыли пароль?') }}</a>
					</div>
               	@endif
			</form>
		</div>
		<!-- end login-content -->
	</div>
	<!-- end login -->
	
	<!-- begin login-bg -->

	<ul class="login-bg-list clearfix">
		<li class="active">
			<a href="javascript:;" data-click="change-bg" data-img="/images/login-bg/login-bg-15.jpg" style="background-image: url(/images/login-bg/login-bg-15.jpg)"></a>
		</li>
		<li>
			<a href="javascript:;" data-click="change-bg" data-img="/images/login-bg/login-bg-16.jpg" style="background-image: url(/images/login-bg/login-bg-16.jpg)"></a>
		</li>
	</ul>
	<!-- end login-bg -->
@endsection

@push('scripts')
	<!-- <script src="/assets/js/demo/login-v2.demo.js"></script>-->
@endpush
