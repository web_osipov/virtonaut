<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	@include('includes.head')
</head>
@php
	$pageContainerClass = (!empty($contentFullHeight)) ? 'page-content-full-height ' : '';
	
	$contentClass = (!empty($contentFullHeight)) ? 'content-full-width ' : '';
@endphp
<body>
	@include('includes.component.page-loader')
	
	<div id="page-container" class="page-container fade page-sidebar-fixed page-header-fixed {{ $pageContainerClass }}">
		
		@include('includes.header')
		
		@include('includes.sidebar')
		
		<div id="content" class="content {{ $contentClass }}">
			@if (session('success'))
			    <div class="alert alert-success fade show">
			        {{ session('success') }}
			    </div>
			@endif

			@if (session('danger'))
			    <div class="alert alert-danger fade show">
			        {{ session('danger') }}
			    </div>
			@endif

			@yield('content')
		</div>
		
		@include('includes.component.scroll-top-btn')
		
	</div>
	
	@include('includes.page-js')
</body>
</html>
