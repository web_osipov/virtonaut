<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	@include('includes.head')
</head>
<body>
	@include('includes.component.page-loader')
	
	@yield('content')
			
	@include('includes.page-js')
</body>
</html>
