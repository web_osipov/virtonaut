@extends('layouts.default')

@section('title', __('Удаленное обучение'))

@push('css')
	<link href="/assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
	<link href="/assets/plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" />

	<link href="/assets/plugins/jvectormap-next/jquery-jvectormap.css" rel="stylesheet" />
	<link href="/assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.css" rel="stylesheet" />
@endpush

@section('content')
	
	<ol class="breadcrumb float-xl-right">
		<li class="breadcrumb-item">{{ __('Личный кабинет') }}</li>
		<li class="breadcrumb-item active">{{ __('Удаленное обучение') }}</li>
	</ol>
	
	<h1 class="page-header">{{ __('Дистанционное обучение') }} <small>{{ __('Работа с камерами') }}</small></h1>
	
	<div class="row">
		<div class="col-xl-12">

			@group('teacher')

				@include('dashboard.remotelearning.onlinerecording')

			@elsegroup('student')

				@include('dashboard.remotelearning.onlinelesson')

			@endgroup

			<div class="panel panel-inverse" data-sortable-id="index-1">
				<div class="panel-heading">
					<h4 class="panel-title">Сохраненные записи</h4>
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
				</div>
				<div class="panel-body">
					<div class="row records-lists">
						<div class="col-md-3 my-3">
							<div class="records-date datepicker-full-width overflow-y-scroll position-relative">
							</div>
						</div>
						<div class="col-md-9 my-3">
							<div class="records-table table-responsive"></div>
						</div>
					</div>
				</div>
			</div>

			<div class="modal fade" id="recordModal" record-link="{{ route('remotelearning.record') }}">
				<div class="modal-dialog" style="max-width: 800px;">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">{{ __('Запись') }}</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Закрыть') }}</button>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>


@endsection

@push('scripts')
	<script src="/assets/plugins/moment/moment.js"></script>	
	<script src="/assets/plugins/moment/locale/ru.js"></script>	
	<script src="/assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>

	<script src="/assets/plugins/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="/assets/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
	<script src="/assets/plugins/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
	<script src="/assets/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
	<script src="/assets/plugins/datatables.net-autofill/js/dataTables.autofill.min.js"></script>
	<script src="/assets/plugins/datatables.net-autofill-bs4/js/autofill.bootstrap4.min.js"></script>

	<script src="/assets/js/remotelearning.min.js?ver={{ config('app.version') }}"></script>
@endpush
