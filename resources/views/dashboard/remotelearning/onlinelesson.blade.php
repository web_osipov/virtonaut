<div class="panel panel-inverse" data-sortable-id="index-2">
	<div class="panel-heading">
		<h4 class="panel-title">Онлайн урок</h4>
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
		</div>
	</div>
	<div class="panel-body">
		@if(!empty($live))
			@if(!empty($live->topic))
			<div class="text-center">{{ $live->topic }}</div>
			@endif
			<div class="online-recording">
				<div class="live-view" id="live-view" set-control-text="Урок идет" lesson-id="{{ $live->id }}" status-url="{{ route('remotelearning.status') }}">
					<div class="player-controlls text-center mb-3">
						<div class="btn-group">
							<a class="btn btn-inverse" href="javascript:;" onclick="muteUnmute(['class-live-view', 'board-live-view'])"><i class="fa fa-volume-mute"></i></a>
							<a class="btn btn-inverse" href="javascript:;" onclick="volumeDown(['class-live-view', 'board-live-view'])"><i class="fa fa-volume-down"></i></a>
							<a class="btn btn-inverse" href="javascript:;" onclick="volumeUp(['class-live-view', 'board-live-view'])"><i class="fa fa-volume-up"></i></a>
							<a class="btn btn-inverse" href="javascript:;" onclick="pauseVideo(['class-live-view', 'board-live-view'])"><i class="fa fa-pause"></i></a>
							<a class="btn btn-inverse" href="javascript:;" onclick="playVideo(['class-live-view', 'board-live-view'])"><i class="fa fa-play"></i></a>
						</div>
					</div>
					<div class="recordings">
						<div class="classroom-recording recording">
							<div class="recording-title">Класс</div>
							<video preload="none" class="video-js vjs-16-9" controls data-setup="{}" video-source="/storage/lesson/{{ $live->id }}/class.m3u8" id="class-live-view">
							</video>
						</div>
						<div class="board-recording recording">
							<div class="recording-title">Интерактивная доска</div>
							<video preload="none" class="video-js vjs-16-9" controls data-setup="{}" video-source="/storage/lesson/{{ $live->id }}/board.m3u8" id="board-live-view">
							</video>
						</div>
					</div>
				</div>
				<div class="lesson-controll">
					<div class="lesson-controll-data">
						<div class="lesson-controll-button">
							@if($live->recording->status == 'created')
							<button class="btn btn-success">Урок скоро начнется</button>
							@elseif($live->recording->status == 'started')
							<button class="btn btn-success">Урок идет</button>
							@endif
						</div>
						<div class="lesson-clock">00:00:00</div>
					</div>
				</div>
				<div class="lesson-participants">
					<div class=""><strong>Преподаватель</strong></div>
					<table id="remotelearning-table" class="table table-striped table-bordered table-td-valign-middle">
						<thead>
							<tr>
								<th width="1%">№</th>
								<th width="1%" data-orderable="false"></th>
								<th class="text-nowrap">Ф.И.О.</th>
								<th class="text-nowrap">Предмет</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width="1%" class="f-s-600 text-inverse">1</td>
								<td width="1%" class="with-img"><img src="{{ $live->teacher->icon_url }}" class="img-rounded height-30" /></td>
								<td>{{ $live->teacher->name }}</td>
								<td>{{ $live->subject->name }}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		@else
			@if($lessons->isNotEmpty())
				<div class="h3">{{ __('Онлайн урок еще начался') }}</div>
				<hr>
				<div class="h4">{{ __('Уроки сегодня:') }}</div>
				@foreach($lessons as $lesson)
				<div class="mt-2"><strong>{{ $lesson->start_carbon->format('H:i') }}</strong>: {{ $lesson->subject->name }}</div>
				@endforeach
			@else
			<div class="text-center h3">{{ __('Нет уроков по расписанию') }}</div>
			@endif
		@endif
	</div>
</div>