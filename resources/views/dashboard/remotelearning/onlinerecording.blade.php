<div class="panel panel-inverse" data-sortable-id="index-2">
	<div class="panel-heading">
		<h4 class="panel-title">Онлайн запись</h4>
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
		</div>
	</div>
	<div class="panel-body">
		@if(!empty($live))
			@if(!empty($live->topic))
			<div class="text-center">{{ $live->topic }}</div>
			@endif
			<div class="online-recording">
				<div class="live-view" id="live-view" lesson-id="{{ $live->id }}" status-url="{{ route('remotelearning.status') }}">
					<div class="player-controlls text-center mb-3">
						<div class="btn-group">
							<a class="btn btn-inverse" href="javascript:;" onclick="muteUnmute(['class-live-view', 'board-live-view'])"><i class="fa fa-volume-mute"></i></a>
							<a class="btn btn-inverse" href="javascript:;" onclick="volumeDown(['class-live-view', 'board-live-view'])"><i class="fa fa-volume-down"></i></a>
							<a class="btn btn-inverse" href="javascript:;" onclick="volumeUp(['class-live-view', 'board-live-view'])"><i class="fa fa-volume-up"></i></a>
							<a class="btn btn-inverse" href="javascript:;" onclick="pauseVideo(['class-live-view', 'board-live-view'])"><i class="fa fa-pause"></i></a>
							<a class="btn btn-inverse" href="javascript:;" onclick="playVideo(['class-live-view', 'board-live-view'])"><i class="fa fa-play"></i></a>
						</div>
					</div>
					<div class="recordings">

						<div class="classroom-recording recording">
							<div class="recording-title">Класс</div>
							<video preload="none" class="video-js vjs-16-9" controls data-setup="{}" video-source="/storage/lesson/{{ $live->id }}/class.m3u8" id="class-live-view">
							</video>
						</div>
						<div class="board-recording recording">
							<div class="recording-title">Интерактивная доска</div>
							<video preload="none" class="video-js vjs-16-9" controls data-setup="{}" video-source="/storage/lesson/{{ $live->id }}/board.m3u8" id="board-live-view">
							</video>
						</div>
					</div>
				</div>
				<div class="lesson-controll">
					<div class="lesson-controll-data">
						<div class="lesson-controll-button">
							@if($live->recording->status == 'created')
							<form method="POST" action="{{ route('remotelearning.index') }}">
								@csrf
								<input type="hidden" name="action" value="start">
								<input type="hidden" name="lesson_id" value="{{ $live->id }}">
								<button class="btn btn-success">Начать урок</button>
							</form>
							@elseif($live->recording->status == 'started')
							<form method="POST" action="{{ route('remotelearning.index') }}">
								@csrf
								<input type="hidden" name="action" value="end">
								<input type="hidden" name="lesson_id" value="{{ $live->id }}">
								<button class="btn btn-danger">Завершить урок</button>
							</form>
							@endif
						</div>
						<div class="lesson-clock">00:00:00</div>
					</div>
				</div>
				<div class="lesson-participants">
					<div class=""><strong>На уроке</strong></div>
					<table id="remotelearning-table" class="table table-striped table-bordered table-td-valign-middle">
						<thead>
							<tr>
								<th width="1%">№</th>
								<th class="text-nowrap">Класс</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width="1%" class="f-s-600 text-inverse">1</td>
								<td>{{ $live->class->name }}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		@else
			@if($lessons->isNotEmpty())
			<form method="POST" action="{{ route('remotelearning.index') }}">
				@csrf
				<input type="hidden" name="action" value="create">
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Урок') }}</label>
					<div class="col-md-9">
						<select name="lesson_id" class="form-control">
							@foreach($lessons as $lesson)
							<option value="{{ $lesson->id }}">{{ $lesson->start_carbon->format('H:i') }} - {{ __('Урок в ').$lesson->class->name }} ({{ $lesson->subject->name }}) Каб. {{ optional($lesson->classroom->first())->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<button class="btn btn-success">Создать урок</button>
			</form>
			@else
			<div class="text-center h3">{{ __('У вас нет уроков') }}</div>
			@endif
		@endif
	</div>
</div>