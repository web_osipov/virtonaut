<div class="lesson-record">
	<div class="player-controlls text-center mb-3">
		<div class="btn-group">
			<a class="btn btn-inverse" href="javascript:;" onclick="muteUnmute(['lesson-{{ $lesson->id }}-class', 'lesson-{{ $lesson->id }}-board'])"><i class="fa fa-volume-mute"></i></a>
			<a class="btn btn-inverse" href="javascript:;" onclick="volumeDown(['lesson-{{ $lesson->id }}-class', 'lesson-{{ $lesson->id }}-board'])"><i class="fa fa-volume-down"></i></a>
			<a class="btn btn-inverse" href="javascript:;" onclick="volumeUp(['lesson-{{ $lesson->id }}-class', 'lesson-{{ $lesson->id }}-board'])"><i class="fa fa-volume-up"></i></a>
			<a class="btn btn-inverse" href="javascript:;" onclick="pauseVideo(['lesson-{{ $lesson->id }}-class', 'lesson-{{ $lesson->id }}-board'])"><i class="fa fa-pause"></i></a>
			<a class="btn btn-inverse" href="javascript:;" onclick="playVideo(['lesson-{{ $lesson->id }}-class', 'lesson-{{ $lesson->id }}-board'])"><i class="fa fa-play"></i></a>
		</div>
	</div>
	<div class="recordings">
		<div class="classroom-recording recording">
            <div class="recording-title">Класс</div>
            <video class="video-js vjs-16-9" controls id="lesson-{{ $lesson->id }}-class" preload="auto" poster="/storage/lesson/{{ $lesson->id }}/class.png" data-setup="{}">
				<source src="/storage/lesson/{{ $lesson->id }}/class.mp4" type="video/mp4" />
				<p class="vjs-no-js">
					{{ __('Этот браузер не поддерживает HTML5') }}
				</p>
			</video>
		</div>
		<div class="board-recording recording">
			<div class="recording-title">Интерактивная доска</div>
			<video class="video-js vjs-16-9" controls id="lesson-{{ $lesson->id }}-board" preload="auto" poster="/storage/lesson/{{ $lesson->id }}/board.png" data-setup="{}">
                <source src="/storage/lesson/{{ $lesson->id }}/board.mp4" type="video/mp4" />
				<p class="vjs-no-js">
					{{ __('Этот браузер не поддерживает HTML5') }}
				</p>
            </video>
		</div>
	</div>
</div>
<script>
	videojs('lesson-{{ $lesson->id }}-class');
	videojs('lesson-{{ $lesson->id }}-board');

	videojs('lesson-{{ $lesson->id }}-class').on("pause", function () {
		videojs('lesson-{{ $lesson->id }}-board').pause();
	});
	videojs('lesson-{{ $lesson->id }}-class').on("play", function () {
		videojs('lesson-{{ $lesson->id }}-board').play();
	});
	videojs('lesson-{{ $lesson->id }}-board').on("pause", function () {
		videojs('lesson-{{ $lesson->id }}-class').pause();
	});
	videojs('lesson-{{ $lesson->id }}-board').on("play", function () {
		videojs('lesson-{{ $lesson->id }}-class').play();
	});

	var lesson_{{ $lesson->id }}_seeking = false;

	videojs('lesson-{{ $lesson->id }}-class').on("seeking", function (e) {
		if(lesson_{{ $lesson->id }}_seeking == false) {
			lesson_{{ $lesson->id }}_seeking = true;
			var video = videojs('lesson-{{ $lesson->id }}-class');
			videojs('lesson-{{ $lesson->id }}-board').currentTime(video.currentTime());	
		} else {
			lesson_{{ $lesson->id }}_seeking = false;
		}	
	});

	videojs('lesson-{{ $lesson->id }}-board').on("seeking", function (e) {
		if(lesson_{{ $lesson->id }}_seeking == false) {
			lesson_{{ $lesson->id }}_seeking = true;
			var video = videojs('lesson-{{ $lesson->id }}-board');
			videojs('lesson-{{ $lesson->id }}-class').currentTime(video.currentTime());
		} else {
			lesson_{{ $lesson->id }}_seeking = false;
		}
	});
</script>