@if ($paginator->hasPages())
<div class="ml-auto mr-2">{{ ($paginator->perPage() * ($paginator->currentPage() - 1)) + 1 }}-{{ $paginator->perPage() * $paginator->currentPage() }} {{ __('из') }} {{ $paginator->total() }}</div>
<div class="btn-group">
	@if (!$paginator->onFirstPage())
	<a class="btn btn-white btn-sm" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">
		<i class="fa fa-chevron-left"></i>
	</a>
	@endif
	@if ($paginator->hasMorePages())
	<a class="btn btn-white btn-sm" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">
		<i class="fa fa-chevron-right"></i>
	</a>
	@endif
</div>
@endif