<input type="hidden" name="draft_id" value="">
<!-- begin email to -->
<div class="email-to">
	<span class="float-right-link">
		<a href="#" data-click="add-cc" data-field="cc" data-name="Cc" class="m-r-5">Cc</a>
		<a href="#" data-click="add-cc" data-field="bcc" data-name="Bcc">Bcc</a>
	</span>
	<label class="control-label">{{ __('Кому') }}:</label>
	<ul id="email-to" class="primary line-mode">
		@if( request()->email)
		<li>{{ request()->email }}</li>
		@endif
	</ul>
</div>
<!-- end email to -->

<div data-id="extra-cc"></div>

<!-- begin email subject -->
<div class="email-subject">
	<input type="text" name="subject" class="form-control form-control-lg" placeholder="{{ __('Тема') }}" />
</div>
<!-- end email subject -->
<!-- begin email content -->
<div class="email-content p-t-15">
	<textarea class="textarea form-control" name="message" id="editor" rows="20"></textarea>
</div>
<!-- end email content -->

<ul class="attached-document clearfix" id="attachments">
	<li class="fa-file" id="attachment-template">
		<input type="hidden" name="attachments[]" value="">
		<div class="document-file">
			<i class="fa fa-file"></i>
			<img data-dz-thumbnail>
		</div>
		<div class="document-name" data-dz-name></div>
		<div class="delete fa-times" data-dz-remove></div>
		<div class="uploading">
			<div class="progress rounded-corner progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
			  	<div class="progress-bar bg-primary progress-bar-striped progress-bar-animated" style="width: 0%" data-dz-uploadprogress></div>
			</div>
		</div>
		<span class="d-none error-msg" data-dz-errormessage></span>
	</li>
</ul>