<input type="hidden" name="draft_id" value="{{ $message->id }}">
<!-- begin email to -->
<div class="email-to">
	<span class="float-right-link">
		@if($message->cc->isEmpty())<a href="#" data-click="add-cc" data-field="cc" data-name="Cc" class="m-r-5">Cc</a>@endif
		@if($message->bcc->isEmpty())<a href="#" data-click="add-cc" data-field="bcc" data-name="Bcc">Bcc</a>@endif
	</span>
	<label class="control-label">{{ __('Кому') }}:</label>
	<ul id="email-to" class="primary line-mode">
		@foreach($message->to as $to)
		<li>{{ $to['mail'] }}</li>
		@endforeach
	</ul>
</div>
<!-- end email to -->

<div data-id="extra-cc">
	@if($message->cc->isNotEmpty())
	<div class="email-to">
		<label class="control-label">Cc:</label>
		<ul id="email-cc" class="primary line-mode">
			@foreach($message->cc as $cc)
			<li>{{ $cc['mail'] }}</li>
			@endforeach
		</ul>
	</div>
	@endif
	@if($message->bcc->isNotEmpty())
	<div class="email-to">
		<label class="control-label">Bcc:</label>
		<ul id="email-bcc" class="primary line-mode">
			@foreach($message->bcc as $bcc)
			<li>{{ $bcc['mail'] }}</li>
			@endforeach
		</ul>
	</div>
	@endif
</div>

<!-- begin email subject -->
<div class="email-subject">
	<input type="text" class="form-control form-control-lg" name="subject" value="{{ $message->subject }}" placeholder="{{ __('Тема') }}" />
</div>
<!-- end email subject -->
<!-- begin email content -->
<div class="email-content p-t-15">
	<textarea class="textarea form-control" name="message" id="editor" rows="20">{!! $message->message !!}</textarea>
</div>
<!-- end email content -->

<ul class="attached-document clearfix" id="attachments">
	<li class="fa-file" id="attachment-template">
		<input type="hidden" name="attachments[]" value="">
		<div class="document-file">
			<i class="fa fa-file"></i>
			<img data-dz-thumbnail>
		</div>
		<div class="document-name" data-dz-name></div>
		<div class="delete fa-times" data-dz-remove></div>
		<div class="uploading">
			<div class="progress rounded-corner progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
			  	<div class="progress-bar bg-primary progress-bar-striped progress-bar-animated" style="width: 0%" data-dz-uploadprogress></div>
			</div>
		</div>
		<span class="d-none error-msg" data-dz-errormessage></span>
	</li>
	@foreach($message->attachments as $attachment)
	<li class="fa-file">
		<input type="hidden" name="attachments[]" value="{{ $attachment->id }}">
		<div class="document-file">
			@if($attachment->type == 'image')
			<img src="{{ $attachment->image }}">
			@else
			<i class="fa fa-file"></i>
			@endif
		</div>
		<div class="document-name">{{ $attachment->name }}</div>
		<div class="delete fa-times" onclick="$(this).parents('li').remove();"></div>
	</li>
	@endforeach
</ul>
<script>
	var attachmentsMock = @json($message->attachments);
</script>