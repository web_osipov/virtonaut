@extends('layouts.default', ['contentFullHeight' => true])

@section('title', 'Email - Входящие')

@push('css')
	<link href="/assets/plugins/tag-it/css/jquery.tagit.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin vertical-box -->
	<div class="vertical-box with-grid inbox bg-light">
		<!-- begin vertical-box-column -->
		<div class="vertical-box-column width-200">
			<!-- begin vertical-box -->
			<div class="vertical-box">
				<!-- begin wrapper -->
				<div class="wrapper">
					<div class="d-flex align-items-center justify-content-center">
						<a href="#emailNav" data-toggle="collapse" class="btn btn-inverse btn-sm mr-auto d-block d-lg-none">
							<i class="fa fa-cog"></i>
						</a>
						<a href="{{ route('mail.compose') }}" class="btn btn-inverse p-l-40 p-r-40 btn-sm">
							{{ __('Написать') }}
						</a>
					</div>
				</div>
				<!-- end wrapper -->
				<!-- begin vertical-box-row -->
				<div class="vertical-box-row collapse d-lg-table-row" id="emailNav">
					<!-- begin vertical-box-cell -->
					<div class="vertical-box-cell">
						<!-- begin vertical-box-inner-cell -->
						<div class="vertical-box-inner-cell">
							<!-- begin scrollbar -->
							<div data-scrollbar="true" data-height="100%">
								<!-- begin wrapper -->
								<div class="wrapper p-0">
									<div class="nav-title"><b>{{ __('Папки') }}</b></div>
									<ul class="nav nav-inbox">
										<li @if($folder->slug == 'inbox') class="active" @endif>
											<a href="{{ route('mail.index', 'inbox') }}">
												<i class="fa fa-inbox fa-fw m-r-5"></i> 
												{{ __('Входящие') }} 
												@if($folders['inbox']->unread_messages || $folder->slug == 'inbox')
												<span class="badge pull-right">
													{{ $folders['inbox']->unread_messages }}
												</span>
												@endif
											</a>
										</li>
										<li @if($folder->slug == 'sent') class="active" @endif>
											<a href="{{ route('mail.index', 'sent') }}">
												<i class="fa fa-envelope fa-fw m-r-5"></i> 
												{{ __('Отправленные') }} 
												@if($folder->slug == 'sent')
												<span class="badge pull-right">
													{{ $folders['sent']->total_messages }}
												</span>
												@endif
											</a>
										</li>
										<li @if($folder->slug == 'trash') class="active" @endif>
											<a href="{{ route('mail.index', 'trash') }}">
												<i class="fa fa-trash fa-fw m-r-5"></i> 
												{{ __('Удалённые') }} 
												@if($folder->slug == 'trash')
												<span class="badge pull-right">
													{{ $folders['trash']->total_messages }}
												</span>
												@endif
											</a>
										</li>
										<li @if($folder->slug == 'spam') class="active" @endif>
											<a href="{{ route('mail.index', 'spam') }}">
												<i class="fa fa-ban fa-fw m-r-5"></i> 
												{{ __('Спам') }} 
												@if($folder->slug == 'spam')
												<span class="badge pull-right">
													{{ $folders['spam']->total_messages }}
												</span>
												@endif
											</a>
										</li>
										<li @if($folder->slug == 'draft') class="active" @endif>
											<a href="{{ route('mail.index', 'draft') }}">
												<i class="fa fa-pencil-alt fa-fw m-r-5"></i> 
												{{ __('Черновики') }} 
												@if($folder->slug == 'draft')
												<span class="badge pull-right">
													{{ $folders['draft']->total_messages }}
												</span>
												@endif
											</a>
										</li>
									</ul>
								</div>
								<!-- end wrapper -->
							</div>
							<!-- end scrollbar -->
						</div>
						<!-- end vertical-box-inner-cell -->
					</div>
					<!-- end vertical-box-cell -->
				</div>
				<!-- end vertical-box-row -->
			</div>
			<!-- end vertical-box -->
		</div>
		<!-- end vertical-box-column -->
		<!-- begin vertical-box-column -->
		<div class="vertical-box-column">
			<!-- begin vertical-box -->
			<div class="vertical-box">
				<!-- begin wrapper -->
				<div class="wrapper">
					<span class="btn-group mr-2">
						<button class="btn btn-white btn-sm send-messsage"><i class="fa fa-fw fa-envelope"></i> <span class="hidden-xs">{{ __('Отправить') }}</span></button>
						<a href="javascript:;" class="btn btn-white btn-sm fileinput-button"><i class="fa fa-fw fa-paperclip"></i> <span class="hidden-xs">{{ __('Прикрепить файл') }}</span></a>
					</span>
					<span class="btn-group">
						<button class="btn btn-white btn-sm save-draft"><i class="fa fa-fw fa-pencil-alt"></i> <span class="hidden-xs">{{ __('Сохранить черновик') }}</span></button>
					</span>
					<span class="pull-right">
						<button class="btn btn-white btn-sm" inbox-url="{{ route('mail.index', 'inbox') }}" message-url="{{ route('mail.message', 0) }}" data-email-action="delete"><i class="fa fa-fw fa-times"></i> <span class="hidden-xs">{{ __('Удалить') }}</span></button>
					</span>
				</div>
				<!-- end wrapper -->
				<!-- begin vertical-box-row -->
				<div class="vertical-box-row bg-white">
					<!-- begin vertical-box-cell -->
					<div class="vertical-box-cell">
						<!-- begin vertical-box-inner-cell -->
						<div class="vertical-box-inner-cell">
							<!-- begin scrollbar -->
							<div data-scrollbar="true" data-height="100%" class="p-15">
								<!-- begin email form -->
								<form draft-action="{{ route('mail.draft') }}" action="{{ route('mail.send') }}" method="POST" id="compose-form" name="email_to_form">
									@csrf
									@includeWhen($message && $is_draft, 'dashboard/mail/compose_draft')
									@includeWhen($message && !$is_draft, 'dashboard/mail/compose_reply')
									@includeWhen(!$message && !$is_draft, 'dashboard/mail/compose_empty')
								</form>
								<!-- end email form -->
							</div>
							<!-- end scrollbar -->
						</div>
						<!-- end vertical-box-inner-cell -->
					</div>
					<!-- end vertical-box-cell -->
				</div>
				<!-- end vertical-box-row -->
				<!-- begin wrapper -->
				<div class="wrapper text-right">
					<button type="submit" class="btn btn-white p-l-40 p-r-40 m-r-5" inbox-url="{{ route('mail.index', 'inbox') }}" message-url="{{ route('mail.message', 0) }}" data-email-action="delete">{{ __('Удалить') }}</button>
					<button type="submit" class="send-messsage btn btn-primary p-l-40 p-r-40">{{ __('Отправить') }}</button>
				</div>
				<!-- end wrapper -->
			</div>
			<!-- end vertical-box -->
		</div>
		<!-- end vertical-box-column -->
	</div>
	<!-- end vertical-box -->
@endsection

@push('scripts')
	<script src="/assets/js/dropzone/dropzone.min.js"></script>
	<script>
		Dropzone.prototype.defaultOptions.dictDefaultMessage = "{{ __('Перетащите файлы сюда, чтобы загрузить') }}";
		Dropzone.prototype.defaultOptions.dictFallbackMessage = "{{ __('Ваш браузер не поддерживает drag\'n\'drop загрузки файлов.') }}";
		Dropzone.prototype.defaultOptions.dictFallbackText = "{{ __('Пожалуйста, используйте запасную форму ниже, чтобы загрузить свои файлы, как в старые времена.') }}";
		Dropzone.prototype.defaultOptions.dictFileTooBig = "Файл слишком большой (@{{filesize}}МБ). Максимальный размер файла: @{{maxFilesize}}МБ.";
		Dropzone.prototype.defaultOptions.dictInvalidFileType = "{{ __('Вы не можете загружать файлы этого типа.') }}";
		Dropzone.prototype.defaultOptions.dictResponseError = "Сервер ответил кодом @{{statusCode}}.";
		Dropzone.prototype.defaultOptions.dictCancelUpload = "{{ __('Отменить загрузку') }}";
		Dropzone.prototype.defaultOptions.dictCancelUploadConfirmation = "{{ __('Вы уверены, что хотите отменить загрузку?') }}";
		Dropzone.prototype.defaultOptions.dictRemoveFile = "{{ __('Удалить файл') }}";
		Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = "{{ __('Вы не можете загружать больше файлов.') }}";
	</script>
	<script src="/assets/plugins/jquery-migrate/dist/jquery-migrate.min.js"></script>
	<script src="/assets/plugins/tag-it/js/tag-it.min.js"></script>
	<script src="/assets/js/tinymce/tinymce.min.js"></script>
	<script src="/assets/js/mail.compose.min.js?ver={{ config('app.version') }}"></script>
@endpush