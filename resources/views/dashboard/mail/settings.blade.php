@extends('layouts.default')

@section('title', __('Настройки Email'))

@push('css')
	<link href="/assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right">
		<li class="breadcrumb-item">{{ __('Личный кабинет') }}</li>
		<li class="breadcrumb-item">{{ __('Email') }}</li>
		<li class="breadcrumb-item active">{{ __('Настройки') }}</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">{{ __('Настройки Email') }}</h1>
	<!-- end page-header -->
	<!-- begin panel -->
	<div class="panel panel-inverse">
		<!-- begin panel-body -->
		<div class="panel-body">
			@if (session('status'))
			    <div class="alert alert-success fade show">
			        {{ session('status') }}
			    </div>
			@endif
			<div class="note note-default">
			  	<div class="note-content mw-100">
			    	<h4>{{ __('DNS записи для доменов') }}</h4>
			   		<div class="table-responsive">
						<table class="table m-b-0">
							<thead>
								<tr>
									<th>{{ __('Хост') }}</th>
									<th>{{ __('Тип записи') }}</th>
									<th>{{ __('Значение записи') }}</th>
									<th>{{ __('Приоритет') }}</th>
									<th>{{ __('TTL') }}</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>@</td>
									<td>MX</td>
									<td>{{ config('mailclient.main_server') }}</td>
									<td>10</td>
									<th>21600</th>
								</tr>
								<tr>
									<td>mail</td>
									<td>MX</td>
									<td>{{ config('mailclient.main_server') }}</td>
									<td>10</td>
									<th>21600</th>
								</tr>
							</tbody>
						</table>
					</div>
			  	</div>
			</div>
			<form action="{{ route('settings.mail.add_domain') }}" class="form-inline m-b-20" method="POST">
				@csrf
				<div class="form-group m-r-10">
					<input type="text" name="domain" required="" class="form-control @error('domain') is-invalid @enderror"  placeholder="{{ __('Введите домен') }}">
				</div>
				<button class="btn btn-primary">{{ __('Добавить домен') }}</button>
			</form>
			<div class="table-responsive">
				<table class="table table-striped m-b-0">
					<thead>
						<tr>
							<th>#</th>
							<th>{{ __('Почтовый домен') }}</th>
							<th>{{ __('Количество почтовых адресов') }}</th>
							<th width="1%"></th>
						</tr>
					</thead>
					<tbody>
						@foreach($mail_domains as $mail_domain)
						<tr>
							<td>{{ $mail_domain->id }}</td>
							<td>{{ $mail_domain->domain }}</td>
							<td>{{ $mail_domain->mails()->count() }}</td>
							<td class="with-btn" nowrap="">
								<button data-del-url="{{ route('settings.mail.del_domain', $mail_domain->id) }}" data-click="delete-domain" class="btn btn-sm btn-danger width-80">{{ __('Удалить') }}</button>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<!-- end panel-body -->
	</div>
	<!-- end panel -->
@endsection


@push('scripts')
	<script src="/assets/plugins/gritter/js/jquery.gritter.js"></script>
	<script src="/assets/plugins/sweetalert/dist/sweetalert.min.js"></script>
	<script src="/assets/js/mail.settings.min.js?ver={{ config('app.version') }}"></script>
@endpush