@extends('layouts.default', ['contentFullHeight' => true])

@section('title', 'Email - Входящие')

@section('content')
	<!-- begin vertical-box -->
	<div class="vertical-box with-grid inbox bg-light">
		<!-- begin vertical-box-column -->
		<div class="vertical-box-column width-200">
			<!-- begin vertical-box -->
			<div class="vertical-box">
				<!-- begin wrapper -->
				<div class="wrapper">
					<div class="d-flex align-items-center justify-content-center">
						<a href="#emailNav" data-toggle="collapse" class="btn btn-inverse btn-sm mr-auto d-block d-lg-none">
							<i class="fa fa-cog"></i>
						</a>
						<a href="{{ route('mail.compose') }}" class="btn btn-inverse p-l-40 p-r-40 btn-sm">
							{{ __('Написать') }}
						</a>
					</div>
				</div>
				<!-- end wrapper -->
				<!-- begin vertical-box-row -->
				<div class="vertical-box-row collapse d-lg-table-row" id="emailNav">
					<!-- begin vertical-box-cell -->
					<div class="vertical-box-cell">
						<!-- begin vertical-box-inner-cell -->
						<div class="vertical-box-inner-cell">
							<!-- begin scrollbar -->
							<div data-scrollbar="true" data-height="100%">
								<!-- begin wrapper -->
								<div class="wrapper p-0">
									<div class="nav-title"><b>{{ __('Папки') }}</b></div>
									<ul class="nav nav-inbox">
										<li @if($folder->slug == 'inbox') class="active" @endif>
											<a href="{{ route('mail.index', 'inbox') }}">
												<i class="fa fa-inbox fa-fw m-r-5"></i> 
												{{ __('Входящие') }} 
												@if($folders['inbox']->unread_messages || $folder->slug == 'inbox')
												<span class="badge pull-right">
													{{ $folders['inbox']->unread_messages }}
												</span>
												@endif
											</a>
										</li>
										<li @if($folder->slug == 'sent') class="active" @endif>
											<a href="{{ route('mail.index', 'sent') }}">
												<i class="fa fa-envelope fa-fw m-r-5"></i> 
												{{ __('Отправленные') }} 
												@if($folder->slug == 'sent')
												<span class="badge pull-right">
													{{ $folders['sent']->total_messages }}
												</span>
												@endif
											</a>
										</li>
										<li @if($folder->slug == 'trash') class="active" @endif>
											<a href="{{ route('mail.index', 'trash') }}">
												<i class="fa fa-trash fa-fw m-r-5"></i> 
												{{ __('Удалённые') }} 
												@if($folder->slug == 'trash')
												<span class="badge pull-right">
													{{ $folders['trash']->total_messages }}
												</span>
												@endif
											</a>
										</li>
										<li @if($folder->slug == 'spam') class="active" @endif>
											<a href="{{ route('mail.index', 'spam') }}">
												<i class="fa fa-ban fa-fw m-r-5"></i> 
												{{ __('Спам') }} 
												@if($folder->slug == 'spam')
												<span class="badge pull-right">
													{{ $folders['spam']->total_messages }}
												</span>
												@endif
											</a>
										</li>
										<li @if($folder->slug == 'draft') class="active" @endif>
											<a href="{{ route('mail.index', 'draft') }}">
												<i class="fa fa-pencil-alt fa-fw m-r-5"></i> 
												{{ __('Черновики') }} 
												@if($folder->slug == 'draft')
												<span class="badge pull-right">
													{{ $folders['draft']->total_messages }}
												</span>
												@endif
											</a>
										</li>
									</ul>
								</div>
								<!-- end wrapper -->
							</div>
							<!-- end scrollbar -->
						</div>
						<!-- end vertical-box-inner-cell -->
					</div>
					<!-- end vertical-box-cell -->
				</div>
				<!-- end vertical-box-row -->
			</div>
			<!-- end vertical-box -->
		</div>
		<!-- end vertical-box-column -->
		<!-- begin vertical-box-column -->
		<div class="vertical-box-column">
			<!-- begin vertical-box -->
			<div class="vertical-box">
				<!-- begin wrapper -->
				<div class="wrapper clearfix">
					<div class="pull-left">
						<div class="btn-group mr-2">
							<a href="{{ route('mail.compose', $message->id) }}" class="btn btn-white btn-sm"><i class="fa fa-fw fa-reply"></i> <span class="d-none d-lg-inline">{{ __('Ответить') }}</span></a>
						</div>
						<div class="btn-group mr-2">
							<a href="javascript:;" class="btn btn-white btn-sm"><i class="fa fa-fw fa-trash"></i> <span class="d-none d-lg-inline" data-email-action="delete">{{ __('Удалить') }}</span></a>
							@if($folder->slug == 'inbox')
							<a href="javascript:;" class="btn btn-white btn-sm"><i class="fa fa-fw fa-ban"></i> <span class="d-none d-lg-inline" data-email-action="spam">{{ __('Это спам') }}</span></a>
							@endif
						</div>
					</div>
					<div class="pull-right">
						<div class="btn-group">
							<a href="{{ route('mail.index', $folder->slug) }}" class="btn btn-white btn-sm"><i class="fa fa-fw fa-times"></i></a>
						</div>
					</div>
				</div>
				<!-- end wrapper -->
				<!-- begin vertical-box-row -->
				<div class="vertical-box-row bg-white">
					<!-- begin vertical-box-cell -->
					<div class="vertical-box-cell">
						<!-- begin vertical-box-inner-cell -->
						<div class="vertical-box-inner-cell">
							<!-- begin scrollbar -->
							<div data-scrollbar="true" data-height="100%">
								<!-- begin wrapper -->
								<div class="wrapper">
									<h3 class="m-t-0 m-b-15 f-w-500">{{ $message->subject ?: __('(без темы)') }}</h3>
									<ul class="media-list underline m-b-15 p-b-15">
										<li class="media media-sm clearfix">
											<div class="media-body">
												<div class="email-from text-inverse f-s-14 f-w-600 m-b-3">
													{{ __('от') }} {{ $message->from->isNotEmpty() ? $message->from[0]['mail'] : __('Без отправителя') }}
												</div>
												<div class="m-b-3">
													<i class="fa fa-clock fa-fw"></i> {{ $message->created_at->isoFormat('LLL') }}
												</div>
												<x-mail-address class="email-to" :addresses="$message->to" title="{{ __('Кому') }}" />
												<x-mail-address class="email-сс" :addresses="$message->cc" title="{{ __('Копия') }}" />
												<x-mail-address class="email-bcc" :addresses="$message->bcc" title="{{ __('Скрытая копия') }}" />
											</div>
										</li>
									</ul>

									@if($message->attachments()->count() > 0)
									<x-mail-attachment :attachments="$message->attachments" />
									@endif

									<div class="message-body">{!! $message->message !!}</div>
								</div>
								<!-- end wrapper -->
							</div>
							<!-- end scrollbar -->
						</div>
						<!-- end vertical-box-inner-cell -->
					</div>
					<!-- end vertical-box-cell -->
				</div>
				<!-- end vertical-box-row -->
				<!-- begin wrapper -->
				<div class="wrapper text-right clearfix">
					<div class="btn-group">
						<a href="{{ route('mail.index', $folder->slug) }}" class="btn btn-white btn-sm"><i class="fa fa-fw fa-times"></i></a>
					</div>
				</div>
				<!-- end wrapper -->
			</div>
			<!-- end vertical-box -->
		</div>
		<!-- end vertical-box-column -->
	</div>
	<!-- end vertical-box -->
@endsection

@push('scripts')
	<script src="/assets/js/mail.message.min.js?ver={{ config('app.version') }}"></script>
@endpush