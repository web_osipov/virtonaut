@extends('layouts.default', ['contentFullHeight' => true])

@section('title', 'Email - Входящие')

@section('content')
	<!-- begin vertical-box -->
	<div class="vertical-box with-grid inbox bg-light">
		<!-- begin vertical-box-column -->
		<div class="vertical-box-column width-200">
			<!-- begin vertical-box -->
			<div class="vertical-box">
				<!-- begin wrapper -->
				<div class="wrapper">
					<div class="d-flex align-items-center justify-content-center">
						<a href="#emailNav" data-toggle="collapse" class="btn btn-inverse btn-sm mr-auto d-block d-lg-none">
							<i class="fa fa-cog"></i>
						</a>
						<a href="{{ route('mail.compose') }}" class="btn btn-inverse p-l-40 p-r-40 btn-sm">
							{{ __('Написать') }}
						</a>
					</div>
				</div>
				<!-- end wrapper -->
				<!-- begin vertical-box-row -->
				<div class="vertical-box-row collapse d-lg-table-row" id="emailNav">
					<!-- begin vertical-box-cell -->
					<div class="vertical-box-cell">
						<!-- begin vertical-box-inner-cell -->
						<div class="vertical-box-inner-cell">
							<!-- begin scrollbar -->
							<div data-scrollbar="true" data-height="100%">
								<!-- begin wrapper -->
								<div class="wrapper p-0">
									<div class="nav-title"><b>{{ __('Папки') }}</b></div>
									<ul class="nav nav-inbox">
										<li @if($folder->slug == 'inbox') class="active" @endif>
											<a href="{{ route('mail.index', 'inbox') }}">
												<i class="fa fa-inbox fa-fw m-r-5"></i> 
												{{ __('Входящие') }} 
												@if($folders['inbox']->unread_messages || $folder->slug == 'inbox')
												<span class="badge pull-right">
													{{ $folders['inbox']->unread_messages }}
												</span>
												@endif
											</a>
										</li>
										<li @if($folder->slug == 'sent') class="active" @endif>
											<a href="{{ route('mail.index', 'sent') }}">
												<i class="fa fa-envelope fa-fw m-r-5"></i> 
												{{ __('Отправленные') }} 
												@if($folder->slug == 'sent')
												<span class="badge pull-right">
													{{ $folders['sent']->total_messages }}
												</span>
												@endif
											</a>
										</li>
										<li @if($folder->slug == 'trash') class="active" @endif>
											<a href="{{ route('mail.index', 'trash') }}">
												<i class="fa fa-trash fa-fw m-r-5"></i> 
												{{ __('Удалённые') }} 
												@if($folder->slug == 'trash')
												<span class="badge pull-right">
													{{ $folders['trash']->total_messages }}
												</span>
												@endif
											</a>
										</li>
										<li @if($folder->slug == 'spam') class="active" @endif>
											<a href="{{ route('mail.index', 'spam') }}">
												<i class="fa fa-ban fa-fw m-r-5"></i> 
												{{ __('Спам') }} 
												@if($folder->slug == 'spam')
												<span class="badge pull-right">
													{{ $folders['spam']->total_messages }}
												</span>
												@endif
											</a>
										</li>
										<li @if($folder->slug == 'draft') class="active" @endif>
											<a href="{{ route('mail.index', 'draft') }}">
												<i class="fa fa-pencil-alt fa-fw m-r-5"></i> 
												{{ __('Черновики') }} 
												@if($folder->slug == 'draft')
												<span class="badge pull-right">
													{{ $folders['draft']->total_messages }}
												</span>
												@endif
											</a>
										</li>
									</ul>
								</div>
								<!-- end wrapper -->
							</div>
							<!-- end scrollbar -->
						</div>
						<!-- end vertical-box-inner-cell -->
					</div>
					<!-- end vertical-box-cell -->
				</div>
				<!-- end vertical-box-row -->
			</div>
			<!-- end vertical-box -->
		</div>
		<!-- end vertical-box-column -->
		<!-- begin vertical-box-column -->
		<div class="vertical-box-column">
			<!-- begin vertical-box -->
			<div class="vertical-box">
				<!-- begin wrapper -->
				<div class="wrapper">
					<!-- begin btn-toolbar -->
					<div class="btn-toolbar align-items-center">
						<div class="custom-control custom-checkbox mr-2">
							<input type="checkbox" class="custom-control-input" data-checked="email-checkbox" id="emailSelectAll" data-change="email-select-all" />
							<label class="custom-control-label" for="emailSelectAll"></label>
						</div>
						@if($folder->slug == 'inbox')
						<div class="dropdown mr-2">
							<button class="btn btn-white btn-sm" data-toggle="dropdown">
								{{ request()->unread ? __('Непрочитанные письма') : __('Все письма') }} <span class="caret m-l-3"></span>
							</button>
							<div class="dropdown-menu">
								<a href="{{ route('mail.index', $folder->slug) }}" class="dropdown-item {{ !request()->unread ? 'active' : '' }}">
									{{ __('Все письма') }}
								</a>
								<a href="{{ route('mail.index', $folder->slug) }}?unread=1" class="dropdown-item {{ request()->unread == 1 ? 'active' : '' }}">
									{{ __('Непрочитанные письма') }}
								</a>
							</div>
						</div>
						<a href="{{ route('mail.index', $folder->slug) }}" class="btn btn-sm btn-white mr-2">
							<i class="fa fa-redo"></i>
						</a>
						@endif
						<!-- begin btn-group -->
						<div class="btn-group">
							<button class="btn btn-sm btn-white hide" data-email-action="delete">
								<i class="fa fa-times mr-2"></i> 
								<span class="hidden-xs">{{ __('Удалить') }}</span>
							</button>
							@if($folder->slug == 'inbox')
							<button class="btn btn-sm btn-white hide" data-email-action="spam">
								<i class="fa fa-ban mr-2"></i> 
								<span class="hidden-xs">{{ __('Это спам') }}</span>
							</button>
							@endif
						</div>
						<!-- end btn-group -->
						<!-- begin btn-group -->
						{{ $messages->links('dashboard.mail.mailpagination') }}
						<!-- end btn-group -->
					</div>
					<!-- end btn-toolbar -->
				</div>
				<!-- end wrapper -->
				<!-- begin vertical-box-row -->
				<div class="vertical-box-row">
					<!-- begin vertical-box-cell -->
					<div class="vertical-box-cell">
						<!-- begin vertical-box-inner-cell -->
						<div class="vertical-box-inner-cell bg-white">
							<!-- begin scrollbar -->
							<div data-scrollbar="true" data-height="100%">
								<!-- begin list-email -->
								<ul class="list-group list-group-lg no-radius list-email">
									@foreach($messages as $message)
									<li class="list-group-item @if(!$message->read) unread @endif">
										<div class="email-checkbox">
											<div class="custom-control custom-checkbox">
												<input type="checkbox" name="messages[]" value="{{ $message->id }}" class="custom-control-input" data-checked="email-checkbox" id="emailCheckbox{{ $message->id }}">
												<label class="custom-control-label" for="emailCheckbox{{ $message->id }}"></label>
											</div>
										</div>
										@if($folder->slug == 'sent')
										<a href="{{ route($folder->slug == 'draft' ? 'mail.compose' : 'mail.message', $message->id) }}" class="email-user bg-blue">
											@if($message->to->isNotEmpty())
											<span class="text-white">{{ \App\Helpers\MailHelper::getSenderLetter($message->to[0]['full']) }}</span>
											@else
											<span class="text-white">{{ __('Б') }}</span>
											@endif
										</a>
										<div class="email-info">
											<a href="{{ route($folder->slug == 'draft' ? 'mail.compose' : 'mail.message', $message->id) }}">
												<span class="email-sender">
													@if($message->to->isNotEmpty())
													{{ $message->to[0]['personal'] ? $message->to[0]['personal'] .' <'.$message->to[0]['mail'].'>' : $message->to[0]['mail'] }}
													@else
													{{ __('Без получателя') }}
													@endif
												</span>
												<span class="email-title">{{ $message->subject ?: __('(без темы)') }}</span>
												<span class="email-desc">{!! \App\Helpers\MailHelper::getTruncateContent($message->message) !!}</span>
												<span class="email-time">{{ $message->created_at->diffForHumans() }}</span>
											</a>
										</div>
										@else
										<a href="{{ route($folder->slug == 'draft' ? 'mail.compose' : 'mail.message', $message->id) }}" class="email-user bg-blue">
											@if($message->from->isNotEmpty())
											<span class="text-white">{{ \App\Helpers\MailHelper::getSenderLetter($message->from[0]['full']) }}</span>
											@else
											<span class="text-white">{{ __('Б') }}</span>
											@endif
										</a>
										<div class="email-info">
											<a href="{{ route($folder->slug == 'draft' ? 'mail.compose' : 'mail.message', $message->id) }}">
												<span class="email-sender">
													@if($message->from->isNotEmpty())
													{{ $message->from[0]['personal'] ? $message->from[0]['personal'] .' <'.$message->to[0]['mail'].'>' : $message->from[0]['mail'] }}
													@else
													{{ __('Без отправителя') }}
													@endif
												</span>
												<span class="email-title">{{ $message->subject ?: __('(без темы)') }}</span>
												<span class="email-desc">{!! \App\Helpers\MailHelper::getTruncateContent($message->message) !!}</span>
												<span class="email-time">{{ $message->created_at->diffForHumans() }}</span>
											</a>
										</div>
										@endif
									</li>
									@endforeach
									@if(count($messages) == 0)
									<li class="p-15 text-center">
										<div class="p-20">
											<i class="fa fa-trash fa-5x text-silver"></i>
										</div>
										{{ __('Пусто') }} 
									</li>
									@endif
								</ul>
								<!-- end list-email -->
							</div>
							<!-- end scrollbar -->
						</div>
						<!-- end vertical-box-inner-cell -->
					</div>
					<!-- end vertical-box-cell -->
				</div>
				<!-- end vertical-box-row -->
				<!-- begin wrapper -->
				<div class="wrapper clearfix d-flex align-items-center">
					<div class="text-inverse f-w-600">{{ $folders[$folder->slug]->total_messages }} {{ \App\Helpers\MailHelper::getMessagePlural($folders[$folder->slug]->total_messages) }}</div>
					{{ $messages->links('dashboard.mail.mailpagination') }}
				</div>
				<!-- end wrapper -->
			</div>
			<!-- end vertical-box -->
		</div>
		<!-- end vertical-box-column -->
	</div>
	<!-- end vertical-box -->
@endsection

@push('scripts')
	<script src="/assets/js/mail.index.min.js?ver={{ config('app.version') }}"></script>
@endpush