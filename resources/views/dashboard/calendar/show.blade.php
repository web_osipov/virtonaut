<div class="event">
	@if($event->picture)
	<img src="/storage/{{ $event->picture }}">
	@endif
	<div class="event-name text-center p-10 h1" style="{{ $event->title_style }}">{{ $event->name }}</div>
	<div class="event-description p-10 f-s-16">
		@if($event->all_day)
			@if($event->start)
			<div class="event-start m-b-10"><strong>{{ __('Весь день') }}:</strong> {{ $event->start->isoFormat('LL') }}</div>
			@endif
		@else
			@if($event->start)
			<div class="event-start m-b-10"><strong>{{ __('Начало') }}:</strong> {{ $event->start->isoFormat('LLLL') }}</div>
			@endif
			@if($event->end)
			<div class="event-start m-b-10"><strong>{{ __('Конец') }}:</strong> {{ $event->end->isoFormat('LLLL') }}</div>
			@endif
		@endif
		@if($event->content)
		<div class="event-content m-t-20">{!! $event->content !!}</div>
		@endif
	</div>
</div>