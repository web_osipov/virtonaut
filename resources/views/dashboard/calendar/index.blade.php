@extends('layouts.default')

@section('title', __('Календарь'))

@push('css')
	<link href="/assets/plugins/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet" />

	<link href="/assets/plugins/fullcalendar/fullcalendar.print.css" rel="stylesheet" media='print' />
	<link href="/assets/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" />
	<link href="/assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
@endpush

@section('content')
	<ol class="breadcrumb float-xl-right">
		<li class="breadcrumb-item">{{ __('Личный кабинет') }}</li>
		<li class="breadcrumb-item active">{{ __('Календарь') }}</li>
	</ol>
	<h1 class="page-header">{{ __('Календарь') }} <small>{{ __('все происходящие события находятся тут') }}</small></h1>
	
	@group('admin')
	<div class="custom-control custom-switch">
		<input type="checkbox" class="custom-control-input" {{ !empty(session('edit_calendar')) ? 'checked' : '' }} id="edit_calendar_mode_checkbox">
		<label class="custom-control-label" for="edit_calendar_mode_checkbox">{{ __('Режим редактирования') }}</label>
	</div>
	@endgroup

	@if(!empty(session('edit_calendar')))
	<hr />
	
	<button type="button" class="btn btn-primary" onclick="loadCreateEventForm()">
		{{ __('Добавить событие') }}
	</button>

	<!-- Modal -->
	<div class="modal fade" id="addEventModal" crate-link="{{ route('calendar.create') }}" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">{{ __('Добавить событие') }}</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Закрыть') }}</button>
					<button type="button" class="btn btn-primary create-event">{{ __('Добавить') }}</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="editEventModal" edit-link="{{ route('calendar.edit') }}" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">{{ __('Изменить событие') }}</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Закрыть') }}</button>
					<button type="button" class="btn btn-primary edit-event">{{ __('Изменить') }}</button>
				</div>
			</div>
		</div>
	</div>
	@else
	<div class="modal fade" id="showEventModal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body p-0">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Закрыть') }}</button>
				</div>
			</div>
		</div>
	</div>
	@endif

	<hr />

	<div id="calendar" class="calendar"></div>

@endsection

@push('scripts')
	<script src="/assets/plugins/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>

	<script src="/assets/plugins/moment/moment.js"></script>
	<script src="/assets/plugins/moment/locale/ru.js"></script>	

	<script src="/assets/plugins/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

	<script src="/assets/plugins/fullcalendar/main.min.js?ver={{ config('app.version') }}"></script>
	<script src="/assets/plugins/fullcalendar/ru.min.js?ver={{ config('app.version') }}"></script>

	<script src="/assets/js/tinymce/tinymce.min.js"></script>

	@group('admin')
	<script>
		$(document).ready(function() {
			$('body').on('change', '#edit_calendar_mode_checkbox', function() {
				location.href = '{{ route('calendar.edit_mode') }}';
			});
		});
	</script>
	@endgroup

	@if(!empty(session('edit_calendar')))
	<script src="/assets/js/calendar.edit.min.js?ver={{ config('app.version') }}"></script>
	@else
	<script src="/assets/js/calendar.min.js?ver={{ config('app.version') }}"></script>
	@endif
@endpush
