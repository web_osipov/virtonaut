<form action="{{ route('calendar.update', $event->id) }}" enctype="multipart/form-data" method="POST" id="event-edit-form">
	@csrf
	@method('PUT')
	<button type="button" delete-link="{{ route('calendar.destroy', $event->id) }}" class="btn btn-danger delete-event m-b-15">{{ __('Удалить событие') }}</button>
	<div class="form-group m-b-15">
		<label>{{ __('Название') }}</label>
		<input type="text" name="name" value="{{ old('name', $event->name) }}" class="form-control" required />
	</div>
	<div class="form-group m-b-15">
		<label>{{ __('Дата начала') }}</label>
		<div class="input-group date datetimepicker">
			<input type="text" name="start" value="{{ old('start', $event->start_formated) }}" class="form-control" required />
			<div class="input-group-addon">
				<i class="fa fa-calendar"></i>
			</div>
		</div>
		@error('start')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
	</div>
	<div class="form-group m-b-15">
		<label>{{ __('Дата конца') }}</label>
		<div class="input-group date datetimepicker">
			<input type="text" name="end" value="{{ old('end', $event->end_formated) }}" class="form-control" required />
			<div class="input-group-addon">
				<i class="fa fa-calendar"></i>
			</div>
		</div>
		@error('end')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
	</div>
	<div class="form-group m-b-15 ">
		<div class="custom-control custom-switch">
			<input type="checkbox" class="custom-control-input" name="all_day" {{ old('all_day', $event->all_day) == 1 ? 'checked' : '' }} id="all_day_checkbox" value="1">
			<label class="custom-control-label" for="all_day_checkbox">{{ __('Весь день') }}</label>
		</div>
	</div>
	<div class="form-group m-b-15">
		<label>{{ __('Цвет события') }}</label>
		<div class="input-group">
			<input type="text" name="color" value="{{ old('color', $event->color) }}" class="form-control colorpicker-element" />
		</div>
	</div>
	<div class="form-group m-b-15">
		<label>{{ __('Цвет текста события') }}</label>
		<div class="input-group">
			<input type="text" name="color_text" value="{{ old('color_text', $event->color_text) }}" class="form-control colorpicker-element" />
		</div>
	</div>
	<div class="form-group m-b-15">
		<label>{{ __('Описание') }}</label>
		<div class="">
			<textarea class="form-control event-editor" name="content">{{ old('content', $event->content) }}</textarea>
		</div>
	</div>
	<div class="form-group">
	    <label>{{ __('Картинка') }}</label>
	    @if($event->picture)
	    <div class="form-check m-b-10">
			<input type="checkbox" name="del_picture" value="1" id="delete-picture" class="form-check-input">
			<label class="form-check-label" for="delete-picture">{{ __('Удалить картинку') }}</label>
		</div>
		@endif
	    <input type="file" name="picture" class="form-control-file">
	</div>
	<button type="submit" class="d-none">{{ __('Изменить') }}</button>
</form>