@extends('layouts.default')

@section('title', __('Добавить урок'))

@push('css')
	<link href="/assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
	<link href="/assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right">
		<li class="breadcrumb-item">{{ __('Личный кабинет') }}</li>
		<li class="breadcrumb-item">{{ __('Расписание') }}</li>
		<li class="breadcrumb-item">{{ __('Уроки') }}</li>
		<li class="breadcrumb-item active">{{ __('Добавить урок') }}</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">{{ __('Добавить урок') }}</h1>
	<!-- end page-header -->
	<!-- begin panel -->
	<div class="panel panel-inverse">
		<!-- begin panel-body -->
		<div class="panel-body">
			<form action="{{ route('schedule.lesson.create') }}" method="POST" id="lesson-edit-form">
				@csrf

				<div class="form-group row m-b-15" id="institution-block">
					<label class="col-form-label col-md-3">{{ __('Учебное учреждение') }}</label>
					<div class="col-md-9">
						<select name="institution" class="form-control @error('institution') is-invalid @enderror selectpicker" data-size="10" data-live-search="true">
							@foreach($institutions as $institution)
								<option value="{{ $institution->id }}" {{ (old('institution') == $institution->id) ? 'selected':'' }}>{{ $institution->short_name }}</option>
							@endforeach
						</select>
						@error('institution')
		                    <span class="invalid-feedback" role="alert">
		                        <strong>{{ $message }}</strong>
		                    </span>
		                @enderror
					</div>
				</div>

				<div class="form-group row m-b-15" id="class-block">
					<label class="col-form-label col-md-3">{{ __('Класс') }}</label>
					<div class="col-md-9">
						<select name="class" class="form-control @error('class') is-invalid @enderror selectpicker" data-size="10" multiple data-max-options="1" data-live-search="true">
							@foreach($institutions as $institution)
								<optgroup label="{{ $institution->short_name }}" institution-id="{{ $institution->id }}">
								@foreach($classes[$institution->id] as $class)
									<option value="{{ $class->id }}" {{ (old('class') == $class->id) ? 'selected':'' }}>{{ $class->name }}</option>
								@endforeach
								</optgroup>
							@endforeach
						</select>
						@error('class')
		                    <span class="invalid-feedback" role="alert">
		                        <strong>{{ $message }}</strong>
		                    </span>
		                @enderror
					</div>
				</div>

				<div class="form-group row m-b-15" id="classroom-block">
					<label class="col-form-label col-md-3">{{ __('Учебный класс (место проведения)') }}</label>
					<div class="col-md-9">
						<select name="classroom" class="form-control @error('classroom') is-invalid @enderror selectpicker" data-size="10" multiple data-max-options="1" data-live-search="true">
							@foreach($institutions as $institution)
								<optgroup label="{{ $institution->short_name }}" institution-id="{{ $institution->id }}">
								@foreach($classrooms[$institution->id] as $classroom)
									<option value="{{ $classroom->id }}" {{ (old('classroom') == $classroom->id) ? 'selected':'' }}>{{ $classroom->name }}</option>
								@endforeach
								</optgroup>
							@endforeach
						</select>
						@error('classroom')
		                    <span class="invalid-feedback" role="alert">
		                        <strong>{{ $message }}</strong>
		                    </span>
		                @enderror
					</div>
				</div>

				<div class="form-group row m-b-15" id="teacher-block">
					<label class="col-form-label col-md-3">{{ __('Преподаватель') }}</label>
					<div class="col-md-9">
						<select name="teacher" class="form-control @error('teacher') is-invalid @enderror selectpicker" data-size="10" multiple data-max-options="1" data-live-search="true">
							@foreach($institutions as $institution)
								<optgroup label="{{ $institution->short_name }}" institution-id="{{ $institution->id }}">
								@foreach($teachers[$institution->id] as $teacher)
									<option value="{{ $teacher->id }}" {{ (old('teacher') == $teacher->id) ? 'selected':'' }}>{{ $teacher->name }}</option>
								@endforeach
								</optgroup>
							@endforeach
						</select>
						@error('teacher')
		                    <span class="invalid-feedback" role="alert">
		                        <strong>{{ $message }}</strong>
		                    </span>
		                @enderror
					</div>
				</div>

				<div class="form-group row m-b-15" id="subject-block">
					<label class="col-form-label col-md-3">{{ __('Предмет') }}</label>
					<div class="col-md-9">
						<select name="subject" class="form-control @error('subject') is-invalid @enderror selectpicker" data-size="10" data-live-search="true">
							@foreach($subjects as $subject)
								<option value="{{ $subject->id }}" {{ (old('subject') == $subject->id) ? 'selected':'' }}>{{ $subject->name }}</option>
							@endforeach
						</select>
						@error('subject')
		                    <span class="invalid-feedback" role="alert">
		                        <strong>{{ $message }}</strong>
		                    </span>
		                @enderror
					</div>
				</div>

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Тема') }}</label>
					<div class="col-md-9">
						<input type="text" name="topic" class="form-control m-b-5 @error('topic') is-invalid @enderror" value="{{ old('topic') }}" placeholder="{{ __('Введите тему урока...') }}" >
						@error('topic')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>

				<div class="form-group row m-b-15" id="start-block">
					<label class="col-form-label col-md-3">{{ __('Начало') }}</label>
					<div class="col-md-9">
						<div class="input-group date" id="datetimepicker">
							<input type="text" name="start" value="{{ old('start') }}" class="form-control" />
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
						</div>
						@error('subject')
		                    <span class="invalid-feedback" role="alert">
		                        <strong>{{ $message }}</strong>
		                    </span>
		                @enderror
					</div>
				</div>

				<div class="form-group row">
					<div class="col-md-7 offset-md-3">
						<button type="submit" class="btn btn-lg btn-primary m-r-5">{{ __('Добавить урок') }}</button>
					</div>
				</div>
			</form>
		</div>
		<!-- end panel-body -->
	</div>
	<!-- end panel -->
@endsection


@push('scripts')

	<script src="/assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
	<script src="/assets/plugins/bootstrap-select/dist/js/i18n/defaults-ru_RU.js"></script>

	<script src="/assets/plugins/moment/moment.js"></script>	
	<script src="/assets/plugins/moment/locale/ru.js"></script>	
	<script src="/assets/plugins/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

	<script src="/assets/js/schedule/lesson.min.js?ver={{ config('app.version') }}"></script>
@endpush