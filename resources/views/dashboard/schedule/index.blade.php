@extends('layouts.default')

@section('title', __('Расписание'))

@push('css')
	<link href="/assets/plugins/jvectormap-next/jquery-jvectormap.css" rel="stylesheet" />
	<link href="/assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right">
		<li class="breadcrumb-item">{{ __('Личный кабинет') }}</li>
		<li class="breadcrumb-item active">{{ __('Расписание') }}</li>
	</ol>

	<h1 class="page-header">{{ __('Расписание') }} <small>{{ __('Школьное расписание') }}</small></h1>
	
	@group('admin')
	<div class="card mb-3">
    	<div class="card-body">
    		<form method="GET" action="">
				<div class="form-group">
					<label>{{ __('Учебное учреждение') }}</label>
					<select class="form-control" name="institution" id="institution">
						@foreach($institutions as $institution)
						<option value="{{ $institution->id }}" {{ request()->institution == $institution->id ? 'selected' : '' }}>{{ $institution->short_name }}</option>
						@endforeach
					</select>
				</div>
				@if(!empty(request()->institution))
				<div class="form-group">
					<label>{{ __('Класс') }}</label>
					<select class="form-control" name="class" id="class">
						@foreach($classes as $class)
						<option value="{{ $class->id }}" {{ request()->class == $class->id ? 'selected' : '' }}>{{ $class->name }}</option>
						@endforeach
					</select>
				</div>
				@endif
				<div class="form-group m-0">
					<button type="submit" class="btn btn-primary">Выбрать</button>
				</div>
			</form>
		</div>
	</div>
	@endgroup
	@if(!auth()->user()->hasGroup('admin'))
	<div class="card mb-3">
    	<div class="card-body">
    		<form method="GET" action="">
				<div class="form-group">
					<label>{{ __('Класс') }}</label>
					<select class="form-control" name="class" id="class">
						@foreach($classes as $class)
						<option value="{{ $class->id }}" {{ request()->class == $class->id ? 'selected' : '' }}>{{ $class->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group m-0">
					<button type="submit" class="btn btn-primary">Выбрать</button>
				</div>
			</form>
		</div>
	</div>
	@endif


	@if(!empty(request()->class))
	<div class="row">
		<div class="col-xl-12">
			@foreach($classes->where('id', request()->class)->all() as $class)

			<div class="panel panel-inverse" data-sortable-id="index-1">
				<div class="panel-heading">
					<h4 class="panel-title">{{ $class->name }} {{ __('класс') }}</h4>
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
				</div>
				<div class="panel-body p-20 schedule" class-id="{{ $class->id }}">
					<div class="row">
						<div class="col-md-3 my-3">
							<div class="schedule-date datepicker-full-width overflow-y-scroll position-relative">
							</div>
						</div>
						<div class="col-md-9 my-3">
							<div class="schedule-table table-responsive"></div>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
	@endif

	<div class="modal fade" id="recordModal" record-link="{{ route('remotelearning.record') }}">
		<div class="modal-dialog" style="max-width: 800px;">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">{{ __('Запись') }}</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Закрыть') }}</button>
				</div>
			</div>
		</div>
	</div>
	
@endsection

@push('scripts')
	<script src="/assets/plugins/moment/moment.js"></script>	
	<script src="/assets/plugins/moment/locale/ru.js"></script>	
	<script src="/assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
	<script src="/assets/js/schedule/main.min.js?ver={{ config('app.version') }}"></script>
@endpush
