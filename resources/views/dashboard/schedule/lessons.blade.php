<table class="table table-striped table-bordered table-td-valign-middle">
	<thead>
		<tr>
			<th width="1%">№</th>
			<th class="text-nowrap">Время</th>
			<th class="text-nowrap">Предмет</th>
			<th class="text-nowrap">Преподаватель</th>
			<th class="text-nowrap">Написать преподавателю</th>
			<th class="text-nowrap">Архив</th>
			<th class="text-nowrap">Тема урока</th>
		</tr>
	</thead>
	<tbody>
		@foreach($lessons as $lesson)
		<tr>
			<td width="1%" class="f-s-600 text-inverse">{{ $lesson->number }}</td>
			<td>{{ $lesson->time }}</td>
			<td>{{ $lesson->subject->name }}</td>
			<td>
				<div class="d-flex align-items-center">
					<img src="{{ $lesson->teacher->icon_url }}" class="img-rounded m-r-20 height-30" /> 
					<span>{{ $lesson->teacher->name }}</span>
				</div>
			</td>
			<td>
				<div class="contact-email">
					<span>{{ $lesson->teacher->correct_email }}</span>
					@can('use', App\Mail::class) 
					<a href="{{ route('mail.compose', ['email' => $lesson->teacher->correct_email]) }}" class="btn btn-sm btn-primary m-l-20">НАПИСАТЬ</a>
					@endcan
				</div>
			</td>
			<td>
				<div class="d-flex align-items-center justify-content-between">
					@if(!empty($lesson->recording) && $lesson->recording->status == 'finished')
					<span style="white-space: nowrap">{{ __('Урок завершен') }}</span>
					<button href="javascript:;" style="white-space: nowrap" class="btn btn-sm btn-primary m-l-20" onclick="showRecord({{ $lesson->id }})">Запись доступна</button>
					@else
					<span style="white-space: nowrap">{{ __('Урок не завершен') }}</span>
					<button href="javascript:;" style="white-space: nowrap" class="btn btn-sm btn-primary m-l-20" disabled>Запись не доступна</button>
					@endif
				</div>
			</td>
			<td>{{ $lesson->topic ?? '- - -' }}</td>
		</tr>
		@endforeach
		@if($lessons->isEmpty())
		<tr class="odd"><td valign="top" colspan="7" class="dataTables_empty">Расписание отсутствует.</td></tr>
		@endif
	</tbody>
</table>