@extends('layouts.default')

@section('title', __('Редактирование проекта'))

@push('css')
	<link href="/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
	<link href="/assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right">
		<li class="breadcrumb-item">{{ __('Личный кабинет') }}</li>
		<li class="breadcrumb-item">{{ __('Проекты') }}</li>
		<li class="breadcrumb-item active">{{ __('Редактирование проекта') }}</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">{{ __('Редактирование проекта') }}</h1>
	<!-- end page-header -->
	
	@if (session('status'))
	    <div class="alert alert-success fade show">
	        {{ session('status') }}
	    </div>
	@endif

	<div class="panel panel-inverse">
		<!-- begin panel-body -->
		<div class="panel-body">
			<form action="{{ route('project.edit', $project->id) }}" enctype="multipart/form-data" method="POST">
				@csrf
				@method('PUT')
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Название') }}</label>
					<div class="col-md-9">
						<input type="text" name="name" class="form-control m-b-5 @error('name') is-invalid @enderror" value="{{ old('name', $project->name) }}" placeholder="{{ __('Введите название...') }}" >
						@error('name')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Статус') }}</label>
					<div class="col-md-9">
						<select name="status" class="form-control @error('status') is-invalid @enderror">
							<option value="0" {{ (old('status', $project->status) === 0) ? 'selected' : '' }}>{{ __('В работе') }}</option>
							<option value="1" {{ (old('status', $project->status) === 1) ? 'selected' : '' }}>{{ __('Доступен') }}</option>
						</select>
						@error('status')
		                    <span class="invalid-feedback" role="alert">
		                        <strong>{{ $message }}</strong>
		                    </span>
		                @enderror
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Описание') }}</label>
					<div class="col-md-9">
						<textarea class="form-control editor @error('content') is-invalid @enderror" name="content">{{ old('content', $project->content) }}</textarea>
						@error('content')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>
				<div class="form-group row m-b-15">
				    <label class="col-form-label col-md-3">{{ __('Картинки') }}</label>
				    <div class="col-md-9">
				    	<div class="m-b-5">
				    		@if($project->images && isset($project->images['image1']))
					    	<div class="form-check m-b-10">
								<img src="/storage/{{$project->images['image1']}}" width="100px" height="auto">
							</div>
						    <div class="form-check m-b-10">
								<input type="checkbox" name="del_images[image1]" value="1" id="delete-image1" class="form-check-input">
								<label class="form-check-label" for="delete-image1">{{ __('Удалить картинку') }}</label>
							</div>
							@endif
				    		<input type="file" name="images[image1]" class="form-control-file @error('images.image1') is-invalid @enderror">
				    		@error('images.image1')
		                        <span class="invalid-feedback" role="alert">
		                            <strong>{{ $message }}</strong>
		                        </span>
		                    @enderror
				    	</div>
				    	<hr>
				    	<div class="m-b-5">
				    		@if($project->images && isset($project->images['image2']))
					    	<div class="form-check m-b-10">
								<img src="/storage/{{$project->images['image2']}}" width="100px" height="auto">
							</div>
						    <div class="form-check m-b-10">
								<input type="checkbox" name="del_images[image2]" value="1" id="delete-image2" class="form-check-input">
								<label class="form-check-label" for="delete-image2">{{ __('Удалить картинку') }}</label>
							</div>
							@endif
				    		<input type="file" name="images[image2]" class="form-control-file @error('images.image2') is-invalid @enderror">
				    		@error('images.image2')
		                        <span class="invalid-feedback" role="alert">
		                            <strong>{{ $message }}</strong>
		                        </span>
		                    @enderror
				    	</div>
				    	<hr>
				    	<div class="m-b-5">
				    		@if($project->images && isset($project->images['image3']))
				    		<div class="form-check m-b-10">
								<img src="/storage/{{$project->images['image3']}}" width="100px" height="auto">
							</div>
						    <div class="form-check m-b-10">
								<input type="checkbox" name="del_images[image3]" value="1" id="delete-image3" class="form-check-input">
								<label class="form-check-label" for="delete-image3">{{ __('Удалить картинку') }}</label>
							</div>
							@endif
				    		<input type="file" name="images[image3]" class="form-control-file @error('images.image3') is-invalid @enderror">
				    		@error('images.image3')
		                        <span class="invalid-feedback" role="alert">
		                            <strong>{{ $message }}</strong>
		                        </span>
		                    @enderror
				    	</div>
				    </div>
				</div>

				<div class="form-group row m-b-15" id="platform-block">
					<label class="col-form-label col-md-3">{{ __('Достижение') }}</label>
					<div class="col-md-9">
						<select name="achivement" class="form-control @error('achivement') is-invalid @enderror selectpicker" data-live-search="true">
							@foreach($achivements as $achivement)
								<option value="{{ $achivement->id }}" {{ (old('achivement', $project->achivement_id) == $achivement->id) ? 'selected' :'' }}>{{ $achivement->name }}</option>
							@endforeach
						</select>
						@error('achivement')
		                    <span class="invalid-feedback" role="alert">
		                        <strong>{{ $message }}</strong>
		                    </span>
		                @enderror
					</div>
				</div>

				<div class="form-group row m-b-15" id="platform-block">
					<label class="col-form-label col-md-3">{{ __('Платформы') }}</label>
					<div class="col-md-9">
						<select multiple="" name="platforms[]" class="form-control @error('platforms') is-invalid @enderror selectpicker" data-size="10" data-live-search="true">
							@foreach($platforms as $platform)
								<option value="{{ $platform->id }}" {{ ($project->platforms->contains($platform->id)) ? 'selected' :'' }}>{{ $platform->name }}</option>
							@endforeach
						</select>
						@error('platforms')
		                    <span class="invalid-feedback" role="alert">
		                        <strong>{{ $message }}</strong>
		                    </span>
		                @enderror
					</div>
				</div>


				<div class="form-group row m-b-15" id="tag-block">
					<label class="col-form-label col-md-3">{{ __('Теги') }}</label>
					<div class="col-md-9">
						<select multiple="" name="tags[]" class="form-control @error('tags') is-invalid @enderror selectpicker" data-size="10" data-live-search="true">
							@foreach($tags as $tag)
								<option value="{{ $tag->id }}" {{ ($project->tags->contains($tag->id)) ? 'selected' :'' }}>{{ $tag->name }}</option>
							@endforeach
						</select>
						@error('tags')
		                    <span class="invalid-feedback" role="alert">
		                        <strong>{{ $message }}</strong>
		                    </span>
		                @enderror
					</div>
				</div>

				<div class="form-group row m-b-15" id="participant-block">
					<label class="col-form-label col-md-3">{{ __('Участники') }}</label>
					<div class="col-md-9">
						<select multiple="" name="participants[]" class="form-control @error('participants') is-invalid @enderror selectpicker" data-size="10" data-live-search="true">
							@foreach($teachers as $teacher)
								<option value="{{ $teacher->id }}" {{ ($project->participants->contains($teacher->id)) ? 'selected' :'' }}>{{ $teacher->name }}</option>
							@endforeach
						</select>
						@error('participants')
		                    <span class="invalid-feedback" role="alert">
		                        <strong>{{ $message }}</strong>
		                    </span>
		                @enderror
					</div>
				</div>
				<div class="form-group row">
					<div class="col-md-7 offset-md-3">
						<button type="submit" class="btn btn-lg btn-primary m-r-5">{{ __('Сохранить проект') }}</button>
					</div>
				</div>
			</form>
		</div>
		<!-- end panel-body -->
	</div>

@endsection


@push('scripts')
	<script src="/assets/js/tinymce/tinymce.min.js"></script>

	<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
	<script src="/assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
	<script src="/assets/plugins/bootstrap-select/dist/js/i18n/defaults-ru_RU.js"></script>
	<script src="/assets/js/project/main.min.js?ver={{ config('app.version') }}"></script>
@endpush