@extends('layouts.default')

@section('title', __('Проекты'))

@push('css')

@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right">
		<li class="breadcrumb-item">{{ __('Личный кабинет') }}</li>
		<li class="breadcrumb-item">{{ __('Проекты') }}</li>
	</ol>

	<h1 class="page-header">{{ __('Проекты') }} <small>{{ __('все проекты, в которых можно принять участие') }}</small></h1>

	<div class="row">
		<div class="col-xl-12" id="projects">
			@foreach($tags as $tag)
			<project-tag :tag='@json($tag)' user-id="{{ Auth::user()->id }}" tag-url="{{ route('project.tag.show', $tag->id) }}"></project-tag>
			@endforeach
		</div>
	</div>
@endsection


@push('scripts')
	<script src="/assets/js/project/main.min.js?ver={{ config('app.version') }}"></script>
@endpush