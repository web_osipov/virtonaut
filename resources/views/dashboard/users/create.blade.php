@extends('layouts.default')

@section('title', __('Добавить пользователя'))

@push('css')
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right">
		<li class="breadcrumb-item">{{ __('Личный кабинет') }}</li>
		<li class="breadcrumb-item"><a href="{{ route('user.index') }}">{{ __('Пользователи') }}</a></li>
		<li class="breadcrumb-item active">{{ __('Добавить пользователя') }}</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">{{ __('Добавить пользователя') }}</h1>
	<!-- end page-header -->
	<!-- begin panel -->
	<div class="panel panel-inverse">
		<!-- begin panel-body -->
		<div class="panel-body">
			<form action="{{ route('user.store') }}" method="POST">
				@csrf
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Имя') }}</label>
					<div class="col-md-9">
						<input type="text" name="name" class="form-control m-b-5 @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="{{ __('Введите имя...') }}" >
						@error('name')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Email') }}</label>
					<div class="col-md-9">
						<input type="email" name="email" class="form-control m-b-5 @error('email') is-invalid @enderror" value="{{ old('email') }}" placeholder="{{ __('Введите Email...') }}" >
						@error('email')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Группы') }}</label>
					<div class="col-md-9">
						<select multiple="" name="groups[]" class="form-control @error('groups') is-invalid @enderror">
							@foreach($groups as $group)
								<option value="{{ $group->id }}" {{ (collect(old('groups'))->contains($group->id)) ? 'selected':'' }}>{{ $group->name }}</option>
							@endforeach
						</select>
						@error('groups')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-sm-3 col-form-label">{{ __('Пароль') }}</label>
					<div class="col-sm-9">
						<input value="{{ old('password') }}" name="password" type="text" class="form-control @error('password') is-invalid @enderror" placeholder="{{ __('Введите пароль') }}">
						@error('password')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>
				<div class="form-group row">
					<div class="col-md-7 offset-md-3">
						<button type="submit" class="btn btn-lg btn-primary m-r-5">{{ __('Добавить пользователя') }}</button>
					</div>
				</div>
			</form>
		</div>
		<!-- end panel-body -->
	</div>
	<!-- end panel -->
@endsection


@push('scripts')
	<script src="/assets/js/users.min.js?ver={{ config('app.version') }}"></script>
@endpush