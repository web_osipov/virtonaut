<div class="tab-pane fade @if($settings_tab == 'default-settings') active show @endif" id="default-settings">
	<form action="{{ route('user.update', $user->id) }}" enctype="multipart/form-data" method="POST">
		@csrf
		{{ method_field('PATCH') }}
		<input type="hidden" name="tab" value="default-settings">
		<div class="form-group row m-b-15">
			<label class="col-form-label col-md-3">{{ __('Имя') }}</label>
			<div class="col-md-9">
				<input type="text" name="name" class="form-control m-b-5 @error('name', 'default') is-invalid @enderror" value="{{ $user->name }}" placeholder="{{ __('Введите имя...') }}" >
				@error('name', 'default')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
			</div>
		</div>

		<div class="form-group row m-b-15">
			<label class="col-form-label col-md-3">{{ __('Количество одновременных сессий') }}</label>
			<div class="col-md-9">
				<input type="text" name="session_limit" class="form-control m-b-5 @error('session_limit', 'default') is-invalid @enderror" value="{{ $user->getData('session_limit', true) }}" placeholder="{{ __('Введите количество...') }}" >
				@error('session_limit', 'default')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
			</div>
		</div>

		<div class="form-group row m-b-15">
		    <label class="col-form-label col-md-3">{{ __('Иконка') }}</label>
		    <div class="col-md-9">
		    	@if($user->icon)
		    	<div class="form-check m-b-10">
					<img src="{{$user->icon_url}}" width="100px" height="auto">
				</div>
			    <div class="form-check m-b-10">
					<input type="checkbox" name="del_icon" value="1" id="delete-icon" class="form-check-input">
					<label class="form-check-label" for="delete-icon">{{ __('Удалить иконку') }}</label>
				</div>
				@endif
		    	<input type="file" name="icon" class="form-control-file @error('icon', 'default') is-invalid @enderror">
		    	@error('icon', 'default')
	                <span class="invalid-feedback" role="alert">
	                    <strong>{{ $message }}</strong>
	                </span>
	            @enderror
		    </div>
		</div>
		<div class="form-group row m-b-15">
			<label class="col-form-label col-md-3">{{ __('Email') }}</label>
			<div class="col-md-9">
				<input type="email" name="email" class="form-control m-b-5 @error('email', 'default') is-invalid @enderror" value="{{ $user->email }}" placeholder="{{ __('Введите Email...') }}" >
				@error('email', 'default')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
			</div>
		</div>
		@if(Auth::user()->id != $user->id)
		<div class="form-group row m-b-15">
			<label class="col-form-label col-md-3">{{ __('Группы') }}</label>
			<div class="col-md-9">
				<select multiple="" name="groups[]" class="form-control @error('groups', 'default') is-invalid @enderror">
					@foreach($groups as $group)
						<option value="{{ $group->slug }}" {{ ($user->hasGroup($group->id)) ? 'selected':'' }}>{{ $group->name }}</option>
					@endforeach
				</select>
				@error('groups', 'default')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
			</div>
		</div>
		@endif

		<div class="form-group row m-b-15" id="institution-block">
			<label class="col-form-label col-md-3">{{ __('Учебное учреждение') }}</label>
			<div class="col-md-9">
				<select name="institution" class="form-control @error('institution', 'default') is-invalid @enderror selectpicker" multiple data-max-options="1" data-live-search="true">
					@foreach($institutions as $institution)
						<option value="{{ $institution->id }}" {{ ($user->institution_id == $institution->id) ? 'selected' :'' }}>{{ $institution->full_name }}</option>
					@endforeach
				</select>
				@error('institution', 'default')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
			</div>
		</div>

		<div class="form-group row m-b-15" id="classes-block">
			<label class="col-form-label col-md-3">{{ __('Классы') }}</label>
			<div class="col-md-9">
				<select multiple="" name="classes[]" class="form-control @error('classes', 'default') is-invalid @enderror selectpicker" data-size="10" data-live-search="true">
					@foreach($institutions as $institution)
						<optgroup label="{{ $institution->short_name }}" institution-id="{{ $institution->id }}">
						@foreach($classes[$institution->id] as $class)
							<option value="{{ $class->id }}" {{ ($user->hasClass($class->id)) ? 'selected':'' }}>{{ $class->name }}</option>
						@endforeach
					</optgroup>
					@endforeach
				</select>
				@error('classes', 'default')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
			</div>
		</div>

		<div class="form-group row m-b-15" id="subjects-block">
			<label class="col-form-label col-md-3">{{ __('Предметы') }}</label>
			<div class="col-md-9">
				<select multiple="" name="subjects[]" class="form-control @error('subjects', 'default') is-invalid @enderror selectpicker" data-size="10" data-live-search="true">
					@foreach($subjects as $subject)
						<option value="{{ $subject->id }}" {{ ($user->hasSubject($subject->id)) ? 'selected':'' }}>{{ $subject->name }}</option>
					@endforeach
				</select>
				@error('subjects', 'default')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
			</div>
		</div>

		<div class="form-group row m-b-15">
			<label class="col-sm-3 col-form-label">{{ __('Пароль') }}</label>
			<div class="col-sm-9">
				<input name="password" type="text" class="form-control @error('password', 'default') is-invalid @enderror" placeholder="{{ __('Введите пароль') }}">
				@error('password', 'default')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-7 offset-md-3">
				<button type="submit" class="btn btn-lg btn-primary m-r-5">{{ __('Сохранить') }}</button>
			</div>
		</div>
	</form>
</div>