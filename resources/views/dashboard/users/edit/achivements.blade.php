<div class="tab-pane fade @if($settings_tab == 'achivements-settings') active show @endif" id="achivements-settings">
	<form action="{{ route('user.update', $user->id) }}" method="POST">
		@csrf
		{{ method_field('PATCH') }}
		<input type="hidden" name="tab" value="achivements-settings">
		
		@foreach($achivement_groups as $group)
		<div class="form-group row m-b-15">
			<label class="col-form-label col-md-3">{{ $group->name }}</label>
			<div class="col-md-9">
				<select multiple="" name="achivements[]" class="form-control selectpicker" data-size="10" data-live-search="true">
					@foreach($group->achivements as $achivement)
						<option value="{{ $achivement->id }}" {{ ($user->hasAchivement($achivement->id)) ? 'selected':'' }}>{{ $achivement->name }}</option>
					@endforeach
				</select>
			</div>
		</div>
		@endforeach

		
		<div class="form-group row">
			<div class="col-md-7 offset-md-3">
				<button type="submit" class="btn btn-lg btn-primary m-r-5">{{ __('Сохранить') }}</button>
			</div>
		</div>
	</form>
</div>

@push('scripts')
	<script src="/assets/js/users/edit/achivements.min.js?ver={{ config('app.version') }}"></script>
@endpush