<div class="tab-pane fade @if($settings_tab == 'schedule-settings') active show @endif" id="schedule-settings">
	<form action="{{ route('user.update', $user->id) }}" method="POST">
		@csrf
		{{ method_field('PATCH') }}
		<input type="hidden" name="tab" value="schedule-settings">

		<div class="form-group row m-b-15">
			<label class="col-form-label col-md-3">{{ __('Разрешить редактировать расписание') }}</label>
			<div class="col-md-9">
				<div class="switcher">
				  	<input type="checkbox" name="enabled" @if($user->getData('edit-schedule-enabled', true) == 1) checked @endif id="schedule-settings-enabled" value="1">	  	
				  	<label for="schedule-settings-enabled"></label>
				</div>
			</div>
		</div>

		<div class="h4">{{ __('Ограничения') }}</div>

		<div class="form-group row m-b-15">
			<label class="col-form-label col-md-3">{{ __('Редактировать расписания других преподавателей') }}</label>
			<div class="col-md-9">
				<div class="switcher">
				  	<input type="checkbox" name="all_teachers" @if($user->getData('edit-schedule-all-teachers', true) == 1) checked @endif id="schedule-settings-all-teachers" value="1">	  	
				  	<label for="schedule-settings-all-teachers"></label>
				</div>
			</div>
		</div>
		<div class="form-group row m-b-15">
			<label class="col-form-label col-md-3">{{ __('Классы') }}</label>
			<div class="col-md-9">
				<select multiple="" name="classes[]" class="form-control @error('classes', 'default') is-invalid @enderror selectpicker" data-size="10" data-live-search="true">
					@foreach($classes as $class)
						<option value="{{ $class->id }}" {{ (in_array($class->id, (array) $user->getData('edit-schedule-classes', true))) ? 'selected':'' }}>{{ $class->name }}</option>
					@endforeach
				</select>
				@error('classes', 'default')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
			</div>
		</div>

		<div class="form-group row">
			<div class="col-md-7 offset-md-3">
				<button type="submit" class="btn btn-lg btn-primary m-r-5">{{ __('Сохранить') }}</button>
			</div>
		</div>
	</form>
</div>

@push('scripts')
	<!-- <script src="/assets/js/users/edit/schedule.min.js?ver={{ config('app.version') }}"></script>-->
@endpush