<div class="tab-pane fade @if($settings_tab == 'email-settings') active show @endif" id="email-settings">
	@if($mail_domains->isNotEmpty())
	<form action="{{ route('user.update', $user->id) }}" id="email-settings-form" method="POST">
		@csrf
		{{ method_field('PATCH') }}
		<input type="hidden" name="tab" value="email-settings">

		<div class="form-group row m-b-15">
			<label class="col-form-label col-md-3">{{ __('Включить почту для аккаунта') }}</label>
			<div class="col-md-9">
				<div class="switcher">
				  	<input type="checkbox" name="active" @if(optional($mail)->active == 1) checked @endif id="email-settings-form-active" value="1">	  	
				  	<label for="email-settings-form-active"></label>
				</div>
			</div>
		</div>
		<div class="form-group row m-b-15">
			<label class="col-form-label col-md-3">{{ __('Почтовый ящик') }}</label>
			<div class="col-md-9">
				<div class="input-group">
					<input class="form-control @error('username', 'email') is-invalid @enderror @error('domain', 'email') is-invalid @enderror" name="username" value="{{ optional($mail)->username }}" type="text" />
					<select name="domain" class="form-control">
						@foreach($mail_domains as $mail_domain)
						<option value="{{ $mail_domain->id }}" @if($mail_domain->id == optional(optional($mail)->domain)->id) selected @endif>{{ '@'.$mail_domain->domain }}</option>
						@endforeach
					</select>
				</div>
				@error('username', 'email')
                    <span class="invalid-feedback d-block" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                @error('domain', 'email')
                    <span class="invalid-feedback d-block" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
			</div>
		</div>
		
		<div class="form-group row">
			<div class="col-md-7 offset-md-3">
				<button type="submit" class="btn btn-lg btn-primary m-r-5">{{ __('Сохранить') }}</button>
			</div>
		</div>
	</form>
	@else
	<div class="alert alert-danger fade show mb-0">
		<a href="{{ route('settings.mail') }}" class="alert-link">{{ __('Добавьте почтовый домен') }}</a> 
	</div>
	@endif
</div>