@extends('layouts.default')

@section('title', __('Редактирование пользователя'))

@push('css')
	<link href="/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
	<link href="/assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right">
		<li class="breadcrumb-item">{{ __('Личный кабинет') }}</li>
		<li class="breadcrumb-item"><a href="{{ route('user.index') }}">{{ __('Пользователи') }}</a></li>
		<li class="breadcrumb-item active">{{ __('Редактирование пользователя') }}</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">{{ __('Редактирование пользователя') }}</h1>
	<!-- end page-header -->
	
	@if (session('status'))
	    <div class="alert alert-success fade show">
	        {{ session('status') }}
	    </div>
	@endif

	<ul class="nav nav-tabs">
		<li class="nav-item">
			<a href="{{ route('user.edit', ['user' => $user->id, 'tab' => 'default-settings']) }}" class="nav-link @if($settings_tab == 'default-settings') active @endif">
				<span class="d-sm-none">{{ __('Настройки аккаунта') }}</span>
				<span class="d-sm-block d-none">{{ __('Настройки аккаунта') }}</span>
			</a>
		</li>
		<li class="nav-item">
			<a href="{{ route('user.edit', ['user' => $user->id, 'tab' => 'email-settings']) }}" class="nav-link @if($settings_tab == 'email-settings') active @endif">
				<span class="d-sm-none">{{ __('Настройки Email') }}</span>
				<span class="d-sm-block d-none">{{ __('Настройки Email') }}</span>
			</a>
		</li>
		<li class="nav-item">
			<a href="{{ route('user.edit', ['user' => $user->id, 'tab' => 'schedule-settings']) }}" class="nav-link @if($settings_tab == 'schedule-settings') active @endif">
				<span class="d-sm-none">{{ __('Настройки расписания') }}</span>
				<span class="d-sm-block d-none">{{ __('Настройки расписания') }}</span>
			</a>
		</li>
		<li class="nav-item">
			<a href="{{ route('user.edit', ['user' => $user->id, 'tab' => 'achivements-settings']) }}" class="nav-link @if($settings_tab == 'achivements-settings') active @endif">
				<span class="d-sm-none">{{ __('Достижения') }}</span>
				<span class="d-sm-block d-none">{{ __('Достижения') }}</span>
			</a>
		</li>
	</ul>
	<div class="tab-content">

		@includeWhen($settings_tab == 'default-settings', 'dashboard.users.edit.default')

		@includeWhen($settings_tab == 'email-settings', 'dashboard.users.edit.email')

		@includeWhen($settings_tab == 'schedule-settings', 'dashboard.users.edit.schedule')

		@includeWhen($settings_tab == 'achivements-settings', 'dashboard.users.edit.achivements')

	</div>
@endsection


@push('scripts')
	<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
	<script src="/assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
	<script src="/assets/plugins/bootstrap-select/dist/js/i18n/defaults-ru_RU.js"></script>
	<script src="/assets/js/users.min.js?ver={{ config('app.version') }}"></script>

	@if($settings_tab == 'default-settings')
		<script src="/assets/js/users/edit/default.min.js?ver={{ config('app.version') }}"></script>
	@endif
@endpush