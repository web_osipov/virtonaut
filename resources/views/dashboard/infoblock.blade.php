@extends('layouts.default')

@section('title', __('Инфоблок'))

@push('css')
	<link href="/assets/plugins/jvectormap-next/jquery-jvectormap.css" rel="stylesheet" />
	<link href="/assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.css" rel="stylesheet" />
	<link href="/assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" />

	<link href="/assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
	<link href="/assets/plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right">
		<li class="breadcrumb-item">{{ __('Личный кабинет') }}</li>
		<li class="breadcrumb-item active">{{ __('Инфоблок') }}</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">{{ __('Инфоблок') }} <small>{{ __('все самое главное здесь') }}</small></h1>
	<!-- end page-header -->
	
	<!-- begin row -->
	@group('teacher')
		@include('dashboard.infoblock.teacher_widgets')
	@elsegroup('student')
		@include('dashboard.infoblock.student_widgets')
	@endgroup
	<!-- end row -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-xl-12">
			@group('teacher')
				@include('dashboard.infoblock.teacher_achivements')
			@elsegroup('student')
				@include('dashboard.infoblock.student_achivements')
			@endgroup

			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="index-2">
				<div class="panel-heading">
					<h4 class="panel-title">Избранные контакты</h4>
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					</div>
				</div>
				<div class="panel-body">
					<table id="data-table-contacts" class="table table-striped table-bordered table-td-valign-middle">
						<thead>
							<tr>
								<th width="1%">№</th>
								<th width="1%" data-orderable="false"></th>
								<th class="text-nowrap">Ф.И.О.</th>
								<th class="text-nowrap">Почта</th>
								<th class="text-nowrap">Образовательное учреждение</th>
								<th class="text-nowrap">Статус</th>
								<th class="text-nowrap">Должность</th>
							</tr>
						</thead>
						<tbody>
							@foreach($contacts as $key => $contact)
							<tr>
								<td width="1%" class="f-s-600 text-inverse">{{ $key + 1 }}</td>
								<td width="1%" class="with-img">
									<img src="{{ $contact->icon_url }}" class="img-rounded height-30" />
								</td>
								<td>{{ $contact->name }}</td>
								<td>
									<div class="contact-email">
										<span>{{ $contact->correct_email }}</span>
										@if($contact->enabledEmail())
										@can('use', App\Mail::class)
										<a href="{{ route('mail.compose', ['email' => $contact->correct_email]) }}" class="btn btn-sm btn-primary m-l-20">НАПИСАТЬ</a>
										@endcan
										@endif
									</div>
								</td>
								<td>{{ $contact->institution ? $contact->institution->full_name : '- - -' }}</td>
								<td class="{{ $contact->is_online ? 'online' : 'offline' }}">
									<span>{{ $contact->is_online ? 'В сети' : 'Не в сети' }}</span>
								</td>
								<td>
									@if($contact->hasGroup('principal'))
									<div>{{ __('Директор') }}</div>
									@endif
									@if($contact->hasGroup('support'))
									<div>{{ __('Техподдержка') }}</div>
									@endif
									@if($contact->hasGroup('teacher'))
									<div>{{ __('Преподаватель') }} 
									{{ $contact->subjects ? $contact->subjects->pluck('name')->join(', ') : '' }}
									@endif
									@if($contact->hasGroup('student'))</div>
									<div>{{ __('Ученик') }}</div>
									@endif
									@if($contact->hasGroup('admin'))
									<div>{{ __('Администратор') }}</div>
									@endif 
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
			<!-- end panel -->
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->
@endsection

@push('scripts')
	<script src="/assets/plugins/gritter/js/jquery.gritter.js"></script>
	<script src="/assets/plugins/flot/jquery.flot.js"></script>
	<script src="/assets/plugins/flot/jquery.flot.time.js"></script>
	<script src="/assets/plugins/flot/jquery.flot.resize.js"></script>
	<script src="/assets/plugins/flot/jquery.flot.pie.js"></script>
	<script src="/assets/plugins/jquery-sparkline/jquery.sparkline.min.js"></script>
	<script src="/assets/plugins/jvectormap-next/jquery-jvectormap.min.js"></script>
	<script src="/assets/plugins/jvectormap-next/jquery-jvectormap-world-mill.js"></script>
	<script src="/assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
	<!-- <script src="/assets/js/demo/dashboard.js?v=2"></script> -->

	<script src="/assets/plugins/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="/assets/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
	<script src="/assets/plugins/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
	<script src="/assets/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
	<script src="/assets/plugins/datatables.net-autofill/js/dataTables.autofill.min.js"></script>
	<script src="/assets/plugins/datatables.net-autofill-bs4/js/autofill.bootstrap4.min.js"></script>

@endpush
