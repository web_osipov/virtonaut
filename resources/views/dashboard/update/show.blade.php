@extends('layouts.default')

@section('title', __('Обновления'))

@push('css')
	<link href="/assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
	<link href="/assets/plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right">
		<li class="breadcrumb-item">{{ __('Личный кабинет') }}</li>
		<li class="breadcrumb-item">{{ __('Лицензия') }}</li>
	</ol>

	<h1 class="page-header">
		{{ __('Обновления') }}
		<small>Актуальные обновления экосистемы</small>
	</h1>

	<ul class="timeline">
		@foreach($updates as $update)
		<li>
			<div class="timeline-time">
				<span class="time">{{ $update->date->format('d.m.Y') }}</span>
			</div>

			<div class="timeline-icon">
				<a href="javascript:;">&nbsp;</a>
			</div>

			<div class="timeline-body">
				<div class="timeline-header">
					<span class="userimage"><img src="{{ $update->author->icon_url }}" alt=""></span>
					<span class="username">{{ $update->author->name }}<small></small></span>
				</div>
				<div class="timeline-content">
					{!! $update->description !!}
				</div>
			</div>

		</li>
		@endforeach
	</ul>

	<div class="mt-4 pagination-block">{{ $updates->links() }}</div>
	

@endsection


@push('scripts')
	<script src="/assets/plugins/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="/assets/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
	<script src="/assets/plugins/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
	<script src="/assets/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
	<script src="/assets/plugins/datatables.net-autofill/js/dataTables.autofill.min.js"></script>
	<script src="/assets/plugins/datatables.net-autofill-bs4/js/autofill.bootstrap4.min.js"></script>


	<script src="/assets/js/update/main.min.js?ver={{ config('app.version') }}"></script>
@endpush