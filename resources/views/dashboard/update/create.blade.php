@extends('layouts.default')

@section('title', __('Добавить'))

@push('css')
	<link href="/assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
	<link href="/assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
	<link href="/assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right">
		<li class="breadcrumb-item">{{ __('Личный кабинет') }}</li>
		<li class="breadcrumb-item">{{ __('Обновления') }}</li>
		<li class="breadcrumb-item active">{{ __('Добавить') }}</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">{{ __('Добавить') }}</h1>
	<!-- end page-header -->
	<!-- begin panel -->
	<div class="panel panel-inverse">
		<!-- begin panel-body -->
		<div class="panel-body">
			<form action="{{ route('update.create') }}" enctype="multipart/form-data" method="POST">
				@csrf
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Активировано') }}</label>
					<div class="col-md-9">
						<div class="switcher">
							<input type="checkbox" name="active" id="update-form-active" value="1">	  	
							<label for="update-form-active"></label>
						</div>
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Описание') }}</label>
					<div class="col-md-9">
						<textarea class="form-control editor" name="description">{{ old('description') }}</textarea>
						@error('description')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>
			
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Группы пользователей') }}</label>
					<div class="col-md-9">
						<select multiple="" name="groups[]" class="form-control @error('groups') is-invalid @enderror">
							@foreach($groups as $group)
								<option value="{{ $group->id }}" {{ (collect(old('groups'))->contains($group->id)) ? 'selected':'' }}>{{ $group->name }}</option>
							@endforeach
						</select>
						@error('groups')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>

				<div class="form-group row m-b-15" id="date-block">
					<label class="col-form-label col-md-3">{{ __('Дата') }}</label>
					<div class="col-md-9">
						<div class="input-group date @error('date') is-invalid @enderror" id="datetimepicker">
							<input type="text" name="date" value="{{ old('date') }}" class="form-control" />
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
						</div>
						@error('date')
		                    <span class="invalid-feedback" role="alert">
		                        <strong>{{ $message }}</strong>
		                    </span>
		                @enderror
					</div>
				</div>

				<div class="form-group row">
					<div class="col-md-7 offset-md-3">
						<button type="submit" class="btn btn-lg btn-primary m-r-5">{{ __('Добавить') }}</button>
					</div>
				</div>
			</form>
		</div>
		<!-- end panel-body -->
	</div>
	<!-- end panel -->
@endsection


@push('scripts')
	<script src="/assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
	<script src="/assets/plugins/bootstrap-select/dist/js/i18n/defaults-ru_RU.js"></script>

	<script src="/assets/js/tinymce/tinymce.min.js"></script>

	<script src="/assets/plugins/moment/moment.js"></script>	
	<script src="/assets/plugins/moment/locale/ru.js"></script>	
	<script src="/assets/plugins/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

	<script src="/assets/js/update/main.min.js?ver={{ config('app.version') }}"></script>
@endpush