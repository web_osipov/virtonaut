@extends('layouts.default')

@section('title', __('Достижения'))

@push('css')
	<link href="/assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
	<link href="/assets/plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right">
		<li class="breadcrumb-item">{{ __('Личный кабинет') }}</li>
		<li class="breadcrumb-item">{{ __('Достижения') }}</li>
	</ol>

	<h1 class="page-header">{{ __('Достижения') }} <small>{{ __('все о ваших достижениях в проекте') }}</small></h1>

	<div class="row">
		<div class="col-xl-12" id="achivements">
			@foreach($achivement_groups as $achivement_group)
			<div class="panel panel-inverse">
		        <div class="panel-heading">
		            <h4 class="panel-title">{{ $achivement_group->name }}</h4>
		        </div>
		        <div class="panel-body p-20">
		        	<table class="datatables table table-striped table-bordered table-td-valign-middle">
						<thead>
							<tr>
								<th width="1%">№</th>
								<th width="1%" data-orderable="false">Достижение</th>
								<th class="text-nowrap">Статус</th>
								<th class="text-nowrap">Описание</th>
								<th class="text-nowrap">Дата присвоения</th>
							</tr>
						</thead>
						<tbody>
							@foreach($achivement_group->sorted_achivements as $key => $achivement)
							<tr>
								<td width="1%" class="f-s-600 text-inverse">{{ $key + 1 }}</td>
								<td width="1%" class="with-img">
									@if(isset($user_achivements[$achivement->id]))
									<img src="{{ $achivement->icon_url }}" class="height-80 width-80 m-10" />
									@else
									<img src="{{ $achivement->unactive_icon_url }}" class="height-80 width-80 m-10" />
									@endif
								</td>
								<td>{{ $achivement->name }}</td>
								<td>{{ $achivement->description ? $achivement->description : '- - -' }}</td>
								<td>{{ isset($user_achivements[$achivement->id]) ? $user_achivements[$achivement->id]['received'] : '- - -' }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
		        </div>
		    </div>
			@endforeach
		</div>
	</div>
@endsection


@push('scripts')
	<script src="/assets/plugins/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="/assets/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
	<script src="/assets/plugins/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
	<script src="/assets/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
	<script src="/assets/plugins/datatables.net-autofill/js/dataTables.autofill.min.js"></script>
	<script src="/assets/plugins/datatables.net-autofill-bs4/js/autofill.bootstrap4.min.js"></script>

	<script src="/assets/js/achivements/main.min.js?ver={{ config('app.version') }}"></script>
@endpush