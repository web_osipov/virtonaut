@extends('layouts.default')

@section('title', __('Настройки дизайна'))

@push('css')

@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right">
		<li class="breadcrumb-item">{{ __('Личный кабинет') }}</li>
		<li class="breadcrumb-item active">{{ __('Настройки дизайна') }}</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">{{ __('Настройки дизайна') }}</h1>
	<!-- end page-header -->
	
	<div class="panel panel-inverse">
		<!-- begin panel-body -->
		<div class="panel-body">
			<form action="{{ route('settings.design') }}" enctype="multipart/form-data" method="POST">
				@csrf

				<div class="form-group row m-b-15">
				    <label class="col-form-label col-md-3">{{ __('Картинка навигационного меню') }}</label>
				    <div class="col-md-9">
				    	@if(!empty($settings['nav_bg']))
				    	<div class="form-check m-b-10">
							<img src="/storage/{{$settings['nav_bg']}}" width="100px" height="auto">
						</div>
					    <div class="form-check m-b-10">
							<input type="checkbox" name="del_nav_bg" value="1" id="delete-nav_bg" class="form-check-input">
							<label class="form-check-label" for="delete-nav_bg">{{ __('Удалить картинку') }}</label>
						</div>
						@endif
				    	<input type="file" name="nav_bg" class="form-control-file @error('nav_bg') is-invalid @enderror">
				    	@error('icon')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
				    </div>
				</div>

				<div class="form-group row">
					<div class="col-md-7 offset-md-3">
						<button type="submit" class="btn btn-lg btn-primary m-r-5">{{ __('Сохранить') }}</button>
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection

@push('scripts')
	
@endpush
