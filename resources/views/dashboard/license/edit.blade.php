@extends('layouts.default')

@section('title', __('Редактирование лицензии'))

@push('css')
	<link href="/assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
	<link href="/assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
	<link href="/assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right">
		<li class="breadcrumb-item">{{ __('Личный кабинет') }}</li>
		<li class="breadcrumb-item">{{ __('Лицензия') }}</li>
		<li class="breadcrumb-item active">{{ __('Редактирование лицензии') }}</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">{{ __('Редактирование лицензии') }}</h1>
	<!-- end page-header -->
	
	@if (session('status'))
	    <div class="alert alert-success fade show">
	        {{ session('status') }}
	    </div>
	@endif

	<div class="panel panel-inverse">
		<!-- begin panel-body -->
		<div class="panel-body">
			<form action="{{ route('license.edit', $license->id) }}" enctype="multipart/form-data" method="POST">
				@csrf
				@method('PUT')
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Название') }}</label>
					<div class="col-md-9">
						<input type="text" name="name" class="form-control m-b-5 @error('name') is-invalid @enderror" value="{{ old('name', $license->name) }}" placeholder="{{ __('Введите название...') }}" >
						@error('name')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Описание') }}</label>
					<div class="col-md-9">
						<textarea class="form-control editor" name="description">{{ old('description', $license->description) }}</textarea>
						@error('description')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>
			
				<div class="form-group row m-b-15" id="platform-block">
					<label class="col-form-label col-md-3">{{ __('Тип') }}</label>
					<div class="col-md-9">
						<select name="type" class="form-control @error('type') is-invalid @enderror selectpicker" data-live-search="true">
							@foreach($types as $type)
								<option value="{{ $type->id }}" {{ ($license->type->contains($type->id))  ? 'selected' :'' }}>{{ $type->name }}</option>
							@endforeach
						</select>
						@error('type')
		                    <span class="invalid-feedback" role="alert">
		                        <strong>{{ $message }}</strong>
		                    </span>
		                @enderror
					</div>
				</div>

				<div class="form-group row m-b-15" id="platform-block">
					<label class="col-form-label col-md-3">{{ __('Учебный класс') }}</label>
					<div class="col-md-9">
						<select name="classroom" class="form-control @error('classroom') is-invalid @enderror selectpicker" data-live-search="true">
							@foreach($classrooms as $classroom)
								<option value="{{ $classroom->id }}" {{ (($license->classroom->contains($classroom->id)) == $classroom->id) ? 'selected' : '' }}>{{ $classroom->name }} ({{ $classroom->institution->pluck('short_name')->join(', ') }})</option>
							@endforeach
						</select>
						@error('classroom')
		                    <span class="invalid-feedback" role="alert">
		                        <strong>{{ $message }}</strong>
		                    </span>
		                @enderror
					</div>
				</div>

				<div class="form-group row m-b-15" id="expires-block">
					<label class="col-form-label col-md-3">{{ __('Дата окончания') }}</label>
					<div class="col-md-9">
						<div class="input-group date @error('expires') is-invalid @enderror" id="datetimepicker">
							<input type="text" name="expires" value="{{ old('expires', $expires) }}" class="form-control" />
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
						</div>
						@error('expires')
		                    <span class="invalid-feedback" role="alert">
		                        <strong>{{ $message }}</strong>
		                    </span>
		                @enderror
					</div>
				</div>

				<div class="form-group row">
					<div class="col-md-7 offset-md-3">
						<button type="submit" class="btn btn-lg btn-primary m-r-5">{{ __('Сохранить лицензию') }}</button>
					</div>
				</div>
			</form>
		</div>
		<!-- end panel-body -->
	</div>

@endsection


@push('scripts')
	<script src="/assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
	<script src="/assets/plugins/bootstrap-select/dist/js/i18n/defaults-ru_RU.js"></script>

	<script src="/assets/js/tinymce/tinymce.min.js"></script>

	<script src="/assets/plugins/moment/moment.js"></script>	
	<script src="/assets/plugins/moment/locale/ru.js"></script>	
	<script src="/assets/plugins/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

	<script src="/assets/js/license/main.min.js?ver={{ config('app.version') }}"></script>
@endpush