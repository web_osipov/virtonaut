@extends('layouts.default')

@section('title', __('Редактирование типа'))

@push('css')
	<link href="/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
	<link href="/assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right">
		<li class="breadcrumb-item">{{ __('Личный кабинет') }}</li>
		<li class="breadcrumb-item">{{ __('Лицензия') }}</li>
		<li class="breadcrumb-item">{{ __('Типы') }}</li>
		<li class="breadcrumb-item active">{{ __('Редактирование типа') }}</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">{{ __('Редактирование типа') }}</h1>
	<!-- end page-header -->
	
	@if (session('status'))
	    <div class="alert alert-success fade show">
	        {{ session('status') }}
	    </div>
	@endif

	<div class="panel panel-inverse">
		<!-- begin panel-body -->
		<div class="panel-body">
			<form action="{{ route('license.type.edit', $type->id) }}" enctype="multipart/form-data" method="POST">
				@csrf
				@method('PUT')
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Название') }}</label>
					<div class="col-md-9">
						<input type="text" name="name" class="form-control m-b-5 @error('name') is-invalid @enderror" value="{{ old('name', $type->name) }}" placeholder="{{ __('Введите название...') }}" >
						@error('name')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>

				<div class="form-group row m-b-15">
				    <label class="col-form-label col-md-3">{{ __('Иконка') }}</label>
				    <div class="col-md-9">
				    	@if($type->icon)
				    	<div class="form-check m-b-10">
							<img src="{{$type->icon_url}}" width="100px" height="auto">
						</div>
					    <div class="form-check m-b-10">
							<input type="checkbox" name="del_icon" value="1" id="delete-icon" class="form-check-input">
							<label class="form-check-label" for="delete-icon">{{ __('Удалить иконку') }}</label>
						</div>
						@endif
				    	<input type="file" name="icon" class="form-control-file @error('icon') is-invalid @enderror">
				    	@error('icon')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
				    </div>
				</div>

				<div class="form-group row m-b-15" id="blocks-block">
					<label class="col-form-label col-md-3">{{ __('Блоки') }}</label>
					<div class="col-md-9">
						<select multiple="" name="blocks[]" class="form-control @error('blocks') is-invalid @enderror selectpicker" data-size="10" data-live-search="true">
							@foreach(App\Helpers\LicenseHelper::getBlocks() as $block_id => $block)
								<option value="{{ $block_id }}" {{ (collect(old('blocks', $type->blocks))->contains($block_id)) ? 'selected':'' }}>{{ $block['name'] }}</option>
							</optgroup>
							@endforeach
						</select>
						@error('blocks')
		                    <span class="invalid-feedback" role="alert">
		                        <strong>{{ $message }}</strong>
		                    </span>
		                @enderror
					</div>
				</div>

				<div class="form-group row">
					<div class="col-md-7 offset-md-3">
						<button type="submit" class="btn btn-lg btn-primary m-r-5">{{ __('Сохранить тип') }}</button>
					</div>
				</div>
			</form>
		</div>
		<!-- end panel-body -->
	</div>

@endsection


@push('scripts')
	<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
	<script src="/assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
	<script src="/assets/plugins/bootstrap-select/dist/js/i18n/defaults-ru_RU.js"></script>

	<script src="/assets/js/license/type.min.js?ver={{ config('app.version') }}"></script>
@endpush