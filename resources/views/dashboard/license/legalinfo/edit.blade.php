@extends('layouts.default')

@section('title', __('Редактирование'))

@push('css')
	<link href="/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
	<link href="/assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right">
		<li class="breadcrumb-item">{{ __('Личный кабинет') }}</li>
		<li class="breadcrumb-item">{{ __('Лицензия') }}</li>
		<li class="breadcrumb-item">{{ __('Правовая информация') }}</li>
		<li class="breadcrumb-item active">{{ __('Редактирование') }}</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">{{ __('Редактирование') }}</h1>
	<!-- end page-header -->
	
	@if (session('status'))
	    <div class="alert alert-success fade show">
	        {{ session('status') }}
	    </div>
	@endif

	<div class="panel panel-inverse">
		<!-- begin panel-body -->
		<div class="panel-body">
			<form action="{{ route('license.legalinfo.edit', $legalinfo->id) }}" enctype="multipart/form-data" method="POST">
				@csrf
				@method('PUT')

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Наименование') }}</label>
					<div class="col-md-9">
						<input type="text" name="name" class="form-control m-b-5 @error('name') is-invalid @enderror" value="{{ old('name', $legalinfo->name) }}" placeholder="{{ __('Введите название...') }}" >
						@error('name')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Краткое описание') }}</label>
					<div class="col-md-9">
						<textarea class="form-control editor @error('short_description') is-invalid @enderror" name="short_description">{{ old('short_description', $legalinfo->short_description) }}</textarea>
						@error('short_description')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Текст') }}</label>
					<div class="col-md-9">
						<textarea class="form-control editor @error('description') is-invalid @enderror" name="description">{{ old('description', $legalinfo->description) }}</textarea>
						@error('description')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Группы пользователей') }}</label>
					<div class="col-md-9">
						<select multiple="" name="groups[]" class="form-control @error('groups', 'default') is-invalid @enderror">
							@foreach($groups as $group)
								<option value="{{ $group->id }}" {{ ($legalinfo->user_groups->contains($group->id)) ? 'selected':'' }}>{{ $group->name }}</option>
							@endforeach
						</select>
						@error('groups')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
				</div>

				<div class="form-group row">
					<div class="col-md-7 offset-md-3">
						<button type="submit" class="btn btn-lg btn-primary m-r-5">{{ __('Сохранить') }}</button>
					</div>
				</div>
			</form>
		</div>
		<!-- end panel-body -->
	</div>

@endsection


@push('scripts')
<script src="/assets/js/tinymce/tinymce.min.js"></script>

<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
<script src="/assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
<script src="/assets/plugins/bootstrap-select/dist/js/i18n/defaults-ru_RU.js"></script>

<script src="/assets/js/license/legalinfo.min.js?ver={{ config('app.version') }}"></script>
@endpush