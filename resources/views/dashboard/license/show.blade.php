@extends('layouts.default')

@section('title', __('Лицензия'))

@push('css')
	<link href="/assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
	<link href="/assets/plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right">
		<li class="breadcrumb-item">{{ __('Личный кабинет') }}</li>
		<li class="breadcrumb-item">{{ __('Лицензия') }}</li>
	</ol>

	<h1 class="page-header">{{ __('Лицензия') }}</h1>

	<div class="row">
		<div class="col-xl-12">
		<div class="panel panel-inverse" id="legalinfo">
		        <div class="panel-heading">
		            <h4 class="panel-title">{{ __('Правовая информация') }}</h4>
		        </div>
		        <div class="panel-body p-20">
		        	<table class="datatables table table-striped table-bordered table-td-valign-middle">
						<thead>
							<tr>
								<th width="1%">№</th>
								<th width="1%" class="text-nowrap">Наименование</th>
								<th class="text-nowrap">Краткое описание</th>
								<th width="1%" data-orderable="false" class="text-nowrap"></th>
								<th width="1%" data-orderable="false" class="text-nowrap"></th>
							</tr>
						</thead>
						<tbody>
							@foreach($legalinfos as $key => $legalinfo)
							<tr>
								<td width="1%" class="f-s-600 text-inverse">{{ $key + 1 }}</td>
								<td>{{ $legalinfo->name }}</td>
								<td>{!! $legalinfo->short_description !!}</td>
								<td>
									<button class="btn btn-info text-nowrap" onclick="$('#legalinfo{{$legalinfo->id}}Modal').modal('show');">Просмотр</button>
								</td>
								<td>
									<legalinfo url="{{ route('license.legalinfo.toggle', $legalinfo->id) }}" legalinfo-id="{{ $legalinfo->id }}" :status="{{ $legalinfo->users->contains(Auth::user()->id) ? 'true' : 'false' }}"></legalinfo>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
		        </div>
		    </div>

		    <div class="panel panel-inverse" id="licenses">
		        <div class="panel-heading">
		            <h4 class="panel-title">{{ __('Лицензия') }}</h4>
		        </div>
		        <div class="panel-body p-20">
		        	<table class="datatables table table-striped table-bordered table-td-valign-middle">
						<thead>
							<tr>
								<th width="1%">№</th>
								<th width="1%" data-orderable="false"></th>
								<th class="text-nowrap">Наименование</th>
								<th class="text-nowrap">Описание</th>
								<th class="text-nowrap">Дата окончания</th>
								<th width="1%" data-orderable="false" class="text-nowrap">Продление лицензии</th>
							</tr>
						</thead>
						<tbody>
							@foreach($licenses as $key => $license)
							<tr>
								<td width="1%" class="f-s-600 text-inverse">{{ $key + 1 }}</td>
								<td width="1%" class="with-img">
									<img src="{{ $license['image_url'] }}" class="height-60" />
								</td>
								<td>{{ $license['name'] }}</td>
								<td>{!! $license['description'] !!}</td>
								<td>{{ $license['expires'] }}</td>
								<td><button class="btn btn-info extend-license text-nowrap" url="{{ route('license.extend', $license['id']) }}" license="{{ $license['id'] }}">Отправить заявку</button></td>
							</tr>
							@endforeach
						</tbody>
					</table>
		        </div>
		    </div>
		</div>
	</div>

	@foreach($legalinfos as $key => $legalinfo)
	<div class="modal fade" id="legalinfo{{$legalinfo->id}}Modal">
		<div class="modal-dialog" style="max-width: 800px;">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">{{ $legalinfo->name }}</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					{!! $legalinfo->description !!}
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Закрыть') }}</button>
				</div>
			</div>
		</div>
	</div>
	@endforeach
@endsection


@push('scripts')
	<script src="/assets/plugins/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="/assets/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
	<script src="/assets/plugins/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
	<script src="/assets/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
	<script src="/assets/plugins/datatables.net-autofill/js/dataTables.autofill.min.js"></script>
	<script src="/assets/plugins/datatables.net-autofill-bs4/js/autofill.bootstrap4.min.js"></script>

	<script>
		$(document).ready(function() {
			if ($('.datatables').length !== 0) {
				$('.datatables').DataTable({
					'responsive': true,
					'language': {
				        'url': '/assets/plugins/datatables.net/js/ru.json'
				    },
				    "drawCallback": function () {
			            $('.dataTables_paginate > .pagination').addClass('flex-wrap');
			        }
				});
			}
		});
	</script>

	<script src="/assets/js/license/main.min.js?ver={{ config('app.version') }}"></script>
@endpush