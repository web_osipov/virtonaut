@extends('layouts.default')

@section('title', __('Преподаватели'))

@push('css')
	<link href="/assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
	<link href="/assets/plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right">
		<li class="breadcrumb-item">{{ __('Личный кабинет') }}</li>
		<li class="breadcrumb-item">{{ __('Преподаватели') }}</li>
	</ol>

	<h1 class="page-header">{{ __('Преподаватели') }}</small></h1>

	<div class="row">
		<div class="col-xl-12">
		    <div class="panel panel-inverse" id="members">
		        <div class="panel-heading">
		            <h4 class="panel-title">{{ __('Преподаватели') }}</h4>
		        </div>
		        <div class="panel-body p-20">
		        	<table class="datatables table table-striped table-bordered table-td-valign-middle">
						<thead>
							<tr>
								<th width="1%">№</th>
								<th width="1%" data-orderable="false"></th>
								<th class="text-nowrap">Ф.И.О.</th>
								<th class="text-nowrap">Почта</th>
								<th class="text-nowrap">Образовательное учреждение</th>
								<th class="text-nowrap">Избранный контакт</th>
								<th class="text-nowrap">Должность</th>
							</tr>
						</thead>
						<tbody>
							@foreach($members as $key => $member)
							<tr>
								<td width="1%" class="f-s-600 text-inverse">{{ $key + 1 }}</td>
								<td width="1%" class="with-img">
									<img src="{{ $member->icon_url }}" class="img-rounded height-30" />
								</td>
								<td>{{ $member->name }}</td>
								<td>
									<div class="contact-email">
										<span>{{ $member->correct_email }}</span>
										@if($member->enabledEmail())
										@can('use', App\Mail::class)
										<a href="{{ route('mail.compose', ['email' => $member->correct_email]) }}" class="btn btn-sm btn-primary m-l-20">НАПИСАТЬ</a>
										@endcan
										@endif
									</div>
								</td>
								<td>{{ $member->institution ? $member->institution->full_name : '- - -' }}</td>
								<td>
									<favorites url="{{ route('favorits') }}" :status="{{ Auth::user()->hasFavorited($member->id) ? 'true' : 'false' }}" user-id="{{ $member->id }}"></favorites>
								</td>
								<td>
									@if($member->hasGroup('principal'))
									<div>{{ __('Директор') }}</div>
									@endif
									@if($member->hasGroup('support'))
									<div>{{ __('Техподдержка') }}</div>
									@endif
									@if($member->hasGroup('teacher'))
									<div>{{ __('Преподаватель') }} 
									{{ $member->subjects ? $member->subjects->pluck('name')->join(', ') : '' }}</div>
									@endif
									@if($member->hasGroup('student'))
									<div>{{ __('Ученик') }}</div>
									@endif
									@if($member->hasGroup('admin'))
									<div>{{ __('Администратор') }}</div>
									@endif 
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
		        </div>
		    </div>
		</div>
	</div>
@endsection


@push('scripts')
	<script src="/assets/plugins/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="/assets/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
	<script src="/assets/plugins/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
	<script src="/assets/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
	<script src="/assets/plugins/datatables.net-autofill/js/dataTables.autofill.min.js"></script>
	<script src="/assets/plugins/datatables.net-autofill-bs4/js/autofill.bootstrap4.min.js"></script>

	<script src="/assets/js/members/main.min.js?ver={{ config('app.version') }}"></script>
@endpush