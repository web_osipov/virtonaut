@extends('layouts.default')

@section('title', __('Редактирование учебного класса'))

@push('css')
	<link href="/assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
	<link href="/assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right">
		<li class="breadcrumb-item">{{ __('Личный кабинет') }}</li>
		<li class="breadcrumb-item">{{ __('Настройки') }}</li>
		<li class="breadcrumb-item">{{ __('Учебные классы') }}</li>
		<li class="breadcrumb-item active">{{ __('Редактирование учебного класса') }}</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">{{ __('Редактирование учебного класса') }}</h1>
	<!-- end page-header -->
	
	@if (session('status'))
	    <div class="alert alert-success fade show">
	        {{ session('status') }}
	    </div>
	@endif

	<div class="panel panel-inverse">
		<!-- begin panel-body -->
		<div class="panel-body">
			<form action="{{ route('settings.classroom.edit', $class->id) }}" method="POST">
				@csrf
				@method('PUT')
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Название') }}</label>
					<div class="col-md-9">
						<input type="text" name="name" class="form-control m-b-5 @error('name') is-invalid @enderror" value="{{ old('name', $class->name) }}" placeholder="{{ __('Введите название...') }}" >
						@error('name')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>
				<div class="form-group row m-b-15" id="institution-block">
					<label class="col-form-label col-md-3">{{ __('Учебное учреждение') }}</label>
					<div class="col-md-9">
						<select name="institution" class="form-control @error('institution') is-invalid @enderror selectpicker" multiple data-max-options="1" data-live-search="true">
							@foreach($institutions as $institution)
								<option value="{{ $institution->id }}" {{ (old('institution', $class->getInstitutionId()) == $institution->id) ? 'selected' :'' }}>{{ $institution->full_name }}</option>
							@endforeach
						</select>
						@error('institution')
		                    <span class="invalid-feedback" role="alert">
		                        <strong>{{ $message }}</strong>
		                    </span>
		                @enderror
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Учебный класс без камер') }}</label>
					<div class="col-md-9">
						<div class="switcher">
							<input type="checkbox" name="no_cam" @if(optional($class)->no_cam == 1) checked @endif id="form-no_cam" value="1">	  	
							<label for="form-no_cam"></label>
						</div>
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('RTSP камеры учебного класса') }}</label>
					<div class="col-md-9">
						<input type="text" name="class_cam" class="form-control m-b-5 @error('class_cam') is-invalid @enderror" value="{{ old('class_cam', $class->class_cam) }}" placeholder="{{ __('Введите название...') }}" >
						@error('class_cam')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('RTSP камеры интерактивной доски') }}</label>
					<div class="col-md-9">
						<input type="text" name="board_cam" class="form-control m-b-5 @error('board_cam') is-invalid @enderror" value="{{ old('board_cam', $class->board_cam) }}" placeholder="{{ __('Введите название...') }}" >
						@error('board_cam')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>
				<div class="form-group row">
					<div class="col-md-7 offset-md-3">
						<button type="submit" class="btn btn-lg btn-primary m-r-5">{{ __('Сохранить учебный класс') }}</button>
					</div>
				</div>
			</form>
		</div>
		<!-- end panel-body -->
	</div>

@endsection


@push('scripts')
	<script src="/assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
	<script src="/assets/plugins/bootstrap-select/dist/js/i18n/defaults-ru_RU.js"></script>

	<script src="/assets/js/settings/class.min.js?ver={{ config('app.version') }}"></script>
@endpush