@extends('layouts.default')

@section('title', __('Добавить класс'))

@push('css')
	<link href="/assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
	<link href="/assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right">
		<li class="breadcrumb-item">{{ __('Личный кабинет') }}</li>
		<li class="breadcrumb-item">{{ __('Настройки') }}</li>
		<li class="breadcrumb-item">{{ __('Классы') }}</li>
		<li class="breadcrumb-item active">{{ __('Добавить класс') }}</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">{{ __('Добавить класс') }}</h1>
	<!-- end page-header -->
	<!-- begin panel -->
	<div class="panel panel-inverse">
		<!-- begin panel-body -->
		<div class="panel-body">
			<form action="{{ route('settings.class.create') }}" method="POST">
				@csrf
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Название') }}</label>
					<div class="col-md-9">
						<input type="text" name="name" class="form-control m-b-5 @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="{{ __('Введите название...') }}" >
						@error('name')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>
				<div class="form-group row m-b-15" id="institution-block">
					<label class="col-form-label col-md-3">{{ __('Учебное учреждение') }}</label>
					<div class="col-md-9">
						<select name="institution" class="form-control @error('institution') is-invalid @enderror selectpicker"  data-live-search="true">
							@foreach($institutions as $institution)
								<option value="{{ $institution->id }}" {{ old('institutions') == $institution->id ? 'selected' :'' }}>{{ $institution->full_name }}</option>
							@endforeach
						</select>
						@error('institution')
		                    <span class="invalid-feedback" role="alert">
		                        <strong>{{ $message }}</strong>
		                    </span>
		                @enderror
					</div>
				</div>
				<div class="form-group row">
					<div class="col-md-7 offset-md-3">
						<button type="submit" class="btn btn-lg btn-primary m-r-5">{{ __('Добавить класс') }}</button>
					</div>
				</div>
			</form>
		</div>
		<!-- end panel-body -->
	</div>
	<!-- end panel -->
@endsection


@push('scripts')
	<script src="/assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
	<script src="/assets/plugins/bootstrap-select/dist/js/i18n/defaults-ru_RU.js"></script>

	<script src="/assets/js/settings/class.min.js?ver={{ config('app.version') }}"></script>
@endpush