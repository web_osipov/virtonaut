@extends('layouts.default')

@section('title', __('Добавить группу'))

@push('css')
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right">
		<li class="breadcrumb-item">{{ __('Личный кабинет') }}</li>
		<li class="breadcrumb-item">{{ __('Настройки') }}</li>
		<li class="breadcrumb-item">{{ __('Достижения') }}</li>
		<li class="breadcrumb-item">{{ __('Группы достижений') }}</li>
		<li class="breadcrumb-item active">{{ __('Добавить группу') }}</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">{{ __('Добавить группу') }}</h1>
	<!-- end page-header -->
	<!-- begin panel -->
	<div class="panel panel-inverse">
		<!-- begin panel-body -->
		<div class="panel-body">
			<form action="{{ route('settings.achivement.group.create') }}" enctype="multipart/form-data" method="POST">
				@csrf
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Название') }}</label>
					<div class="col-md-9">
						<input type="text" name="name" class="form-control m-b-5 @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="{{ __('Введите название...') }}" >
						@error('name')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Группы пользователей') }}</label>
					<div class="col-md-9">
						<select multiple="" name="user_groups[]" class="form-control @error('user_groups') is-invalid @enderror">
							@foreach($user_groups as $group)
								<option value="{{ $group->id }}" {{ (collect(old('user_groups'))->contains($group->id)) ? 'selected':'' }}>{{ $group->name }}</option>
							@endforeach
						</select>
						@error('user_groups')
		                    <span class="invalid-feedback" role="alert">
		                        <strong>{{ $message }}</strong>
		                    </span>
		                @enderror
					</div>
				</div>

				<div class="form-group row">
					<div class="col-md-7 offset-md-3">
						<button type="submit" class="btn btn-lg btn-primary m-r-5">{{ __('Добавить группу') }}</button>
					</div>
				</div>
			</form>
		</div>
		<!-- end panel-body -->
	</div>
	<!-- end panel -->
@endsection


@push('scripts')
	<script src="/assets/js/settings/achivement_group.min.js?ver={{ config('app.version') }}"></script>
@endpush