@extends('layouts.default')

@section('title', __('Редактирование учреждения'))

@push('css')
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right">
		<li class="breadcrumb-item">{{ __('Личный кабинет') }}</li>
		<li class="breadcrumb-item">{{ __('Настройки') }}</li>
		<li class="breadcrumb-item">{{ __('Учебные учреждения') }}</li>
		<li class="breadcrumb-item active">{{ __('Редактирование учреждения') }}</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">{{ __('Редактирование учреждения') }}</h1>
	<!-- end page-header -->
	
	@if (session('status'))
	    <div class="alert alert-success fade show">
	        {{ session('status') }}
	    </div>
	@endif

	<div class="panel panel-inverse">
		<!-- begin panel-body -->
		<div class="panel-body">
			<form action="{{ route('settings.institution.edit', $institution->id) }}" enctype="multipart/form-data" method="POST">
				@csrf
				@method('PUT')
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Полное название') }}</label>
					<div class="col-md-9">
						<input type="text" name="full_name" class="form-control m-b-5 @error('full_name', $institution->full_name) is-invalid @enderror" value="{{ old('full_name', $institution->full_name) }}" placeholder="{{ __('Введите полное название...') }}" >
						@error('full_name')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Краткое название') }}</label>
					<div class="col-md-9">
						<input type="text" name="short_name" class="form-control m-b-5 @error('short_name') is-invalid @enderror" value="{{ old('short_name', $institution->short_name) }}" placeholder="{{ __('Введите краткое название...') }}" >
						@error('short_name')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Адрес') }}</label>
					<div class="col-md-9">
						<input type="text" name="address" class="form-control m-b-5 @error('address') is-invalid @enderror" value="{{ old('address', $institution->address) }}" placeholder="{{ __('Введите адрес...') }}" >
						@error('address')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Телефон/факс') }}</label>
					<div class="col-md-9">
						<input type="text" name="phone" class="form-control m-b-5 @error('phone') is-invalid @enderror" value="{{ old('phone', $institution->phone) }}" placeholder="{{ __('Введите телефон/факс...') }}" >
						@error('phone')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Почта') }}</label>
					<div class="col-md-9">
						<input type="text" name="mail" class="form-control m-b-5 @error('mail') is-invalid @enderror" value="{{ old('mail', $institution->mail) }}" placeholder="{{ __('Введите почта...') }}" >
						@error('mail')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Сайт') }}</label>
					<div class="col-md-9">
						<input type="text" name="site" class="form-control m-b-5 @error('site') is-invalid @enderror" value="{{ old('site', $institution->site) }}" placeholder="{{ __('Введите сайт...') }}" >
						@error('site')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Директор') }}</label>
					<div class="col-md-9">
						<input type="text" name="principal" class="form-control m-b-5 @error('principal') is-invalid @enderror" value="{{ old('principal', $institution->principal) }}" placeholder="{{ __('Директор...') }}" >
						@error('principal')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>

				<div class="form-group row m-b-15">
				    <label class="col-form-label col-md-3">{{ __('Иконка') }}</label>
				    <div class="col-md-9">
				    	@if($institution->icon)
				    	<div class="form-check m-b-10">
							<img src="{{$institution->icon_url}}" width="100px" height="auto">
						</div>
					    <div class="form-check m-b-10">
							<input type="checkbox" name="del_icon" value="1" id="delete-icon" class="form-check-input">
							<label class="form-check-label" for="delete-icon">{{ __('Удалить иконку') }}</label>
						</div>
						@endif
				    	<input type="file" name="icon" class="form-control-file @error('icon') is-invalid @enderror">
				    	@error('icon')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
				    </div>
				</div>

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Логин') }}</label>
					<div class="col-md-9">
						<input type="text" name="login" class="form-control m-b-5 @error('login') is-invalid @enderror" value="{{ old('login', $institution->login) }}" placeholder="{{ __('Логин...') }}" >
						@error('login')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">{{ __('Пароль') }}</label>
					<div class="col-md-9">
						<input type="text" name="pass" class="form-control m-b-5 @error('pass') is-invalid @enderror" value="{{ old('pass', $institution->pass) }}" placeholder="{{ __('Пароль...') }}" >
						@error('pass')
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $message }}</strong>
	                        </span>
	                    @enderror
					</div>
				</div>

				<div class="form-group row">
					<div class="col-md-7 offset-md-3">
						<button type="submit" class="btn btn-lg btn-primary m-r-5">{{ __('Сохранить учреждение') }}</button>
					</div>
				</div>
			</form>
		</div>
		<!-- end panel-body -->
	</div>

@endsection


@push('scripts')
	<script src="/assets/js/settings/institution.min.js?ver={{ config('app.version') }}"></script>
@endpush