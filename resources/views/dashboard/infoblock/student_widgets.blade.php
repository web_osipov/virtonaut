<div class="row">
	<!-- begin col-3 -->
	<div class="col-xl-3 col-md-6">
		<div class="widget widget-stats bg-blue">
			<div class="stats-icon"><i class="fa fa-desktop"></i></div>
			<div class="stats-info">
				<h4>Онлайн уроки</h4>
				<p>{{ Auth::user()->availableRecordings() }} <span class="f-s-12">{{ get_plural_form(Auth::user()->availableRecordings(), ['урок', 'урока', 'уроков']) }} {{ Auth::user()->availableRecordings() == 1 ? 'доступен' : 'доступно' }}</p>	
			</div>
			<div class="stats-link">
				<a href="{{ route('remotelearning.index') }}">Узнать подробнее <i class="fa fa-arrow-alt-circle-right"></i></a>
			</div>
		</div>
	</div>
	<!-- end col-3 -->
	<!-- begin col-3 -->
	<div class="col-xl-3 col-md-6">
		<div class="widget widget-stats bg-info">
			<div class="stats-icon"><i class="fa fa-link"></i></div>
			<div class="stats-info">
				<h4>Ваши достижения</h4>
				<p><span class="f-s-12">У вас</span> {{ count($user_achivements) }} <span class="f-s-12">{{ get_plural_form(count($user_achivements), ['достижение', 'достижения', 'достижений']) }}</span></p>	
			</div>
			<div class="stats-link">
				<a href="{{ route('achivement.index') }}">Узнать подробнее <i class="fa fa-arrow-alt-circle-right"></i></a>
			</div>
		</div>
	</div>
	<!-- end col-3 -->
	<!-- begin col-3 -->
	<div class="col-xl-3 col-md-6">
		<div class="widget widget-stats bg-orange">
			<div class="stats-icon"><i class="fa fa-users"></i></div>
			<div class="stats-info">
				@if(!empty($live_lesson))
				<h4>Сейчас проходит урок</h4>
				<p><span class="f-s-12">{{ $live_lesson->subject->name }} "{{ $live_lesson->topic }}"</span></p>	
				@elseif(!empty($next_lesson))
				<h4>Следующий урок</h4>
				<p><span class="f-s-12">{{ $next_lesson->subject->name }} "{{ $next_lesson->topic }}"</span></p>
				@else
				<h4>Следующий урок</h4>
				<p>- - -</p>
				@endif
			</div>
			<div class="stats-link">
				<a href="{{ route('remotelearning.index') }}">Присоединиться <i class="fa fa-arrow-alt-circle-right"></i></a>
			</div>
		</div>
	</div>
	<!-- end col-3 -->
	<!-- begin col-3 -->
	<div class="col-xl-3 col-md-6">
		<div class="widget widget-stats bg-red">
			<div class="stats-icon"><i class="fa fa-clock"></i></div>
			<div class="stats-info">
				<h4>До следующего урока</h4>
				@if(!empty($next_lesson))
				<p>
					@if(Carbon\Carbon::parse($next_lesson->start)->diffInDays() > 0)
					{{ Carbon\Carbon::parse($next_lesson->start)->diffInDays() }} <span class="f-s-12">{{ get_plural_form( Carbon\Carbon::parse($next_lesson->start)->diffInDays(), ['день', 'дня', 'дней']) }}</span>
					@endif

					@if(Carbon\Carbon::parse($next_lesson->start)->diffInHours() > 0)
					{{ Carbon\Carbon::parse($next_lesson->start)->diffInHours() - (Carbon\Carbon::parse($next_lesson->start)->diffInDays() * 24) }} <span class="f-s-12">{{ get_plural_form( Carbon\Carbon::parse($next_lesson->start)->diffInHours() - (Carbon\Carbon::parse($next_lesson->start)->diffInDays() * 24), ['час', 'часа', 'часов']) }}</span>
					@endif

					@if(Carbon\Carbon::parse($next_lesson->start)->diffInMinutes() > 0)
					{{ (Carbon\Carbon::parse($next_lesson->start)->diffInMinutes() - (Carbon\Carbon::parse($next_lesson->start)->diffInHours() * 60)) }} <span class="f-s-12">{{ get_plural_form( (Carbon\Carbon::parse($next_lesson->start)->diffInMinutes() - (Carbon\Carbon::parse($next_lesson->start)->diffInHours() * 60)), ['минута', 'минуты', 'минут']) }}</span>
					@else
					<span class="f-s-12">менее</span> 1 <span class="f-s-12">минуты</span>
					@endif
				</p>
				@else
				<p>- - -</p>
				@endif	
			</div>
			<div class="stats-link">
				<a href="{{ route('schedule.myschedule') }}">Перейти к расписанию <i class="fa fa-arrow-alt-circle-right"></i></a>
			</div>
		</div>
	</div>
	<!-- end col-3 -->
</div>