<!-- begin panel -->
<div class="panel panel-inverse" data-sortable-id="index-1">
	<div class="panel-heading">
		<h4 class="panel-title">Доска достижений класса</h4>
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
		</div>
	</div>
	<div class="panel-body p-0">
		<div class="achievements">
			@foreach($achivement_groups as $achivement_group) 
			<div class="achievement">
				<div class="achievement-title">{{ $achivement_group->name }}</div>
				<div class="achievement-badges-wrapper">
					<div class="achievement-badges">
						@foreach($achivement_group->sorted_achivements as $achivement)
						@if(in_array($achivement->id, $user_achivements))
						<div class="achievement-badge">
							<div class="achievement-badge-image">
								<img src="{{ $achivement->icon_url }}">
							</div>
							<div class="achievement-badge-name">{{ $achivement->name }}</div>
						</div>
						@endif
						@endforeach
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</div>
<!-- end panel -->