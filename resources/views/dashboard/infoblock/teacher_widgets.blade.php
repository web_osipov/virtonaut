<div class="row">
	<!-- begin col-3 -->
	<div class="col-xl-3 col-md-6">
		<div class="widget widget-stats bg-blue">
			<div class="stats-icon"><i class="fa fa-desktop"></i></div>
			<div class="stats-info">
				<h4>Объем вашего хранилища</h4>
				<p>99% <span class="f-s-12">свободно</span> 1% <span class="f-s-12">занято</span></p>	
			</div>
			<div class="stats-link">
				<a href="javascript:;">Узнать подробнее <i class="fa fa-arrow-alt-circle-right"></i></a>
			</div>
		</div>
	</div>
	<!-- end col-3 -->
	<!-- begin col-3 -->
	<div class="col-xl-3 col-md-6">
		<div class="widget widget-stats bg-info">
			<div class="stats-icon"><i class="fa fa-link"></i></div>
			<div class="stats-info">
				<h4>События сообщества</h4>
				<p>{{ $ended_events }} <span class="f-s-12">состоявшихся</span> {{ $future_events }} <span class="f-s-12">планируемых</span></p>	
			</div>
			<div class="stats-link">
				<a href="{{ route('calendar') }}">Узнать подробнее <i class="fa fa-arrow-alt-circle-right"></i></a>
			</div>
		</div>
	</div>
	<!-- end col-3 -->
	<!-- begin col-3 -->
	<div class="col-xl-3 col-md-6">
		<div class="widget widget-stats bg-orange">
			<div class="stats-icon"><i class="fa fa-users"></i></div>
			<div class="stats-info">
				<h4>Количество участников</h4>
				<p><span class="f-s-12"></span> {{ $institutuions_count }} <span class="f-s-12">{{ get_plural_form($institutuions_count, ['учреждение', 'учреждения', 'учреждений']) }} и</span> {{ $users_count }} <span class="f-s-12">{{ get_plural_form($users_count, ['участник', 'участника', 'участников']) }}</span></p>	
			</div>
			<div class="stats-link">
				<a href="{{ route('members') }}">Узнать подробнее <i class="fa fa-arrow-alt-circle-right"></i></a>
			</div>
		</div>
	</div>
	<!-- end col-3 -->
	<!-- begin col-3 -->
	<div class="col-xl-3 col-md-6">
		<div class="widget widget-stats @if($license_expires) bg-green @else bg-danger @endif">
			<div class="stats-icon"><i class="fa fa-clock"></i></div>
			<div class="stats-info">
				<h4>До окончания вашей лицензии</h4>
				@if($license_expires)
					<p>
						@if($license_expires->diffInDays() > 0)
						{{ $license_expires->diffInDays() }} <span class="f-s-12">{{ get_plural_form( $license_expires->diffInDays(), ['день', 'дня', 'дней']) }}</span>
						@endif

						@if($license_expires->diffInHours() > 0)
						{{ $license_expires->diffInHours() - ($license_expires->diffInDays() * 24) }} <span class="f-s-12">{{ get_plural_form( $license_expires->diffInHours() - ($license_expires->diffInDays() * 24), ['час', 'часа', 'часов']) }}</span>
						@endif

						@if($license_expires->diffInMinutes() > 0)
						{{ ($license_expires->diffInMinutes() - ($license_expires->diffInHours() * 60)) }} <span class="f-s-12">{{ get_plural_form( ($license_expires->diffInMinutes() - ($license_expires->diffInHours() * 60)), ['минута', 'минуты', 'минут']) }}</span>
						@else
						<span class="f-s-12">менее</span> 1 <span class="f-s-12">минуты</span>
						@endif
					</p>
				@else
					<p>- - -</p>	
				@endif
			</div>
			<div class="stats-link">
				<a href="{{ route('license.index') }}">Узнать подробнее <i class="fa fa-arrow-alt-circle-right"></i></a>
			</div>
		</div>
	</div>
	<!-- end col-3 -->
</div>