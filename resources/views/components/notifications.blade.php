<li class="dropdown">
	<a href="#" data-toggle="dropdown" class="dropdown-toggle f-s-14">
		<i class="fa fa-bell"></i>
		@if($notifications && $notifications->count() > 0)
		<span class="label">{{ $notifications->count() }}</span>
		@endif
	</a>
	<div class="dropdown-menu media-list header-notifications dropdown-menu-right">
		<div class="dropdown-header">{{ __('Уведомления') }} ({{ $notifications ? $notifications->count() : 0 }})</div>

		@if($notifications && $notifications->count() > 0)
			<div class="notifications-list">
				@foreach($notifications as $notification)
				<a href="{{ $notification->link }}" class="dropdown-item media">
					<div class="media-left">
						@if($notification->type == 'App\Notifications\NewEmail')
						<i class="fa fa-envelope media-object bg-silver-darker"></i>
						<i class="fab fa-google text-warning media-object-icon f-s-14"></i>
						@elseif($notification->type == 'App\Notifications\NewAchivement')
						<i class="fa fa-award media-object bg-silver-darker"></i>
						@endif
					</div>
					<div class="media-body">
						<h6 class="media-heading">{{ $notification->title }}</h6>
						<div class="text-muted f-s-10">{{ $notification->date }}</div>
					</div>
				</a>
				@endforeach
			</div>
			<div class="dropdown-footer text-center">
				<a href="javascript:;" onclick="event.preventDefault(); document.getElementById('clear-all-notifications-form').submit();" >{{ __('Очистить') }}</a>
				<form id="clear-all-notifications-form" action="{{ route('notifications') }}" method="POST" style="display: none;">
                    @csrf
                    @method('DELETE')
                </form>
			</div>
		@else
		<div class="dropdown-item media">
			<div class="media-body">
				<h6 class="media-heading text-center">{{ __('Нет уведомлений') }}</h6>
			</div>
		</div>
		@endif
	</div>
</li>