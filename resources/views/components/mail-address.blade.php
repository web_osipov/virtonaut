@if($mails)
<div {{ $attributes }}>
	{{ $mails_title }}: {{ $mails }}
</div>
@endif