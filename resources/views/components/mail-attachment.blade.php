<ul class="attached-document clearfix">
	@foreach($attachments as $attachment)
	<li class="{{ $attachment->type == 'image' ? 'fa-camera' : 'fa-file' }}">
		<div class="document-file">
			<a href="{{ $attachment->url }}">
				@if($attachment->type == 'image')
				<img src="{{ $attachment->url }}?view=y" alt="" />
				@else
				<i class="{{ $attachment->icon }}"></i>
				@endif
			</a>
		</div>
		<div class="document-name"><a href="{{ $attachment->url }}">{{ $attachment->name }}</a></div>
	</li>
	@endforeach
</ul>