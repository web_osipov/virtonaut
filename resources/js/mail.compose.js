$(document).ready(function() {

	tinymce.init({
		selector: 'textarea#editor',
    	plugins: 'lists, link, image, imagetools',
    	language: 'ru',
    	toolbar: 'h1 h2 bold italic strikethrough blockquote bullist numlist backcolor | link image | removeformat',
    	menubar: false,
    	branding: false,
    	setup: function (editor) {
	        editor.on('change', function () {
	            editor.save();
	        });
	    }
	});

	$('#email-to').tagit({
		availableTags: [],
		fieldName: 'to[]'
	});

	$(document).on('click', '[data-click="add-cc"]', function(e) {
		e.preventDefault();
		
		var targetName = $(this).attr('data-name');
		var targetFieldName = $(this).attr('data-field');
		var targetId = 'email-cc-'+ targetName +'';
		var targetHtml = ''+
		'	<div class="email-to">'+
		'		<label class="control-label">'+ targetName +':</label>'+
		'		<ul id="'+ targetId +'" class="primary line-mode"></ul>'+
		'	</div>';
		$('[data-id="extra-cc"]').append(targetHtml);
		$('#' + targetId).tagit({fieldName: targetFieldName + '[]'});
		$(this).remove();
	});

	if($('#email-cc').length > 0) $('#email-cc').tagit({fieldName: 'cc[]'});

	if($('#email-bcc').length > 0) $('#email-bcc').tagit({fieldName: 'bcc[]'});

	var previewNode = document.querySelector("#attachment-template");
	previewNode.id = "";
	var previewTemplate = previewNode.parentNode.innerHTML;
	previewNode.parentNode.removeChild(previewNode);

	var myDropzone = new Dropzone(document.body, {
		url: "/dashboard/email/a", 
		thumbnailWidth: 300,
		thumbnailHeight: null,
		parallelUploads: 20,
		previewTemplate: previewTemplate,
		autoQueue: true,
		previewsContainer: "#attachments",
		clickable: ".fileinput-button",
		headers: {
	        'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr("content")
	    },
	    init: function() {
			let myDropzone = this;

			if(typeof attachmentsMock != 'undefined' && attachmentsMock.length > 0) {
				Object.keys(attachmentsMock).forEach(key => {
					var value = attachmentsMock[key];
					let mockFile = { name: value.name, size: value.size };
					//myDropzone.displayExistingFile(mockFile, value.image);
				});
				//myDropzone.options.maxFiles = myDropzone.options.maxFiles - attachmentsMock.length;
			}
		}
	});
	
	myDropzone.on("complete", function(file) {
		if(file.status == 'error') {
			var error = $(file.previewElement).find('.error-msg').text();
			if(error && error != '[object Object]') {
				alert(error);
			} else {
				alert('Возникла ошибка при загрузке файла.');
			}
			myDropzone.removeFile(file);
		} else {
			$(file.previewElement).find('input[name="attachments[]"]').val(file.xhr.responseText);
		}
	});

	$(document).on('click', '.save-draft', function() {
		var button = $(this);
		$('.save-draft').attr('disabled', true);
		$('.save-draft').removeClass('is-loading').addClass('is-loading');

		var form = $('#compose-form');

		var draftUrl = form.attr('draft-action');

		$.ajax({
	        url: draftUrl,
	        type: "POST",
	        data: form.serializeArray(),
	        dataType: "json",
	        success: function (data) {
	        	$('.save-draft').attr('disabled', false);
				$('.save-draft').removeClass('is-loading');
				window.history.pushState({draft_id: data.id}, "", data.url);
				form.find('input[name="draft_id"]').val(data.id);
				$.gritter.add({
					title: 'Черновик сохранен!',
					text: 'Теперь вы можете продолжить написание письма в любой момент.',
					image: '',
					sticky: false,
					time: ''
				});
	        },
	        error: function (xhr, ajaxOptions, thrownError) {
	            $('.save-draft').attr('disabled', false);
				$('.save-draft').removeClass('is-loading');
				swal("Ошибка!", "Черновик не сохранен!", "error");
	        }
	    });

	});

	$(document).on('click', '.send-messsage', function() {
		var button = $(this);
		$('.send-messsage').attr('disabled', true);
		$('.send-messsage').removeClass('is-loading').addClass('is-loading');

		var form = $('#compose-form');

		var url = form.attr('action');

		$.ajax({
	        url: url,
	        type: "POST",
	        data: form.serializeArray(),
	        dataType: "json",
	        success: function (data) {
	        	$('.send-messsage').attr('disabled', false);
				$('.send-messsage').removeClass('is-loading');
				if(data.error) {
					swal("Ошибка!", data.error, "error");
				} else if(data.url) {
					window.location.href = data.url;
				}
	        },
	        error: function (xhr, ajaxOptions, thrownError) {
	            $('.send-messsage').attr('disabled', false);
				$('.send-messsage').removeClass('is-loading');
				swal("Ошибка!", "Сообщение не отправлено!", "error");
	        }
	    });
	});

	$(document).on('click', '[data-email-action=delete]', function() {
		var button = $(this);
		var action = 'delete';
		$('[data-email-action='+action+']').attr('disabled', true);
		$('[data-email-action='+action+']').removeClass('is-loading').addClass('is-loading');

		var form = $('#compose-form');

		var draft_id = form.find('input[name="draft_id"]').val();

		if(draft_id == '') {
			window.location.href = $(this).attr('inbox-url');
		} else {
			var url = $(this).attr('message-url').slice(0, -1) + draft_id;

			$.ajax({
		        url: url,
		        type: "POST",
		        data: {
		        	'_method': 'DELETE',
	        		'_token': $("meta[name='csrf-token']").attr("content")
		        },
		        dataType: "json",
		        success: function (data) {
		        	$('[data-email-action='+action+']').attr('disabled', false);
					$('[data-email-action='+action+']').removeClass('is-loading');
					if(data.error) {
						swal("Ошибка!", data.error, "error");
					} else if(data.url) {
						window.location.href = data.url;
					}
		        },
		        error: function (xhr, ajaxOptions, thrownError) {
		            $('[data-email-action='+action+']').attr('disabled', false);
					$('[data-email-action='+action+']').removeClass('is-loading');
					swal("Ошибка!", "Сообщение не удалено!", "error");
		        }
		    });	
		}
	});
});
