$(document).ready(function() {

	updateFieldBasedOnInstitution();
	$(document).on('change', 'select[name=institution]', function() {
		updateFieldBasedOnInstitution();
	});
	
});

function updateFieldBasedOnInstitution() {
	var institution_id = $('select[name=institution]').val()[0];

	$('select[name="classes[]"] optgroup').each(function() {
		if($(this).attr('institution-id') != institution_id) {
			$(this).attr('hidden', true);
			$(this).find('option').attr('selected', false).attr('hidden', true);
		} else {
			$(this).attr('hidden', false);
			$(this).find('option').attr('hidden', false);
		}
	});

	$('.selectpicker').selectpicker('refresh');
}