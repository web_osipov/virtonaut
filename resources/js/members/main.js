$(document).ready(function() {
	if ($('.datatables').length !== 0) {
		$('.datatables').DataTable({
			'responsive': true,
			"order": [[ 4, "desc" ]],
			'language': {
		        'url': '/assets/plugins/datatables.net/js/ru.json'
		    },
		    "drawCallback": function () {
	            $('.dataTables_paginate > .pagination').addClass('flex-wrap');
	        }
		});
	}
});

import Vue from 'vue'

Vue.component('favorites', require('./components/Favorites.vue').default);

const members = new Vue({
    el: '#members',
    data () {
	    return {
	    	app: null
	    }
  	},
  	mounted() {
    }
});