var handleEmailActionButtonStatus = function() {
	if ($('[data-checked=email-checkbox]:checked').length !== 0) {
		$('[data-email-action]').removeClass('hide');
	} else {
		$('[data-email-action]').addClass('hide');
	}
};

var handleEmailCheckboxChecked = function() {
	$(document).on('change', '[data-checked=email-checkbox]', function() {
		var targetLabel = $(this).closest('label');
		var targetEmailList = $(this).closest('li');
		if ($(this).prop('checked')) {
			$(targetLabel).addClass('active');
			$(targetEmailList).addClass('selected');
		} else {
			$(targetLabel).removeClass('active');
			$(targetEmailList).removeClass('selected');
		}
		handleEmailActionButtonStatus();
	});
};

var handleEmailAction = function() {
	$(document).on('click', '[data-email-action]', function() {
		var targetEmailList = '[data-checked="email-checkbox"]:checked';
		var action = $(this).attr('data-email-action');
		var messages = [];
		if ($(targetEmailList).length !== 0) {
			$(targetEmailList).each(function() {
				messages.push($(this).val());
			});
		}
		if(messages.length > 0) {
			if(action == 'delete') {
				$('[data-email-action='+action+']').attr('disabled', true);
				$('[data-email-action='+action+']').removeClass('is-loading').addClass('is-loading');
				$.ajax({
			        url: '',
			        type: "POST",
			        data: {
			        	'messages': messages,
			        	'_method': 'DELETE',
	            		'_token': $("meta[name='csrf-token']").attr("content")
			        },
			        dataType: "json",
			        success: function (data) {
			        	$('[data-email-action='+action+']').attr('disabled', false);
						$('[data-email-action='+action+']').removeClass('is-loading');
						if(data.error) {
							swal("Ошибка!", data.error, "error");
						} else if(data.url) {
							window.location.href = data.url;
						}
			        },
			        error: function (xhr, ajaxOptions, thrownError) {
			            $('[data-email-action='+action+']').attr('disabled', false);
						$('[data-email-action='+action+']').removeClass('is-loading');
						swal("Ошибка!", "Сообщения не удалены!", "error");
			        }
			    });
			} else {
				$.ajax({
			        url: '',
			        type: "POST",
			        data: {
			        	'messages': messages,
			        	'_method': 'PUT',
			        	'action': action,
	            		'_token': $("meta[name='csrf-token']").attr("content")
			        },
			        dataType: "json",
			        success: function (data) {
			        	$('[data-email-action='+action+']').attr('disabled', false);
						$('[data-email-action='+action+']').removeClass('is-loading');
						if(data.error) {
							swal("Ошибка!", data.error, "error");
						} else if(data.url) {
							window.location.href = data.url;
						}
			        },
			        error: function (xhr, ajaxOptions, thrownError) {
			            $('[data-email-action='+action+']').attr('disabled', false);
						$('[data-email-action='+action+']').removeClass('is-loading');
						swal("Ошибка!", "Действие не выполнено!", "error");
			        }
			    });
			}
		}
		handleEmailActionButtonStatus();
	});
};

var handleEmailSelectAll = function () {
	"use strict";
	$(document).on('change', '[data-change=email-select-all]', function() {
		if (!$(this).is(':checked')) {
			$('.list-email .email-checkbox input[type="checkbox"]').prop('checked', false);
		} else {
			$('.list-email .email-checkbox input[type="checkbox"]').prop('checked', true);
		}
		$('.list-email .email-checkbox input[type="checkbox"]').trigger('change');
	});
};

var EmailInbox = function () {
	"use strict";
	return {
		//main function
		init: function () {
			handleEmailCheckboxChecked();
			handleEmailAction();
			handleEmailSelectAll();
		}
	};
}();

$(document).ready(function() {
	EmailInbox.init();
});