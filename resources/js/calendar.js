$(document).ready(function() {

	$('#calendar').fullCalendar({
		locale: 'ru',
		header: {
			left: 'month,agendaWeek,agendaDay',
			center: 'title',
			right: 'prev,today,next '
		},
		droppable: false,
		drop: function() {
			$(this).remove();
		},
		selectable: false,
		selectHelper: false,
		eventClick: function(calEvent, jsEvent, view) {
			loadEvent(calEvent.id);
		},
		editable: false,
		eventLimit: false, 
		events: {
			url: '',
			method: 'POST',
			extraParams: {
				'csrf': $("meta[name='csrf-token']").attr("content"),
			},
			failure: function() {
				alert('there was an error while fetching events!');
			}
		}
	});

});

window.loadEventAjax = false;
window.loadEvent = function(id) {
	$('#showEventModal').modal('show');
	$('#showEventModal').find('.modal-body').removeClass('is-loading').addClass('is-loading');
	if(loadEventAjax) loadEventAjax.abort();
	loadEventAjax = $.ajax({
	    url: '/dashboard/calendar/' + id,
	    type: "POST",
	    data: {
			'_token': $("meta[name='csrf-token']").attr("content")
	    },
	    success: function (data) {
	    	$('#showEventModal').find('.modal-body').removeClass('is-loading').html(data);
	    },
	    error: function (xhr, ajaxOptions, thrownError) {
	    	$('#showEventModal').modal('hide');
			swal("Ошибка!", "Повторите попытку еще раз!", "error");
	    }
	});
}