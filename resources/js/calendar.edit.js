$(document).ready(function() {

	$('#calendar').fullCalendar({
		locale: 'ru',
		header: {
			left: 'month,agendaWeek,agendaDay',
			center: 'title',
			right: 'prev,today,next '
		},
		droppable: true,
		drop: function() {
			$(this).remove();
		},
		selectable: true,
		selectHelper: true,
		select: function(start, end) {
			loadCreateEventForm(start.format('DD.MM.YYYY HH:mm'), end.format('DD.MM.YYYY HH:mm'));
		},
		eventClick: function(calEvent, jsEvent, view) {
			loadEditEventForm(calEvent.id);
		},
		editable: false,
		eventLimit: false, 
		events: {
			url: '',
			method: 'POST',
			extraParams: {
				'csrf': $("meta[name='csrf-token']").attr("content"),
			},
			failure: function() {
				alert('there was an error while fetching events!');
			}
		}
	});

	$('#addEventModal').on('hidden.bs.modal', function (e) {
		$('#addEventModal').find('.modal-body').removeClass('is-loading').html('');
		if(loadEditFormAjax) loadEditFormAjax.abort();
	});
	$(document).on('click', '#addEventModal button.create-event', function() {
		$('#addEventModal').find('button[type=submit]').click();
	});
	$(document).on('submit', '#addEventModal form', function(e) {
		e.preventDefault();    

		var form = this;

    	var formData = new FormData(form);
		
    	$('#addEventModal').find('.modal-body').removeClass('is-loading').addClass('is-loading');

    	$(form).css('display', 'none');

    	if(loadEditFormAjax) loadEditFormAjax.abort();
		loadEditFormAjax = $.ajax({
	        url: $(form).attr('action'),
	        type: 'POST',
	        dataType: "json",
	        data: formData,
	        success: function (data) {
	            if(data.success) {
	            	$('#calendar').fullCalendar('refetchEvents');
					$('#addEventModal').modal('hide');
	            } else {
	            	$('#addEventModal').find('.modal-body').removeClass('is-loading');
	            	$(form).css('display', 'block');

	            	var span = document.createElement("span");
	            	span.innerHTML = data.errors;
	            	swal({
					    title: "Ошибка", 
					    content: span,
					    icon: "error",
					    allowOutsideClick: "true" 
					});
	            }
	        },
		    error: function (xhr, ajaxOptions, thrownError) {
		    	$('#addEventModal').modal('hide');
				swal("Ошибка!", "Повторите попытку еще раз!", "error");
		    },
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	});

	// Edit event

	$('#editEventModal').on('hidden.bs.modal', function (e) {
		$('#editEventModal').find('.modal-body').removeClass('is-loading').html('');
		if(loadEditFormAjax) loadEditFormAjax.abort();
	});
	$(document).on('click', '#editEventModal button.edit-event', function() {
		$('#editEventModal').find('button[type=submit]').click();
	});
	$(document).on('submit', '#editEventModal form', function(e) {
		e.preventDefault();    

		var form = this;

    	var formData = new FormData(form);
		
    	$('#editEventModal').find('.modal-body').removeClass('is-loading').addClass('is-loading');

    	$(form).css('display', 'none');

    	if(loadEditFormAjax) loadEditFormAjax.abort();
		loadEditFormAjax = $.ajax({
	        url: $(form).attr('action'),
	        type: 'POST',
	        dataType: "json",
	        data: formData,
	        success: function (data) {
	            if(data.success) {
					$('#calendar').fullCalendar('refetchEvents');
					$('#editEventModal').modal('hide');
	            } else {
	            	$('#editEventModal').find('.modal-body').removeClass('is-loading');
	            	$(form).css('display', 'block');

	            	var span = document.createElement("span");
	            	span.innerHTML = data.errors;
	            	swal({
					    title: "Ошибка", 
					    content: span,
					    icon: "error",
					    allowOutsideClick: "true" 
					});
	            }
	        },
		    error: function (xhr, ajaxOptions, thrownError) {
		    	$('#editEventModal').modal('hide');
				swal("Ошибка!", "Повторите попытку еще раз!", "error");
		    },
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	});

	$('body').on('click', '.delete-event', function(e) {

		e.preventDefault();

		var deleteUrl = $(this).attr('delete-link');
		var token = $("meta[name='csrf-token']").attr("content");

		swal({
			title: 'Вы уверены?',
			text: 'Это действие необратимое!',
			icon: 'error',
			buttons: {
				cancel: {
					text: 'Отменить',
					value: null,
					visible: true,
					closeModal: true,
				},
				confirm: {
					text: 'Удалить',
					value: true,
					visible: true,
					closeModal: false
				}
			}
		}).then(willDelete => {
			if (!willDelete) return;

			$.ajax({
	            url: deleteUrl,
	            type: "POST",
	            data: {
	            	'_method': 'DELETE',
	            	'_token': token
	            },
	            dataType: "html",
	            success: function () {
	                swal("Событие удалено!", {
				 		icon: "success"
				  	}).then(() => {
						$('#calendar').fullCalendar('refetchEvents');
						$('#editEventModal').modal('hide');
				  	});
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                swal("Ошибка!", "Событие не удалено!", "error");
	            }
	        });
		})
		.catch(err => {
			if (err) {
			  	swal("Ошибка!", "Событие не удалено!", "error");
			} else {
			  	swal.stopLoading();
    			swal.close();
			}
		});
	});
});

window.loadEditFormAjax = false;
window.loadCreateEventForm = function(start = '', end = '') {
	$('#addEventModal').modal('show');
	$('#addEventModal').find('.modal-body').removeClass('is-loading').addClass('is-loading');
	if(loadEditFormAjax) loadEditFormAjax.abort();
	loadEditFormAjax = $.ajax({
	    url: $('#addEventModal').attr('crate-link'),
	    type: "GET",
	    data: {
	    	'start': start,
	    	'end': end,
			'_token': $("meta[name='csrf-token']").attr("content")
	    },
	    success: function (data) {
	    	$('#addEventModal').find('.modal-body').removeClass('is-loading').html(data);
	    	afterLoadingFormFunctions();
	    },
	    error: function (xhr, ajaxOptions, thrownError) {
	    	$('#addEventModal').modal('hide');
			swal("Ошибка!", "Повторите попытку еще раз!", "error");
	    }
	});
}
window.loadEditEventForm = function(id) {
	$('#editEventModal').modal('show');
	$('#editEventModal').find('.modal-body').removeClass('is-loading').addClass('is-loading');
	if(loadEditFormAjax) loadEditFormAjax.abort();
	loadEditFormAjax = $.ajax({
	    url: $('#editEventModal').attr('edit-link'),
	    type: "GET",
	    data: {
	    	'id': id,
			'_token': $("meta[name='csrf-token']").attr("content")
	    },
	    success: function (data) {
	    	$('#editEventModal').find('.modal-body').removeClass('is-loading').html(data);
	    	afterLoadingFormFunctions();
	    },
	    error: function (xhr, ajaxOptions, thrownError) {
	    	$('#editEventModal').modal('hide');
			swal("Ошибка!", "Повторите попытку еще раз!", "error");
	    }
	});
}



window.afterLoadingFormFunctions = function() {
	if ($('.datetimepicker').length !== 0) {
		$('.datetimepicker').datetimepicker({
    	    locale: 'ru'
    	});
	}

	if ($('.colorpicker-element').length !== 0) {
		$('.colorpicker-element').colorpicker({format: 'hex'});
	}

	tinymce.init({
		selector: '.event-editor',
    	plugins: 'lists, link',
    	language: 'ru',
    	toolbar: 'h1 h2 bold italic strikethrough blockquote bullist numlist backcolor | link | removeformat',
    	menubar: false,
    	branding: false,
    	setup: function (editor) {
	        editor.on('change', function () {
	            editor.save();
	        });
	    }
	});
}