import videojs from 'video.js';

window.videojs = videojs;

$(document).ready(function() {
	if ($('#remotelearning-table').length !== 0) {
		$('#remotelearning-table').DataTable({
			'responsive': true,
			"searching": false,
			"info": false,
			"paging": false,
			"autoWidth": false,
			'language': {
		        'url': '/assets/plugins/datatables.net/js/ru.json'
		    },
		    "drawCallback": function () {
	            $('.dataTables_paginate > .pagination').addClass('flex-wrap');
	        }
		});
	}

	if ($('.lesson-clock').length !== 0) {
		clockUpdate('.lesson-clock');
	  	setInterval(function() { clockUpdate('.lesson-clock') }, 1000);
  	}

  	videojs.Hls.xhr.beforeRequest = function(options) {
  		options.uri = options.uri + '?v=' + $.now();
    	return options;
    };

  	if ($('#live-view').length !== 0) {
  		videojs('class-live-view').addClass('vjs-waiting');
		videojs('board-live-view').addClass('vjs-waiting');
		  
		videojs('class-live-view').on("pause", function () {
			videojs('board-live-view').pause();
		});
		videojs('class-live-view').on("play", function () {
			videojs('board-live-view').play();
		});
		videojs('board-live-view').on("pause", function () {
			videojs('class-live-view').pause();
		});
		videojs('board-live-view').on("play", function () {
			videojs('class-live-view').play();
		});

		checkIfStreamStared();
	}

	$('#recordModal').on('hidden.bs.modal', function (e) {
		var modalbody = $('#recordModal').find('.modal-body');
		var videos = modalbody.find('.video-js');
		videos.each(function() {
			var id = $(this).attr('id');
			videojs(id).dispose();
		});
		modalbody.removeClass('is-loading').html('');
		if(loadShowRecord) loadShowRecord.abort();
	});
});


window.muteUnmute = function(players) {
	players.forEach(function(e) {
		var muted = videojs(e).muted();
		videojs(e).muted(!muted);
	});
}

window.volumeDown = function(players) {
	players.forEach(function(e) {
		var volume = videojs(e).volume();
		if(volume > 0.2) {
			volume = volume - 0.1;
		} else {
			volume = 0.1;
		}
		videojs(e).volume(volume);
	});
}

window.volumeUp = function(players) {
	players.forEach(function(e) {
		var volume = videojs(e).volume();
		if(volume < 0.9) {
			volume = volume + 0.1;
		} else {
			volume = 1;
		}
		videojs(e).volume(volume);
	});
}

window.pauseVideo = function(players) {
	players.forEach(function(e) {
		videojs(e).pause();
	});
}

window.playVideo = function(players) {
	players.forEach(function(e) {
		videojs(e).play();
	});
}

window.loadShowRecord = false;
window.showRecord = function(lessson_id) {
	$('#recordModal').modal('show');
	$('#recordModal').find('.modal-body').empty();
	$('#recordModal').find('.modal-body').removeClass('is-loading').addClass('is-loading');
	if(loadShowRecord) loadShowRecord.abort();
	loadShowRecord = $.ajax({
	    url: $('#recordModal').attr('record-link'),
	    type: "POST",
	    data: {
			'_token': $("meta[name='csrf-token']").attr("content"),
			'lesson': lessson_id
	    },
	    success: function (data) {
	    	$('#recordModal').find('.modal-body').removeClass('is-loading').html(data);
	    },
	    error: function (xhr, ajaxOptions, thrownError) {
	    	$('#recordModal').modal('hide');
			swal("Ошибка!", "Повторите попытку еще раз!", "error");
	    }
	});
}

function checkIfStreamStared() {
	$.ajax({
	    url: $('#live-view').attr('status-url'),
	    type: "POST",
	    data: {
	    	'lesson_id': $('#live-view').attr('lesson-id'),
			'_token': $("meta[name='csrf-token']").attr("content")
	    },
	    dataType: "json",
	    success: function (data) {
			if(data.success) {
				videojs('class-live-view').removeClass('vjs-waiting');
				videojs('class-live-view').src({
					type: 'application/x-mpegURL', 
					src: $('#class-live-view').attr('video-source')
				});
				videojs('class-live-view').ready(function() {
					videojs('class-live-view').play();
				});

				videojs('board-live-view').removeClass('vjs-waiting');
				videojs('board-live-view').src({
					type: 'application/x-mpegURL',
					src: $('#board-live-view').attr('video-source')
				});
				videojs('board-live-view').ready(function() {
					videojs('board-live-view').play();
				});

				if($('#live-view').attr('set-control-text')) {
					$('.lesson-controll-button .btn').text($('#live-view').attr('set-control-text'));
				}
			} else {
				setTimeout(checkIfStreamStared, 1000);
			}
	    },
	    error: function (xhr, ajaxOptions, thrownError) {
			location.reload();
	    }
	});
}

function clockUpdate(clock_class) {
	var date = new Date();

	function addZero(x) {
		if (x < 10) {
			return x = '0' + x;
		} else {
			return x;
		}
	}

	var h = addZero(date.getHours());
	var m = addZero(date.getMinutes());
	var s = addZero(date.getSeconds());

	$(clock_class).text(h + ':' + m + ':' + s)
}

// Records

$(document).ready(function() {
	$.fn.datepicker.dates['ru'] = {
		days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
		daysShort: ["Вск", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Суб"],
		daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
		months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
		monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
		today: "Сегодня",
		clear: "Очистить",
		format: "dd.mm.yyyy",
		weekStart: 1,
    	monthsTitle: 'Месяцы'
	};
	$('.records-date').datepicker({
		todayHighlight: true,
		language: 'ru'
	}).on('changeDate', function(e) {
        showRecordsForDate($(this).parents('.records-lists'), e.date);
    });

    $('.records-lists').each(function() {
    	showRecordsForDate($(this), '');
    });
});

window.loadRecordsAjax = false;

function showRecordsForDate($element, $date) {
	if(window.loadRecordsAjax) window.loadRecordsAjax.abort();
	setPanelLoadingState($element.parents('.panel'), true);
	window.loadRecordsAjax = $.ajax({
	    url: '/dashboard/remotelearning/records',
	    type: "POST",
	    data: {
	    	'date': $date,
			'_token': $("meta[name='csrf-token']").attr("content")
	    },
	    dataType: "json",
	    success: function (data) {
			setPanelLoadingState($element.parents('.panel'), false);
	    	$element.find('.records-table').html(data.html);
	    },
	    error: function (xhr, ajaxOptions, thrownError) {
	        setPanelLoadingState($element.parents('.panel'), false);
			swal("Ошибка!", "Записи не загружены!", "error");
	    }
	});
}

function setPanelLoadingState($panel, $loading) {
	if($loading) {
		if(!$panel.hasClass('panel-loading')) {
			$panel.addClass('panel-loading');
			$panel.find('.panel-body').prepend('<div class="panel-loader"><span class="spinner-small"></span></div>');
		}
	} else {
		$panel.removeClass('panel-loading');
		if($panel.find('.panel-body .panel-loader').length > 0)
			$panel.find('.panel-body .panel-loader').remove();
	}
}