require('./theme-app.js');

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

$(document).ready(function() {
	if ($('#data-table-contacts').length !== 0) {
		$('#data-table-contacts').DataTable({
			'responsive': true,
			'language': {
		        'url': '/assets/plugins/datatables.net/js/ru.json'
		    },
		    "drawCallback": function () {
	            $('.dataTables_paginate > .pagination').addClass('flex-wrap');
	        }
		});
	}

	$(document).on('click', '[data-click="change-bg"]', function(e) {
		e.preventDefault();
		var targetImage = '[data-id="login-cover-image"]';
		var targetImageSrc = 'url(' + $(this).attr('data-img') +')';

		$(targetImage).css('background-image', targetImageSrc);
		$('[data-click="change-bg"]').closest('li').removeClass('active');
		$(this).closest('li').addClass('active');	
	});

	$( window ).resize(function() {
		$('.table').each(function() {
			$(this).css('width', '100%');
		});
	});

	$(document).on('click', '.extend-license', function(e) {
		var url = $(this).attr('url');
		$.ajax({
		    url: url,
		    type: "POST",
		    data: {
	    		'_token': $("meta[name='csrf-token']").attr("content")
		    },
		    dataType: "json",
		    success: function (data) {
		    	swal("Ваш запрос отправлен!", " Менеджер свяжется с Вами в ближайшее время.", "success");
		    },
		    error: function (xhr, ajaxOptions, thrownError) {
				swal("Ошибка!", "Запрос не отправлен!", "error");
		    }
		});
	});

	setTimeout(updateActivity, 10000);
});

function updateActivity() {
	$.ajax({
		url: '/update_activity',
		type: "POST",
		data: {
			'_token': $("meta[name='csrf-token']").attr("content")
		},
		dataType: "json",
		success: function (data) {
			setTimeout(updateActivity, 10000);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log('error');
			//location.reload();
		}
	});
}