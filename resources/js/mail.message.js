var handleEmailAction = function() {
	$(document).on('click', '[data-email-action]', function() {
		var action = $(this).attr('data-email-action');
		if(action == 'delete') {
			$('[data-email-action='+action+']').attr('disabled', true);
			$('[data-email-action='+action+']').removeClass('is-loading').addClass('is-loading');
			$.ajax({
		        url: '',
		        type: "POST",
		        data: {
		        	'_method': 'DELETE',
	        		'_token': $("meta[name='csrf-token']").attr("content")
		        },
		        dataType: "json",
		        success: function (data) {
		        	$('[data-email-action='+action+']').attr('disabled', false);
					$('[data-email-action='+action+']').removeClass('is-loading');
					if(data.error) {
						swal("Ошибка!", data.error, "error");
					} else if(data.url) {
						window.location.href = data.url;
					}
		        },
		        error: function (xhr, ajaxOptions, thrownError) {
		            $('[data-email-action='+action+']').attr('disabled', false);
					$('[data-email-action='+action+']').removeClass('is-loading');
					swal("Ошибка!", "Сообщение не удалено!", "error");
		        }
		    });
		} else {
			$.ajax({
		        url: '',
		        type: "POST",
		        data: {
		        	'_method': 'PUT',
		        	'action': action,
	        		'_token': $("meta[name='csrf-token']").attr("content")
		        },
		        dataType: "json",
		        success: function (data) {
		        	$('[data-email-action='+action+']').attr('disabled', false);
					$('[data-email-action='+action+']').removeClass('is-loading');
					if(data.error) {
						swal("Ошибка!", data.error, "error");
					} else if(data.url) {
						window.location.href = data.url;
					}
		        },
		        error: function (xhr, ajaxOptions, thrownError) {
		            $('[data-email-action='+action+']').attr('disabled', false);
					$('[data-email-action='+action+']').removeClass('is-loading');
					swal("Ошибка!", "Действие не выполнено!", "error");
		        }
		    });
		}
	});
};

var EmailMessage = function () {
	"use strict";
	return {
		//main function
		init: function () {
			handleEmailAction();
		}
	};
}();

$(document).ready(function() {
	EmailMessage.init();
});