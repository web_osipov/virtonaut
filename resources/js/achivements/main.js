$(document).ready(function() {
	if ($('.datatables').length !== 0) {
		$('.datatables').DataTable({
			'responsive': true,
			"order": [[ 4, "desc" ]],
			'language': {
		        'url': '/assets/plugins/datatables.net/js/ru.json'
		    },
		    "drawCallback": function () {
	            $('.dataTables_paginate > .pagination').addClass('flex-wrap');
	        }
		});
	}
});