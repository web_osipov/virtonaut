$(document).ready(function() {

    tinymce.init({
		selector: '.editor',
    	plugins: 'lists, link',
    	language: 'ru',
    	toolbar: 'h1 h2 bold italic strikethrough blockquote bullist numlist backcolor | link | removeformat',
    	menubar: false,
    	branding: false,
    	setup: function (editor) {
	        editor.on('change', function () {
	            editor.save();
	        });
	    }
	});

	if ($('#license-legalinfo-data-table').length !== 0) {
		$('#license-legalinfo-data-table').DataTable({
			'responsive': true,
			'autoWidth': false,
			'language': {
		        'url': '/assets/plugins/datatables.net/js/ru.json'
		    },
		    'processing': true,
		    'serverSide': true,
		    'serverMethod': 'GET',
		    'ajax': {
		    	'url':''
		    },
		    'columns': [
		    	{ data: 'id' },
                { data: 'name' },
                { data: 'groups' },
		    	{ data: 'buttons', class: "with-btn"},
		    ],
		    "drawCallback": function () {
	            $('.dataTables_paginate > .pagination').addClass('flex-wrap');
	        }
		});
	}


	$('body').on('click', '[data-click="swal-danger"]', function(e) {

		var deleteUrl = $(this).attr('data-delete-url');
		var token = $("meta[name='csrf-token']").attr("content");

		swal({
			title: 'Вы уверены?',
			text: 'Это действие необратимое!',
			icon: 'error',
			buttons: {
				cancel: {
					text: 'Отменить',
					value: null,
					visible: true,
					closeModal: true,
				},
				confirm: {
					text: 'Удалить',
					value: true,
					visible: true,
					closeModal: false
				}
			}
		}).then(willDelete => {
			if (!willDelete) return;

			$.ajax({
	            url: deleteUrl,
	            type: "POST",
	            data: {
	            	'_method': 'DELETE',
	            	'_token': token
	            },
	            dataType: "html",
	            success: function () {
	            	$('#license-legalinfo-data-table').DataTable().ajax.reload();
	                swal("Удалено!", {
				 		icon: "success",
				  	});
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                swal("Ошибка!", "Удаление не выполнено!", "error");
	            }
	        });
		})
		.catch(err => {
			if (err) {
			  	swal("Ошибка!", "Удаление не выполнено!", "error");
			} else {
			  	swal.stopLoading();
    			swal.close();
			}
		});
	});
});