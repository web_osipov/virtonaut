$(document).ready(function() {

	if($('#lesson-edit-form').length > 0) {
		updateLessonData();
		$(document).on('change', 'select[name=institution]', function() {
			updateLessonData();
		});
	}

	if ($('#datetimepicker').length !== 0) {
		$('#datetimepicker').datetimepicker({
    	    locale: 'ru'
    	});
	}

	if ($('#schedule-lessons-data-table').length !== 0) {
		$('#schedule-lessons-data-table').DataTable({
			'responsive': true,
			'autoWidth': false,
			'language': {
		        'url': '/assets/plugins/datatables.net/js/ru.json'
		    },
		    'processing': true,
		    'serverSide': true,
		    'serverMethod': 'GET',
		    'ajax': {
		    	'url':''
		    },
		    'columns': [
		    	{ data: 'id' },
		    	{ data: 'start' },
		    	{ data: 'class' },
		    	{ data: 'subject' },
		    	{ data: 'teacher' },
		    	{ data: 'institution' },
		    	{ data: 'buttons', class: "with-btn"},
		    ],
		    "drawCallback": function () {
	            $('.dataTables_paginate > .pagination').addClass('flex-wrap');
	        }
		});
	}


	$('body').on('click', '[data-click="swal-danger"]', function(e) {

		var deleteUrl = $(this).attr('data-delete-url');
		var token = $("meta[name='csrf-token']").attr("content");

		swal({
			title: 'Вы уверены?',
			text: 'Это действие необратимое!',
			icon: 'error',
			buttons: {
				cancel: {
					text: 'Отменить',
					value: null,
					visible: true,
					closeModal: true,
				},
				confirm: {
					text: 'Удалить',
					value: true,
					visible: true,
					closeModal: false
				}
			}
		}).then(willDelete => {
			if (!willDelete) return;

			$.ajax({
	            url: deleteUrl,
	            type: "POST",
	            data: {
	            	'_method': 'DELETE',
	            	'_token': token
	            },
	            dataType: "html",
	            success: function () {
	            	$('#schedule-lessons-data-table').DataTable().ajax.reload();
	                swal("Урок удален!", {
				 		icon: "success",
				  	});
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                swal("Ошибка!", "Удаление не выполнено!", "error");
	            }
	        });
		})
		.catch(err => {
			if (err) {
			  	swal("Ошибка!", "Удаление не выполнено!", "error");
			} else {
			  	swal.stopLoading();
    			swal.close();
			}
		});
	});
});


function updateLessonData() {
	var institution_id = $('select[name=institution]').val();

	$('select[name=class] optgroup').each(function() {
		if($(this).attr('institution-id') != institution_id) {
			$(this).attr('hidden', true);
			$(this).find('option').attr('selected', false).attr('hidden', true);
		} else {
			$(this).attr('hidden', false);
			$(this).find('option').attr('hidden', false);
		}
	});
	if($('select[name=class]').val().length == 0) $('select[name=class]').val();

	$('select[name=classroom] optgroup').each(function() {
		if($(this).attr('institution-id') != institution_id) {
			$(this).attr('hidden', true);
			$(this).find('option').attr('selected', false).attr('hidden', true);
		} else {
			$(this).attr('hidden', false);
			$(this).find('option').attr('hidden', false);
		}
	});
	if($('select[name=classroom]').val().length == 0) $('select[name=classroom]').val();

	$('select[name=teacher] optgroup').each(function() {
		if($(this).attr('institution-id') != institution_id) {
			$(this).attr('hidden', true);
			$(this).find('option').attr('selected', false).attr('hidden', true);
		} else {
			$(this).attr('hidden', false);
			$(this).find('option').attr('hidden', false);
		}
	});
	if($('select[name=teacher]').val().length == 0) $('select[name=teacher]').val();

	$('.selectpicker').selectpicker('refresh');
}