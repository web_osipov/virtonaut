import videojs from 'video.js';

window.videojs = videojs;


window.muteUnmute = function(players) {
	players.forEach(function(e) {
		var muted = videojs(e).muted();
		videojs(e).muted(!muted);
	});
}

window.volumeDown = function(players) {
	players.forEach(function(e) {
		var volume = videojs(e).volume();
		if(volume > 0.2) {
			volume = volume - 0.1;
		} else {
			volume = 0.1;
		}
		videojs(e).volume(volume);
	});
}

window.volumeUp = function(players) {
	players.forEach(function(e) {
		var volume = videojs(e).volume();
		if(volume < 0.9) {
			volume = volume + 0.1;
		} else {
			volume = 1;
		}
		videojs(e).volume(volume);
	});
}

window.pauseVideo = function(players) {
	players.forEach(function(e) {
		videojs(e).pause();
	});
}

window.playVideo = function(players) {
	players.forEach(function(e) {
		videojs(e).play();
	});
}

window.loadShowRecord = false;
window.showRecord = function(lessson_id) {
	$('#recordModal').modal('show');
	$('#recordModal').find('.modal-body').empty();
	$('#recordModal').find('.modal-body').removeClass('is-loading').addClass('is-loading');
	if(loadShowRecord) loadShowRecord.abort();
	loadShowRecord = $.ajax({
	    url: $('#recordModal').attr('record-link'),
	    type: "POST",
	    data: {
			'_token': $("meta[name='csrf-token']").attr("content"),
			'lesson': lessson_id
	    },
	    success: function (data) {
	    	$('#recordModal').find('.modal-body').removeClass('is-loading').html(data);
	    },
	    error: function (xhr, ajaxOptions, thrownError) {
	    	$('#recordModal').modal('hide');
			swal("Ошибка!", "Повторите попытку еще раз!", "error");
	    }
	});
}


$(document).ready(function() {
	$('#recordModal').on('hidden.bs.modal', function (e) {
		var modalbody = $('#recordModal').find('.modal-body');
		var videos = modalbody.find('.video-js');
		videos.each(function() {
			var id = $(this).attr('id');
			videojs(id).dispose();
		});
		modalbody.removeClass('is-loading').html('');
		if(loadShowRecord) loadShowRecord.abort();
	});

	

	$.fn.datepicker.dates['ru'] = {
		days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
		daysShort: ["Вск", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Суб"],
		daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
		months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
		monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
		today: "Сегодня",
		clear: "Очистить",
		format: "dd.mm.yyyy",
		weekStart: 1,
    	monthsTitle: 'Месяцы'
	};
	$('.schedule-date').datepicker({
		todayHighlight: true,
		language: 'ru'
	}).on('changeDate', function(e) {
        showScheduleForDate($(this).parents('.schedule'), e.date);
    });

    $('.schedule').each(function() {
    	showScheduleForDate($(this), '');
    });
});

window.loadScheduleAjax = false;

function showScheduleForDate($element, $date) {
	if(window.loadScheduleAjax) window.loadScheduleAjax.abort();
	setPanelLoadingState($element.parents('.panel'), true);
	window.loadScheduleAjax = $.ajax({
	    url: '',
	    type: "POST",
	    data: {
	    	'date': $date,
			'_token': $("meta[name='csrf-token']").attr("content")
	    },
	    dataType: "json",
	    success: function (data) {
	    	setPanelLoadingState($element.parents('.panel'), false);
	    	$element.find('.schedule-table').html(data.html);
	    },
	    error: function (xhr, ajaxOptions, thrownError) {
	        setPanelLoadingState($element.parents('.panel'), false);
			swal("Ошибка!", "Расписание не загружено!", "error");
	    }
	});
}

function setPanelLoadingState($panel, $loading) {
	if($loading) {
		if(!$panel.hasClass('panel-loading')) {
			$panel.addClass('panel-loading');
			$panel.find('.panel-body').prepend('<div class="panel-loader"><span class="spinner-small"></span></div>');
		}
	} else {
		$panel.removeClass('panel-loading');
		if($panel.find('.panel-body .panel-loader').length > 0)
			$panel.find('.panel-body .panel-loader').remove();
	}
}