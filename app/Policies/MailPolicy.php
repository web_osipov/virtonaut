<?php

namespace App\Policies;

use App\Mail;
use App\User;
use App\MailMessage;
use Illuminate\Auth\Access\HandlesAuthorization;

class MailPolicy
{
    use HandlesAuthorization;

    public function use(User $user)
    {
        $mails = $user->mails()->where('active', 1)->count();

        return $mails > 0;
    }

    public function view(User $user, Mail $model)
    {
        return $model->user_id == $user->id;
    }

    public function config(User $user)
    {
        return $user->hasGroup('admin');
    }
}
