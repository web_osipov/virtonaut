<?php

namespace App\Policies;

use App\License;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LicensePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasGroup('teacher') || $user->hasGroup('principal') || $user->hasGroup('support');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\License  $license
     * @return mixed
     */
    public function view(User $user, License $license)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\License  $license
     * @return mixed
     */
    public function update(User $user, License $license)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\License  $license
     * @return mixed
     */
    public function delete(User $user, License $license)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\License  $license
     * @return mixed
     */
    public function restore(User $user, License $license)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\License  $license
     * @return mixed
     */
    public function forceDelete(User $user, License $license)
    {
        //
    }
}
