<?php

namespace App\View\Components;

use Illuminate\View\Component;

class MailAttachment extends Component
{

    public $attachments;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($attachments)
    {
        foreach($attachments as $attachment) {
            $attachment->url = route('mail.attachment', $attachment->id);
            $attachment->icon = 'fa fa-file';
            if (strpos($attachment->name, '.pdf') !== false) {
                $attachment->icon = 'fa fa-file-pdf';
            } elseif (strpos($attachment->name, '.docx') !== false) {
                $attachment->icon = 'fa fa-file-word';
            } elseif (strpos($attachment->name, '.doc') !== false) {
                $attachment->icon = 'fa fa-file-word';
            } elseif (strpos($attachment->name, '.xls') !== false) {
                $attachment->icon = 'fa fa-file-excel';
            } elseif (strpos($attachment->name, '.xlsx') !== false) {
                $attachment->icon = 'fa fa-file-excel';
            }
        }

        $this->attachments = $attachments;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.mail-attachment');
    }
}
