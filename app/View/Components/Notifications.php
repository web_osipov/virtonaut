<?php

namespace App\View\Components;

use App\MailMessage;
use App\Achivement;
use Illuminate\View\Component;
use Illuminate\Support\Facades\Auth;

class Notifications extends Component
{
    public $notifications;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $user = Auth::user();

        $notifications = $user->notifications;

        foreach($notifications as $notification) {
            $data = $notification->data;
            if($notification->type == 'App\Notifications\NewAchivement') {
                $achivement = Achivement::find($data['achivement_id']);
                if(!empty($achivement)) {
                    $notification->link = route('achivement.index');
                    $notification->title = __('Новое достижение').' "'.$achivement->name.'"';
                    $notification->date = $notification->created_at->diffForHumans();
                } else {
                    $notification->delete();
                }
            } elseif($notification->type == 'App\Notifications\NewEmail') {
                $message = MailMessage::find($data['mail_message_id']);
                if(!empty($message)) {
                    $notification->link = route('mail.message', $message->id);
                    $notification->title = __('Новый Email');
                    $notification->date = $notification->created_at->diffForHumans();
                } else {
                    $notification->delete();
                }
            }
        }

        $this->notifications = $notifications;

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.notifications');
    }
}
