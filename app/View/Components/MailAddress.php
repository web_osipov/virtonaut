<?php

namespace App\View\Components;

use Illuminate\View\Component;

class MailAddress extends Component
{
    public $mails_title;

    public $mails;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($addresses, $title)
    {
        $this->mails_title = $title;

        $mails = [];

        foreach($addresses as $address) {
            $mails[] = $address['mail'];
        }

        $this->mails = implode(', ', $mails);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.mail-address');
    }
}
