<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Update extends Model
{
    protected $guarded = [];

    protected $casts = [
        'date' => 'datetime'
    ];

    public function user_groups()
    {
        return $this->belongsToMany(config('acl.models.group'), 'group_update');
    }

    public function author()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
