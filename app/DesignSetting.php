<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DesignSetting extends Model
{
    protected $guarded = [];
}
