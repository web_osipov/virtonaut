<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classroom extends Model
{
    protected $guarded = [];

    public function licenses()
    {
        return $this->belongsToMany('App\License')->withPivot('expires');
    }

    public function institution()
    {
        return $this->belongsToMany('App\Institution');
    }

    public function getInstitutionId()
    {
        $institution = $this->institution()->first();

        if(!empty($institution)) return $institution->id;

        return 0;
    }
}
