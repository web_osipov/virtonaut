<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LicenseType extends Model
{
    protected $guarded = [];

    protected $appends = ['icon_url'];

    protected $casts = [
        'blocks' => 'array'
    ];

    public function getIconUrlAttribute($value = null)
    {
        if(empty($this->icon)) {
            return '/images/no-icon.svg';
        }

        return '/storage/' . $this->icon;
    }
}
