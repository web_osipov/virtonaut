<?php

namespace App\Http\Controllers\RemoteLearning;

use Carbon\Carbon;
use App\Lesson;
use App\Classroom;
use App\Recording;
use App\Helpers\RemoteLearningHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

class RemoteLearningController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
    	$user = Auth::user();

    	$date = Carbon::today();
        $end_date = Carbon::today()->addDay();

    	if($user->hasGroup('teacher')) {

    		$recordings = Lesson::with('recording')->whereHas('recording', function (Builder $query) {
			    $query->where('status', 'finished');
			})->where('user_id', $user->id)->orderBy('start', 'desc')->paginate(2);


    		$live = Lesson::with('recording')
                ->where('user_id', $user->id)
                ->whereHas('recording', function (Builder $query) {
                    $query->where('status', '<>', 'finished');
                })
                ->orderBy('start', 'asc')
                ->first(); 
                
            $lessons = Lesson::with('recording')
                    ->whereHas('classroom', function (Builder $query) {
                        $query->where('no_cam', 0);
                    })
                    ->where('user_id', $user->id)
                    ->where('start', '>', $date)
                    ->where('start', '<', $end_date)
                    ->whereDoesntHave('recording')
                    ->orderBy('start', 'asc')
                    ->get();



    	} elseif($user->hasGroup('student')) {
    		$class = $user->classes()->first();

    		if(empty($class)) return redirect()->route('home');

    		$recordings = Lesson::with('recording')->whereHas('recording', function (Builder $query) {
                $query->where('status', 'finished');
            })->where('study_class_id', $class->id)->orderBy('start', 'desc')->paginate(2);

            $live = Lesson::with('recording')
                ->where('study_class_id', $class->id)
                ->whereHas('recording', function (Builder $query) {
                    $query->where('status', '<>', 'finished');
                })
                ->orderBy('start', 'asc')
                ->first();

            $lessons = Lesson::with('recording')
                    ->whereHas('classroom', function (Builder $query) {
                        $query->where('no_cam', 0);
                    })
                    ->where('study_class_id', $class->id)
                    ->where('start', '>', $date)
                    ->where('start', '<', $end_date)
                    ->whereDoesntHave('recording')
                    ->orderBy('start', 'asc')
                    ->get();

    	} else {
    		return redirect()->route('home');
    	}

        $classrooms = [];

        if($user->institution) {
           $classrooms = $user->institution->classrooms; 
        }

    	return view('dashboard.remotelearning.index', compact('recordings', 'live', 'lessons', 'classrooms'));
    }

    public function checkIfStreamStarted(Request $request)
    {
        $lesson_id = $request['lesson_id'];

        $lesson = Lesson::find($lesson_id);

        $user = Auth::user();

        $class_stream = storage_path('app/public/lesson/'.$lesson_id.'/class.m3u8');

        $board_stream = storage_path('app/public/lesson/'.$lesson_id.'/board.m3u8');

        if (file_exists($class_stream) && file_exists($board_stream)) {
            if($user->hasGroup('student') && $lesson->recording->status == 'created') {
                return ['success' => false];
            }
            return ['success' => true];
        }

        return ['success' => false];
    }

    public function controller(Request $request)
    {
    	$user = Auth::user();

    	$action = $request['action'];

    	$lesson_id = $request['lesson_id'];

    	if($action == 'create') {
    		RemoteLearningHelper::createLesson($lesson_id);
    	} elseif($action == 'start') {
    		RemoteLearningHelper::startLesson($lesson_id);
    	} elseif($action == 'end') {
    		RemoteLearningHelper::endLesson($lesson_id);
    	}

    	return redirect()->back();
    }

    public function records(Request $request)
    {
        if(empty($request->date)) {
            $date = Carbon::now();
        } else {
            $dateStarted = \DateTime::createFromFormat('D M d Y H:i:s e+', $request->date);
            $date = Carbon::parse($dateStarted);    
        }

        $user = Auth::user();

        $institution_id = 0;

        if(!empty($user->institution)) {
            $institution_id = $user->institution->id;
        }

        $lessons = collect([]);

        if($user->hasGroup('teacher')) {
            $lessons = Lesson::whereHas('institution', function (\Illuminate\Database\Eloquent\Builder $query) use ($institution_id) {
                $query->where('id', $institution_id);
            })->where('user_id', $user->id)->whereDate('start', $date->toDateString())->orderBy('start', 'asc')->get();
        } elseif($user->hasGroup('student')) {
            $class = $user->classes()->first();
            $class_id = !empty($class) ? $class->id : 0;
            $lessons = Lesson::whereHas('institution', function (\Illuminate\Database\Eloquent\Builder $query) use ($institution_id) {
                $query->where('id', $institution_id);
            })->where('study_class_id', $class_id)->whereDate('start', $date->toDateString())->orderBy('start', 'asc')->get();    
        }

        $counter = 1;
        foreach($lessons as $lesson) {
            $lesson->number = $counter;
            $lesson->time = Carbon::parse($lesson->start)->format('H:i');
            $counter++;
        }

        $output = ['error' => false];

        $output['html'] = view('dashboard.remotelearning.records', compact('lessons'))->render();
        
        return $output;
    }

    public function recordModal(Request $request)
    {
        $lesson = $request->lesson;
        $lesson = Lesson::with('recording')->whereHas('recording')->where('id', $lesson)->first();

        if(empty($lesson)) die();

        $recording = $lesson->recording;

        $view = view('dashboard.remotelearning.record_modal', compact('lesson', 'recording'));
        
        echo $view->render();
    }

}
