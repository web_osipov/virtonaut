<?php

namespace App\Http\Controllers;

use App\User;
use App\Lesson;
use App\Calendar;
use Carbon\Carbon;
use App\Institution;
use App\AchivementGroup;
use Faker\Factory as Faker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $user = Auth::user();

        $user_achivements = $user->achivements;

        $user_achivements = $user_achivements->pluck('id')->toArray();

        $user_groups = $user->groups->pluck('id')->toArray();

        $achivement_groups = AchivementGroup::whereHas('user_groups', function (Builder $query) use ($user_groups) {
            $query->whereIn('id', $user_groups);
        })->get();

        foreach($achivement_groups as $achivement_group) {
            $achivement_group->sorted_achivements = $achivement_group->achivements()->orderBy('sort')->get(); 
        }

        $contacts = $user->favorites;

        if($user->hasGroup('admin')) {
            $contacts = User::where('id', '<>', $user->id)->get();
        }

        $institutuions_count = Institution::count();

        $users_count = User::group('teacher')->count();

        $now = Carbon::now();

        $ended_events = Calendar::where('start', '<=', $now)->count();

        $future_events = Calendar::where('start', '>', $now)->count();


        // Licenses

        $institution = $user->institution;

        $classrooms = !empty($institution) ? $institution->classrooms : [];

        $licenses = [];

        $license_expires = false;

        foreach($classrooms as $classroom) {
            $classroom_licenses = $classroom->licenses;

            foreach($classroom_licenses as $classroom_license) {
                $expires = Carbon::parse($classroom_license->pivot->expires);
                if(!$license_expires) $license_expires = $expires;

                if($license_expires > $expires) $license_expires = $expires;
            }
        }

        if($license_expires && $license_expires < Carbon::now()) {
            $license_expires = false;
        }

        // student widgets data

        $live_lesson = $next_lesson = false;

        if($user->hasGroup('student')) {
            $class = $user->classes()->first();

            $institution_id = $user->institution;

            $live_lesson = Lesson::with('recording')
                ->where('study_class_id', $class->id)
                ->whereHas('recording', function (Builder $query) {
                    $query->where('status', '<>', 'finished');
                })
                ->orderBy('start', 'asc')
                ->first();

            $next_lesson = Lesson::where('study_class_id', $class->id)->whereDate('start', Carbon::now())->orderBy('start', 'asc')->first();   
        }

        

        return view('dashboard.infoblock', compact(
            'contacts', 
            'achivement_groups', 
            'user_achivements', 
            'institutuions_count',
            'users_count',
            'ended_events',
            'future_events',
            'license_expires',
            'live_lesson',
            'next_lesson'
        ));
    }
}
