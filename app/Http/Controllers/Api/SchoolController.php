<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\Institution;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SchoolController extends Controller
{
    public function license(Request $request)
    {
        $login = $request->login;

        $pass = $request->pass;

        if(empty($login) or empty($pass)) {
            return response()->json([
                'error' => 'Empty login or password'
            ], 403);
        }

        $institution = Institution::where('login', $login)->where('pass', $pass)->first();

        if(empty($institution)) {
            return response()->json([
                'error' => 'Wrong login or password'
            ], 403);
        }

        $classrooms = $institution->classrooms;

        $licenses = [];

        foreach($classrooms as $classroom) {
            $classroom_licenses = $classroom->licenses;

            foreach($classroom_licenses as $classroom_license) {
                $expires = Carbon::parse($classroom_license->pivot->expires);
                $licenses[] = [
                    'id' => $classroom_license->id,
                    'name' => $classroom_license->name,
                    'type' => $classroom_license->type->first()->name,
                    'description' => $classroom_license->description,
                    'image_url' => $classroom_license->type->first()->icon_url,
                    'expires' => $expires
                ];
            }
        }

        return $licenses;
    }
}
