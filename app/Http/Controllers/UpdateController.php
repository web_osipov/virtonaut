<?php

namespace App\Http\Controllers;

use Gate;
use App\User;
use App\Update;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Validator;

class UpdateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Gate::allows('configure', User::class)) {

            if($request->ajax()) {
                $output = [];
                $updates = new Update;
                $recordsTotal = $updates->count();
                $data = $request->all();
                $output['draw'] = $data['draw'];
                $output['recordsTotal'] = $recordsTotal;

                if(!empty($data['search']['value'])) {
                    $searchwheres = [];
                    $institutions_search = false;
                    $classroom_search = false;
                    $type_search = false;
                    foreach($data['columns'] as $column) {
                        if($column['searchable'] != 'false') {
                            $searchwheres[] = [$column['data'], 'like', '%'.$data['search']['value'].'%']; 
                        }
                    }
                    if(!empty($searchwheres)) {
                        $updates = $updates->where(function ($query) use ($searchwheres) {
                            foreach ($searchwheres as $searchwhere) {
                                $query->orWhere($searchwhere[0], $searchwhere[1], $searchwhere[2]);
                            }
                        });
                    } 
                }
                if(!empty($data['order'])) {
                    foreach($data['order'] as $order) {
                        $updates = $updates->orderBy($data['columns'][$order['column']]['data'], $order['dir']);
                    }
                }
                $recordsFiltered = $updates->count();
                $output['recordsFiltered'] = $recordsFiltered;
                $updates = $updates->skip($data['start'])->take($data['length']);
                $updates = $updates->get();
                $output['data'] = [];
                foreach($updates as $update) {
                    $output['data'][] = [
                        'id' => $update->id,
                        'active' => $update->active ? 'Да' : 'Нет',
                        'description' => $update->description,
                        'date' => $update->date->format('d.m.Y'),
                        'groups' => $update->user_groups()->pluck('name')->join(', '),
                        'buttons' => '<a href="'.route('update.edit', $update->id).'" class="btn btn-sm btn-primary m-r-2">'.(__('Редактировать')).'</a>'.'<a href="javascript:;" data-delete-url="'.route('update.destroy', $update->id).'" data-click="swal-danger" class="btn btn-sm btn-white">'.(__('Удалить')).'</a>'
                    ];
                }
            
                return $output;
            }
            
            return view('dashboard.update.index');

        } else {
            $user = Auth::user();
            
            $user_groups = $user->groups()->pluck('id')->toArray();
            
            $updates = Update::where('active', 1)->whereHas('user_groups', function (Builder $query) use ($user_groups) {
                $query->whereIn('id', $user_groups);
            })->orderBy('date', 'desc')->paginate(10);

            return view('dashboard.update.show', compact('updates'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('configure', User::class);

        $groupModel = app(config('acl.models.group'));

        $groups = $groupModel->all();

        return view('dashboard.update.create', compact('groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('configure', User::class);

        $validator = Validator::make($request->all(), [
            'description' => ['required'],
            'groups' => ['required'],
            'date' => ['required', 'date']
        ])->setAttributeNames([
            'description' => __('Описание'),
            'groups' => __('Группы пользователей'),
            'date' => __('Дата')
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
        }

        $user = Auth::user();

        $update = Update::create([
            'user_id' => $user->id,
            'active' => $request['active'] ? 1 : 0,
            'description' => $request['description'],
            'date' => $request['date']
        ]);

        $update->user_groups()->sync($request['groups']);

        return redirect()->route('update.index')->withSuccess(__('Обновление добавлено!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('configure', User::class);

        $update = Update::findOrFail($id);

        $groupModel = app(config('acl.models.group'));

        $groups = $groupModel->all();

        return view('dashboard.update.edit', compact('update', 'groups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('configure', User::class);

        $update = Update::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'description' => ['required'],
            'groups' => ['required'],
            'date' => ['required', 'date']
        ])->setAttributeNames([
            'description' => __('Описание'),
            'groups' => __('Группы пользователей'),
            'date' => __('Дата')
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
        }

        $user = Auth::user();

        $update->user_id = $user->id;

        $update->active = $request['active'] ? 1 : 0;

        $update->description = $request['description']; 

        $update->date = $request['date']; 

        $update->save();

        $update->user_groups()->sync($request['groups']);

        return redirect()->route('update.index')->withSuccess(__('Обновление сохранено!'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('configure', User::class);

        $update = Update::findOrFail($id);

        $update->delete();

        return true;
    }
}
