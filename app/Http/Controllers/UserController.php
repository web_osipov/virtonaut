<?php

namespace App\Http\Controllers;

use App\User;
use App\Mail;
use App\MailDomain;
use App\StudyClass;
use App\Subject;
use App\Institution;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Builder;


class UserController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('update', User::class);

        if($request->ajax()) {
            $output = [];
            $users = new User;
            $recordsTotal = $users->count();
            $data = $request->all();
            $output['draw'] = $data['draw'];
            $output['recordsTotal'] = $recordsTotal;

            if(!empty($data['search']['value'])) {
                $searchwheres = [];
                foreach($data['columns'] as $column) {
                    if($column['searchable'] != 'false') {
                        $searchwheres[] = [$column['data'], 'like', '%'.$data['search']['value'].'%'];
                    }
                }
                if(!empty($searchwheres)) {
                    $users = $users->where(function ($query) use ($searchwheres) {
                        foreach ($searchwheres as $searchwhere) {
                            $query->orWhere($searchwhere[0], $searchwhere[1], $searchwhere[2]);
                        }
                    });
                }
            }
            if(!empty($data['order'])) {
                foreach($data['order'] as $order) {
                    $users = $users->orderBy($data['columns'][$order['column']]['data'], $order['dir']);
                }
            }
            $recordsFiltered = $users->count();
            $output['recordsFiltered'] = $recordsFiltered;
            $users = $users->skip($data['start'])->take($data['length']);
            $users = $users->get();
            $output['data'] = [];
            foreach($users as $user) {
                $groups = $user->groups()->get()->pluck('name')->toArray();
                $output['data'][] = [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'groups' => implode(', ', $groups),
                    'buttons' => '<a href="'.route('user.edit', $user->id).'" class="btn btn-sm btn-primary m-r-2">'.(__('Редактировать')).'</a>'.($user->id == Auth::user()->id ? '' : '<a href="javascript:;" data-delete-url="'.route('user.destroy', $user->id).'" data-click="swal-danger" class="btn btn-sm btn-white">'.(__('Удалить')).'</a>')
                ];
            }
      
            return $output;
        }
        
        return view('dashboard.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('update', User::class);

        $groupModel = app(config('acl.models.group'));

        $groups = $groupModel->all();

        return view('dashboard.users.create', compact('groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('update', User::class);

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
            'groups' => ['required'],
        ])->setAttributeNames(['groups' => __('Группы')]);

        if ($validator->fails()) {
            return redirect(route('user.create'))
                        ->withErrors($validator)
                        ->withInput();
        }

        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password'])
        ]); 

        $user->syncGroups($request['groups']);

        return redirect()->route('user.index')->withSuccess(__('Пользователь создан!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $this->authorize('update', User::class);

        $settings_tab = in_array($request->tab, ['email-settings', 'schedule-settings', 'achivements-settings']) ? $request->tab : 'default-settings';

        if($settings_tab == 'email-settings') {
            return self::editEmail($id, $request);
        } elseif($settings_tab == 'schedule-settings') {
            return self::editSchedule($id, $request);
        } elseif($settings_tab == 'achivements-settings') {
            return self::editAchivements($id, $request);
        } else {
            return self::editDefault($id, $request);
        }
    }

    public function editDefault($id, Request $request)
    {
        $settings_tab = 'default-settings';

        $user = User::findOrFail($id);

        $groupModel = app(config('acl.models.group'));

        $groups = $groupModel->all();

        $subjects = Subject::orderBy('name')->get();

        $institutions = Institution::orderBy('full_name')->get();

        $classes = [];

        foreach($institutions as $institution) {
            $classes[$institution->id] = $institution->classes()->orderBy('name')->get();
        }

        return view('dashboard.users.edit', compact('user', 'classes', 'subjects', 'groups', 'institutions', 'settings_tab'));
    }

    public function editEmail($id, Request $request)
    {
        $settings_tab = 'email-settings';

        $user = User::findOrFail($id);

        $mail = $user->mails()->first();

        $mail_domains = MailDomain::all();

        return view('dashboard.users.edit', compact('user', 'settings_tab', 'mail', 'mail_domains'));
    }

    public function editAchivements($id, Request $request)
    {
        $settings_tab = 'achivements-settings';

        $user = User::findOrFail($id);

        $user_groups = $user->groups->pluck('id')->toArray();

        $achivement_groups = \App\AchivementGroup::whereHas('user_groups', function (Builder $query) use ($user_groups) {
            $query->whereIn('id', $user_groups);
        })->get();

        return view('dashboard.users.edit', compact('user', 'settings_tab', 'achivement_groups'));
    }

    public function editSchedule($id, Request $request)
    {
        $settings_tab = 'schedule-settings';

        $user = User::findOrFail($id);

        $classes = StudyClass::orderBy('name')->get();

        return view('dashboard.users.edit', compact('user', 'settings_tab', 'classes'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update', User::class);

        $settings = in_array($request->tab, ['email-settings', 'schedule-settings', 'achivements-settings']) ? $request->tab : 'default-settings';

        if($settings == 'email-settings') {
            return self::updateEmail($request, $id);
        } elseif($settings == 'schedule-settings') {
            return self::updateSchedule($request, $id);
        } elseif($settings == 'achivements-settings') {
            return self::updateAchivements($request, $id);
        } else {
            return self::updateDefault($request, $id);
        }
    }

    public function updateDefault(Request $request, $id)
    {
        $user = User::findOrFail($id);

        if($id == Auth::user()->id) $request->merge(['groups' => ['admin']]);

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$id],
            'password' => ['nullable', 'string', 'min:8'],
            'groups' => ['required'],
            'icon' => ['nullable', 'file', 'image', 'dimensions:ratio=1'],
            'session_limit' => ['nullable', 'integer', 'min:1', 'max:55']
        ])->setAttributeNames([
            'groups' => __('Группы'),
            'icon' => __('Иконка'),
            'session_limit' => __('Количество одновременных сессий')
        ]);

        if ($validator->fails()) {
            return redirect()->route('user.edit', $user->id)
                        ->withErrors($validator, 'default')
                        ->withInput();
        }

        $user->name = $request['name'];

        $user->email = $request['email'];

        $user->institution_id = $request['institution'];

        if(!empty($request['password'])) $user->password = Hash::make($request['password']);

        $user->save();

        $user->setData(['session_limit' => $request->session_limit ?? '']);

        $user->syncGroups($request['groups']);

        if(!empty($request['classes'])) {
            $user->classes()->sync($request['classes']);
        } else {
            $user->classes()->sync([]);
        }

        if(!empty($request['subjects'])) {
            $user->subjects()->sync($request['subjects']);
        } else {
            $user->subjects()->sync([]);
        }

        if(!empty($request->file('icon'))) {
            if(!empty($user->icon)) {
                Storage::disk('public')->delete($user->icon);
            }

            $path = $request->file('icon')->store(
                'user/'. $user->id, 'public'
            );
            $user->icon = $path;
            $user->save();
        } elseif($request->del_icon == 1) {
            if(!empty($user->icon)) {
                Storage::disk('public')->delete($user->icon);
            }
            $user->icon = '';
            $user->save();
        }

        return redirect()->route('user.index')->withSuccess(__('Данные сохранены!'));
    }

    public function updateSchedule(Request $request, $id)
    {
        $user = User::findOrFail($id);
        
        $data = [
            'edit-schedule-enabled' => $request->enabled,
            'edit-schedule-all-teachers' => $request->all_teachers,
            'edit-schedule-classes' => $request->classes,
        ];

        $user->setData($data);

        return redirect()->route('user.index')->withSuccess(__('Данные сохранены!'));
    }

    public function updateEmail(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'username' => [
                'required', 
                'string', 
                'max:255',
                'regex:/^[A-Za-z0-9.]*$/', 
                Rule::unique('mails')->ignore($user->id, 'user_id')->where(function ($query) use ($request) {
                    return $query->where('mail_domain_id', $request->domain);
                })
            ],
            'domain' => ['required', 'exists:mail_domains,id']
        ], [
            'username.regex' => __('Поле :attribute может содержать только английские буквы, цифры и точку.')
        ])->setAttributeNames([
            'username' => __('Никнейм'),
            'domain' => __('Домен')
        ]);

        if ($validator->fails()) {
            return redirect(route('user.edit', $user->id).'?tab=email-settings')
                        ->withErrors($validator, 'email')
                        ->withInput();
        }

        $mail = Mail::firstOrNew(
            ['user_id' => $user->id],
            ['password' => Str::random(16)]
        );

        $mail->username = $request->username;
        
        $mail->mail_domain_id = $request->domain;

        $mail->active = $request->active ? 1 : 0;

        $mail->save();  

        return redirect()->route('user.index')->withSuccess(__('Данные сохранены!'));
    }

    public function updateAchivements(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $achivements = (array) $request['achivements'];

        $user_achivements = $user->achivements;

        foreach($user_achivements as $achivement) {
            if(!in_array($achivement->id, $achivements)) {
                $user->achivements()->detach([
                    $achivement->id
                ]);
            }
        }

        foreach($achivements as $achivement) {
            if(!$user->hasAchivement($achivement)) {
                $achivement = \App\Achivement::find($achivement);

                $received = Carbon::now();

                $user->achivements()->attach([
                    $achivement->id => ['received' => $received]
                ]);

                $user->notify(new \App\Notifications\NewAchivement([
                    'achivement_id' => $achivement->id
                ]));
            }
        }

        return redirect()->route('user.index')->withSuccess(__('Данные сохранены!'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        $this->authorize('delete', $user);

        $user->delete();
        return true;
    }

    public function updateActivity()
    {
        return [true];
    }
}
