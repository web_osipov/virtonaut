<?php

namespace App\Http\Controllers;

use App\User;
use App\Institution;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MemberController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$user = Auth::user();

    	$institutions = Institution::all();

    	$members = $students = [];

    	if($user->hasGroup('teacher')) {
    		$members = User::whereDoesntHave('groups', function ($query) {
                $query->where('slug', 'student');
            })->where('id', '<>', $user->id)->get();
            $students = User::group('student')->where('institution_id', $user->institution_id)->get();
    	} elseif($user->hasGroup('student')) {
    		$members = User::group('teacher')->where('institution_id', $user->institution_id)->get();
    	} elseif($user->hasGroup('admin')) {
    		$members = User::whereDoesntHave('groups', function ($query) {
                $query->where('slug', 'student');
            })->where('id', '<>', $user->id)->get();
            $students = User::group('student')->get();
    	}

        if($user->hasGroup('student')) {
            return view('dashboard.member.student', compact('institutions', 'members', 'students'));
        }

    	return view('dashboard.member.index', compact('institutions', 'members', 'students'));
    }

    public function favorite(Request $request)
    {
    	$user = Auth::user();

    	$user_id = $request->user_id;

    	if(empty($user_id) || $user_id == $user->id) return false;

    	if($request->favorite) {
    		$user->favorites()->attach($user_id);
    	} else {
    		$user->favorites()->detach($user_id);
    	}

    	return true;
    }
}
