<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function clearAllNotifications(Request $reqest)
    {
    	$user = Auth::user();

        $user->notifications()->delete();

        return redirect()->back();
    }
}
