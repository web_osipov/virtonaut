<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Achivement;
use App\AchivementGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

class AchivementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        $user_groups = $user->groups->pluck('id')->toArray();

        $achivement_groups = AchivementGroup::whereHas('user_groups', function (Builder $query) use ($user_groups) {
            $query->whereIn('id', $user_groups);
        })->get();

        $user_achivements = $user->achivements;

        foreach($user_achivements as $user_achivement) {
            $user_achivement->received = Carbon::parse($user_achivement->pivot->received)->isoFormat('LL');
        }

        $user_achivements = $user_achivements->keyBy('id')->toArray();

        foreach($achivement_groups as $achivement_group) {
            $achivement_group->sorted_achivements = $achivement_group->achivements()->orderBy('sort')->get(); 
        }

        return view('dashboard.achivement.index', compact('achivement_groups', 'user_achivements'));
    }
}
