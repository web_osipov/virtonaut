<?php

namespace App\Http\Controllers\Settings;

use App\User;
use App\Classroom;
use App\Institution;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ClassroomController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('configure', User::class);

        if($request->ajax()) {
            $output = [];
            $classes = new Classroom;
            $recordsTotal = $classes->count();
            $data = $request->all();
            $output['draw'] = $data['draw'];
            $output['recordsTotal'] = $recordsTotal;

            if(!empty($data['search']['value'])) {
                $searchwheres = [];
                $institutions_search = false;
                foreach($data['columns'] as $column) {
                    if($column['searchable'] != 'false') {
                        if($column['data'] == 'institution') {
                            $institutions_search = true;
                        } else {
                            $searchwheres[] = [$column['data'], 'like', '%'.$data['search']['value'].'%']; 
                        }
                    }
                }
                if(!empty($searchwheres)) {
                    $classes = $classes->where(function ($query) use ($searchwheres) {
                        foreach ($searchwheres as $searchwhere) {
                            $query->orWhere($searchwhere[0], $searchwhere[1], $searchwhere[2]);
                        }
                    });
                } 
                if($institutions_search) {
                    $classes = $classes->orWhereHas('institution', function ($query) use ($data) {
                        $query->where('short_name', 'like', '%'.$data['search']['value'].'%');
                    });
                }
            }
            if(!empty($data['order'])) {
                foreach($data['order'] as $order) {
                    $classes = $classes->orderBy($data['columns'][$order['column']]['data'], $order['dir']);
                }
            }
            $recordsFiltered = $classes->count();
            $output['recordsFiltered'] = $recordsFiltered;
            $classes = $classes->skip($data['start'])->take($data['length']);
            $classes = $classes->get();
            $output['data'] = [];
            foreach($classes as $class) {
                $output['data'][] = [
                    'id' => $class->id,
                    'name' => $class->name,
                    'institution' => $class->institution->pluck('short_name')->join(', '),
                    'buttons' => '<a href="'.route('settings.classroom.edit', $class->id).'" class="btn btn-sm btn-primary m-r-2">'.(__('Редактировать')).'</a>'.'<a href="javascript:;" data-delete-url="'.route('settings.classroom.destroy', $class->id).'" data-click="swal-danger" class="btn btn-sm btn-white">'.(__('Удалить')).'</a>'
                ];
            }
      
            return $output;
        }
        
        return view('dashboard.settings.classroom.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('configure', User::class);

        $institutions = Institution::all();

        return view('dashboard.settings.classroom.create', compact('institutions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('configure', User::class);

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'class_cam' => ['required_unless:no_cam,1'],
            'board_cam' => ['required_unless:no_cam,1'],
            'institution' => ['required', 'exists:institutions,id']
        ], [
            'class_cam.required_unless' => __('Поле :attribute обязательно для заполнения, когда класс с камерами.'),
            'board_cam.required_unless' => __('Поле :attribute обязательно для заполнения, когда класс с камерами.'),
        ])->setAttributeNames([
        	'name' => __('Название'),
        	'class_cam' => __('RTSP камеры учебного класса'),
        	'board_cam' => __('RTSP камеры интерактивной доски'),
            'institution' => __('Учебное учреждение')
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $class = Classroom::create([
            'name' => $request['name'],
            'no_cam' => $request['no_cam'] ? 1 : 0,
            'class_cam' => $request['class_cam'],
            'board_cam' => $request['board_cam']
        ]); 

        $class->institution()->sync($request->institution);

        return redirect()->route('settings.classroom.index')->withSuccess(__('Учебный класс создан!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('configure', User::class);

        $class = Classroom::findOrFail($id);

        $institutions = Institution::all();

        return view('dashboard.settings.classroom.edit', compact('class', 'institutions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('configure', User::class);

        $class = Classroom::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'class_cam' => ['required_unless:no_cam,1'],
            'board_cam' => ['required_unless:no_cam,1'],
            'institution' => ['required', 'exists:institutions,id']
        ], [
            'class_cam.required_unless' => __('Поле :attribute обязательно для заполнения, когда класс с камерами.'),
            'board_cam.required_unless' => __('Поле :attribute обязательно для заполнения, когда класс с камерами.'),
        ])->setAttributeNames([
        	'name' => __('Название'),
        	'class_cam' => __('RTSP камеры учебного класса'),
        	'board_cam' => __('RTSP камеры интерактивной доски'),
            'institution' => __('Учебное учреждение')
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
        }

        $class->name = $request['name']; 

        $class->class_cam = $request['class_cam']; 

        $class->board_cam = $request['board_cam']; 

        $class->no_cam = $request['no_cam'] ? 1 : 0;

        $class->save();

        $class->institution()->sync($request->institution);

        return redirect()->route('settings.classroom.index')->withSuccess(__('Учебный класс сохранен!'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('configure', User::class);

        $class = Classroom::findOrFail($id);

        $class->delete();

        return true;
    }
}
