<?php

namespace App\Http\Controllers\Settings;

use App\User;
use App\Institution;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class InstitutionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('configure', User::class);

        if($request->ajax()) {
            $output = [];
            $institutions = new Institution;
            $recordsTotal = $institutions->count();
            $data = $request->all();
            $output['draw'] = $data['draw'];
            $output['recordsTotal'] = $recordsTotal;

            if(!empty($data['search']['value'])) {
                $searchwheres = [];
                foreach($data['columns'] as $column) {
                    if($column['searchable'] != 'false') {
                        $searchwheres[] = [$column['data'], 'like', '%'.$data['search']['value'].'%'];
                    }
                }
                if(!empty($searchwheres)) {
                    $institutions = $institutions->where(function ($query) use ($searchwheres) {
                        foreach ($searchwheres as $searchwhere) {
                            $query->orWhere($searchwhere[0], $searchwhere[1], $searchwhere[2]);
                        }
                    });
                }
            }
            if(!empty($data['order'])) {
                foreach($data['order'] as $order) {
                    $institutions = $institutions->orderBy($data['columns'][$order['column']]['data'], $order['dir']);
                }
            }
            $recordsFiltered = $institutions->count();
            $output['recordsFiltered'] = $recordsFiltered;
            $institutions = $institutions->skip($data['start'])->take($data['length']);
            $institutions = $institutions->get();
            $output['data'] = [];
            foreach($institutions as $institution) {
                $output['data'][] = [
                    'id' => $institution->id,
                    'full_name' => $institution->full_name,
                    'buttons' => '<a href="'.route('settings.institution.edit', $institution->id).'" class="btn btn-sm btn-primary m-r-2">'.(__('Редактировать')).'</a>'.'<a href="javascript:;" data-delete-url="'.route('settings.institution.destroy', $institution->id).'" data-click="swal-danger" class="btn btn-sm btn-white">'.(__('Удалить')).'</a>'
                ];
            }
      
            return $output;
        }
        
        return view('dashboard.settings.institution.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('configure', User::class);

        return view('dashboard.settings.institution.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('configure', User::class);

        $validator = Validator::make($request->all(), [
            'full_name' => ['required', 'string', 'max:255'],
            'short_name' => ['required', 'string', 'max:255'],
            'icon' => ['file', 'image'],
            'login' => ['nullable', 'string', 'max:255', 'unique:institutions,login'],
            'pass' => ['nullable', 'string', 'max:255'],
        ])->setAttributeNames([
            'full_name' => __('Полное название'),
            'short_name' => __('Краткое название'),
            'icon' => __('Иконка'),
            'login' => __('Логин'),
            'pass' => __('Пароль'),
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $institution = Institution::create([
            'full_name' => $request['full_name'],
            'short_name' => $request['short_name'],
            'address' => $request['address'],
            'phone' => $request['phone'],
            'mail' => $request['mail'],
            'site' => $request['site'],
            'principal' => $request['principal'],
            'login' => $request['login'],
            'pass' => $request['pass'],
        ]);

        if(!empty($request->file('icon'))) {
        	$path = $request->file('icon')->store(
			    'institution/'. $institution->id, 'public'
			);
        	$institution->icon = $path;
        	$institution->save();
        }

        return redirect()->route('settings.institution.index')->withSuccess(__('Учебное учреждение добавлено!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('configure', User::class);

        $institution = Institution::findOrFail($id);

        return view('dashboard.settings.institution.edit', compact('institution'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('configure', User::class);

        $institution = Institution::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'full_name' => ['required', 'string', 'max:255'],
            'short_name' => ['required', 'string', 'max:255'],
            'icon' => ['file', 'image'],
            'login' => ['nullable', 'string', 'max:255', 'unique:institutions,login,'.$institution->id],
            'pass' => ['nullable', 'string', 'max:255'],
        ])->setAttributeNames([
            'full_name' => __('Полное название'),
            'short_name' => __('Краткое название'),
            'icon' => __('Иконка'),
            'login' => __('Логин'),
            'pass' => __('Пароль'),
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $institution->full_name = $request['full_name'];

        $institution->short_name = $request['short_name'];

        $institution->address = $request['address'];

        $institution->phone = $request['phone'];

        $institution->mail = $request['mail'];

        $institution->site = $request['site'];

        $institution->principal = $request['principal'];

        $institution->login = $request['login'];

        $institution->pass = $request['pass'];

        $institution->save();

        if(!empty($request->file('icon'))) {
        	if(!empty($institution->icon)) {
        		Storage::disk('public')->delete($institution->icon);
        	}

        	$path = $request->file('icon')->store(
			    'institution/'. $institution->id, 'public'
			);
        	$institution->icon = $path;
        	$institution->save();
        } elseif($request->del_icon == 1) {
        	if(!empty($institution->icon)) {
        		Storage::disk('public')->delete($institution->icon);
        	}
            $institution->icon = '';
            $institution->save();
        }

        return redirect()->route('settings.institution.index')->withSuccess(__('Учебное учреждение сохранено!'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('configure', User::class);

        $institution = Institution::findOrFail($id);

        $institution->delete();

        return true;
    }
}
