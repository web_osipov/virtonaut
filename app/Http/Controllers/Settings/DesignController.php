<?php

namespace App\Http\Controllers\Settings;

use App\User;
use App\DesignSetting;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class DesignController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $this->authorize('configure', User::class);

        $settings = DesignSetting::all()->pluck('value', 'type')->toArray();

        return view('dashboard.design.index', compact('settings'));
    }

    public function store(Request $request)
    {
        $this->authorize('configure', User::class);

        $validator = Validator::make($request->all(), [
            'nav_bg' => ['nullable', 'file', 'image']
        ])->setAttributeNames([
        	'nav_bg' => __('Картинка навигационного меню')
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $settings = DesignSetting::all()->pluck('value', 'type')->toArray();

        if(!empty($request->file('nav_bg'))) {
        	if(!empty($settings['nav_bg'])) {
        		Storage::disk('public')->delete($settings['nav_bg']);
        	}

        	$path = $request->file('nav_bg')->store(
			    'design', 'public'
			);

			$data = DesignSetting::updateOrCreate(
			    ['type' => 'nav_bg'],
			    ['value' => $path]
			);
        } elseif($request->del_nav_bg == 1) {
        	if(!empty($settings['nav_bg'])) {
        		Storage::disk('public')->delete($settings['nav_bg']);
        		optional(DesignSetting::where('type', 'nav_bg')->first())->delete();
        	}
        }

        return redirect()->back()->withSuccess('Настройки сохранены!');
    }
}
