<?php

namespace App\Http\Controllers\Settings;

use App\User;
use App\Subject;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SubjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('configure', User::class);

        if($request->ajax()) {
            $output = [];
            $subjects = new Subject;
            $recordsTotal = $subjects->count();
            $data = $request->all();
            $output['draw'] = $data['draw'];
            $output['recordsTotal'] = $recordsTotal;

            if(!empty($data['search']['value'])) {
                $searchwheres = [];
                foreach($data['columns'] as $column) {
                    if($column['searchable'] != 'false') {
                        $searchwheres[] = [$column['data'], 'like', '%'.$data['search']['value'].'%'];
                    }
                }
                if(!empty($searchwheres)) {
                    $subjects = $subjects->where(function ($query) use ($searchwheres) {
                        foreach ($searchwheres as $searchwhere) {
                            $query->orWhere($searchwhere[0], $searchwhere[1], $searchwhere[2]);
                        }
                    });
                }
            }
            if(!empty($data['order'])) {
                foreach($data['order'] as $order) {
                    $subjects = $subjects->orderBy($data['columns'][$order['column']]['data'], $order['dir']);
                }
            }
            $recordsFiltered = $subjects->count();
            $output['recordsFiltered'] = $recordsFiltered;
            $subjects = $subjects->skip($data['start'])->take($data['length']);
            $subjects = $subjects->get();
            $output['data'] = [];
            foreach($subjects as $subject) {
                $output['data'][] = [
                    'id' => $subject->id,
                    'name' => $subject->name,
                    'buttons' => '<a href="'.route('settings.subject.edit', $subject->id).'" class="btn btn-sm btn-primary m-r-2">'.(__('Редактировать')).'</a>'.'<a href="javascript:;" data-delete-url="'.route('settings.subject.destroy', $subject->id).'" data-click="swal-danger" class="btn btn-sm btn-white">'.(__('Удалить')).'</a>'
                ];
            }
      
            return $output;
        }
        
        return view('dashboard.settings.subject.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('configure', User::class);

        return view('dashboard.settings.subject.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('configure', User::class);

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'unique:subjects'],
        ])->setAttributeNames(['name' => __('Название')]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $subject = Subject::create([
            'name' => $request['name']
        ]); 

        return redirect()->route('settings.subject.index')->withSuccess(__('Предмет создан!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('configure', User::class);

        $subject = Subject::findOrFail($id);

        return view('dashboard.settings.subject.edit', compact('subject'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('configure', User::class);

        $subject = Subject::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'unique:subjects,name,'.$id],
        ])->setAttributeNames(['name' => __('Название')]);

        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
        }

        $subject->name = $request['name']; 

        $subject->save();

        return redirect()->route('settings.subject.index')->withSuccess(__('Предмет сохранен!'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('configure', User::class);

        $subject = Subject::findOrFail($id);

        $subject->delete();

        return true;
    }
}
