<?php

namespace App\Http\Controllers\Settings;

use App\User;
use App\Achivement;
use App\AchivementGroup;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class AchivementGroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('configure', User::class);

        if($request->ajax()) {
            $output = [];
            $achivement_groups = new AchivementGroup;
            $recordsTotal = $achivement_groups->count();
            $data = $request->all();
            $output['draw'] = $data['draw'];
            $output['recordsTotal'] = $recordsTotal;

            if(!empty($data['search']['value'])) {
                $searchwheres = [];
                foreach($data['columns'] as $column) {
                    if($column['searchable'] != 'false') {
                        $searchwheres[] = [$column['data'], 'like', '%'.$data['search']['value'].'%'];
                    }
                }
                if(!empty($searchwheres)) {
                    $achivement_groups = $achivement_groups->where(function ($query) use ($searchwheres) {
                        foreach ($searchwheres as $searchwhere) {
                            $query->orWhere($searchwhere[0], $searchwhere[1], $searchwhere[2]);
                        }
                    });
                }
            }
            if(!empty($data['order'])) {
                foreach($data['order'] as $order) {
                    $achivement_groups->orderBy($data['columns'][$order['column']]['data'], $order['dir']);
                }
            }
            $recordsFiltered = $achivement_groups->count();
            $output['recordsFiltered'] = $recordsFiltered;
            $achivement_groups = $achivement_groups->skip($data['start'])->take($data['length']);
            $achivement_groups = $achivement_groups->get();
            $output['data'] = [];
            foreach($achivement_groups as $achivement_group) {
                $output['data'][] = [
                    'id' => $achivement_group->id,
                    'name' => $achivement_group->name,
                    'buttons' => '<a href="'.route('settings.achivement.group.edit', $achivement_group->id).'" class="btn btn-sm btn-primary m-r-2">'.(__('Редактировать')).'</a>'.'<a href="javascript:;" data-delete-url="'.route('settings.achivement.group.destroy', $achivement_group->id).'" data-click="swal-danger" class="btn btn-sm btn-white">'.(__('Удалить')).'</a>'
                ];
            }
      
            return $output;
        }
        
        return view('dashboard.settings.achivement.group.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('configure', User::class);

        $userGroupModel = app(config('acl.models.group'));

        $user_groups = $userGroupModel->all();

        return view('dashboard.settings.achivement.group.create', compact('user_groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('configure', User::class);

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'user_groups' => ['required']
        ])->setAttributeNames([
        	'name' => __('Название'),
        	'user_groups' => __('Группы пользователей')
        ]);

        if ($validator->fails()) {
            return redirect()->back()
            	->withErrors($validator)
            	->withInput();
        }

        $achivement_group = AchivementGroup::create([
            'name' => $request['name']
        ]);

        $achivement_group->user_groups()->sync($request['user_groups']);

        return redirect()->route('settings.achivement.group.index')->withSuccess(__('Группа достижений создана!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('configure', User::class);

        $achivement_group = AchivementGroup::findOrFail($id);

        $userGroupModel = app(config('acl.models.group'));

        $user_groups = $userGroupModel->all();

        return view('dashboard.settings.achivement.group.edit', compact('achivement_group', 'user_groups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('configure', User::class);

        $achivement_group = AchivementGroup::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'user_groups' => ['required']
        ])->setAttributeNames([
        	'name' => __('Название'),
        	'user_groups' => __('Группы пользователей')
        ]);

        if ($validator->fails()) {
            return redirect()->back()
            		->withErrors($validator)
            		->withInput();
        }

        $achivement_group->name = $request['name'];

        $achivement_group->save();

        $achivement_group->user_groups()->sync($request['user_groups']);

        return redirect()->route('settings.achivement.group.index')->withSuccess(__('Группа достижений сохранена!'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('configure', User::class);

        $achivement_group = AchivementGroup::findOrFail($id);

        $achivement_group->delete();

        return true;
    }
}
