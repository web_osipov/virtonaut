<?php

namespace App\Http\Controllers\Settings;

use App\User;
use App\Achivement;
use App\AchivementGroup;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class AchivementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('configure', User::class);

        if($request->ajax()) {
            $output = [];
            $achivements = new Achivement;
            $recordsTotal = $achivements->count();
            $data = $request->all();
            $output['draw'] = $data['draw'];
            $output['recordsTotal'] = $recordsTotal;

            if(!empty($data['search']['value'])) {
                $searchwheres = [];
                foreach($data['columns'] as $column) {
                    if($column['searchable'] != 'false') {
                        $searchwheres[] = [$column['data'], 'like', '%'.$data['search']['value'].'%'];
                    }
                }
                if(!empty($searchwheres)) {
                    $achivements = $achivements->where(function ($query) use ($searchwheres) {
                        foreach ($searchwheres as $searchwhere) {
                            $query->orWhere($searchwhere[0], $searchwhere[1], $searchwhere[2]);
                        }
                    });
                }
            }
            if(!empty($data['order'])) {
                foreach($data['order'] as $order) {
                    $achivements = $achivements->orderBy($data['columns'][$order['column']]['data'], $order['dir']);
                }
            }
            $recordsFiltered = $achivements->count();
            $output['recordsFiltered'] = $recordsFiltered;
            $achivements = $achivements->skip($data['start'])->take($data['length']);
            $achivements = $achivements->get();
            $output['data'] = [];
            foreach($achivements as $achivement) {
                $output['data'][] = [
                    'id' => $achivement->id,
                    'name' => $achivement->name,
                    'sort' => $achivement->sort,
                    'groups' => $achivement->groups->pluck('name')->join(', '),
                    'buttons' => '<a href="'.route('settings.achivement.edit', $achivement->id).'" class="btn btn-sm btn-primary m-r-2">'.(__('Редактировать')).'</a>'.'<a href="javascript:;" data-delete-url="'.route('settings.achivement.destroy', $achivement->id).'" data-click="swal-danger" class="btn btn-sm btn-white">'.(__('Удалить')).'</a>'
                ];
            }
      
            return $output;
        }
        
        return view('dashboard.settings.achivement.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('configure', User::class);

        $groups = AchivementGroup::all();

        return view('dashboard.settings.achivement.create', compact('groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('configure', User::class);

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'groups' => ['required'],
            'sort' => ['required', 'integer'],
            'icon' => ['file', 'image']
        ])->setAttributeNames([
            'name' => __('Название'),
            'groups' => __('Группы достижений'),
            'sort' => __('Сортировка'),
            'icon' => __('Иконка'),
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $achivement = Achivement::create([
            'name' => $request['name'],
            'description' => $request['description'],
            'sort' => $request['sort']
        ]);

        $achivement->groups()->sync($request['groups']);

        if(!empty($request->file('icon'))) {
        	$path = $request->file('icon')->store(
			    'achivement/'. $achivement->id, 'public'
			);
        	$achivement->icon = $path;
        	$achivement->save();
        }

        return redirect()->route('settings.achivement.index')->withSuccess(__('Достижение создано!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('configure', User::class);

        $achivement = Achivement::findOrFail($id);

        $groups = AchivementGroup::all();

        return view('dashboard.settings.achivement.edit', compact('achivement', 'groups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('configure', User::class);

        $achivement = Achivement::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'groups' => ['required'],
            'sort' => ['required', 'integer'],
            'icon' => ['file', 'image']
        ])->setAttributeNames([
            'name' => __('Название'),
            'groups' => __('Группы достижений'),
            'sort' => __('Сортировка'),
            'icon' => __('Иконка'),
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $achivement->name = $request['name'];

        $achivement->description = $request['description'];

        $achivement->sort = $request['sort'];

        $achivement->save();

        $achivement->groups()->sync($request['groups']);

        if(!empty($request->file('icon'))) {
        	if(!empty($achivement->icon)) {
        		Storage::disk('public')->delete($achivement->icon);
        	}

        	$path = $request->file('icon')->store(
			    'achivement/'. $achivement->id, 'public'
			);
        	$achivement->icon = $path;
        	$achivement->save();
        } elseif($request->del_icon == 1) {
        	if(!empty($achivement->icon)) {
        		Storage::disk('public')->delete($achivement->icon);
        	}
            $achivement->icon = '';
            $achivement->save();
        }

        return redirect()->route('settings.achivement.index')->withSuccess(__('Достижение сохранено!'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('configure', User::class);

        $achivement = Achivement::findOrFail($id);

        $achivement->delete();

        return true;
    }
}