<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        if(!empty($request->email)) {
            $user = User::where('email', $request->email)->first();
            if($user && !empty($user->getData('session_limit', true))) {
                if($user->valid_sessions >= $user->getData('session_limit', true)) {
                    throw ValidationException::withMessages([
                        $this->username() => [__('Слишком много одновременных соединений для данного пользователя, попробуйте позже')],
                    ])->status(Response::HTTP_TOO_MANY_REQUESTS);    
                }
            }
        }

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated(Request $request, $user)
    {
        $institution = $user->institution;

        $classrooms = !empty($institution) ? $institution->classrooms : [];

        $licenses = [];

        $license_expired = false;

        foreach($classrooms as $classroom) {
            $classroom_licenses = $classroom->licenses;

            foreach($classroom_licenses as $classroom_license) {
                $expires = Carbon::parse($classroom_license->pivot->expires);
                if($expires < Carbon::now()) {
                    $license_expired = [
                        'name' => $classroom_license->name,
                        'id' => $classroom_license->id
                    ];
                }
            }
        }

        if($user->hasGroup('teacher') || $user->hasGroup('principal') || $user->hasGroup('support')) {
            $request->session()->flash('license_expired', $license_expired);
        }
        
        return redirect()->intended($this->redirectTo);
    }
}
