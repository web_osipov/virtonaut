<?php

namespace App\Http\Controllers;

use App\Calendar;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class CalendarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function toggleEditMode(Request $request)
    {
        if(session('edit_calendar')) {
        	$request->session()->put('edit_calendar', false);
        } else {
        	$request->session()->put('edit_calendar', true);
        }

        return redirect()->back();
    }

    public function index(Request $request)
    {
        return view('dashboard.calendar.index');
    }

    public function show($id, Request $request)
    {
    	$event = Calendar::findOrFail($id);

    	$event->start_formated = $event->start->format('d.m.Y H:i');
        $event->end_formated = $event->end->format('d.m.Y H:i');
        $event->all_day = optional($event->data)['all_day'] ? 1 : 0;
    	$event->color = optional($event->data)['color'] ? $event->data['color'] : '';
        $event->color_text = optional($event->data)['color_text'] ? $event->data['color_text'] : '';

        $title_style = '';
        if(!empty($event->color)) $title_style .= 'background-color: '.$event->color.'; ';
        if(!empty($event->color_text)) $title_style .= 'color: '.$event->color_text.'; ';
        $event->title_style = $title_style;
    	
    	return view('dashboard.calendar.show', compact('event'));
    }

    public function events(Request $request)
    {
    	$start = Carbon::parse($request->start);
    	$end = Carbon::parse($request->end);

    	$events = Calendar::where('start', '>=', $start)
                            ->orWhere('end', '<=', $end)
                            ->orWhere([
                                ['start', '<', $start],
                                ['end', '>', $end],
                            ])
                            ->get();

    	$output = [];

    	foreach($events as $event) {
    		$event_output = [
    			'id' => $event->id,
    			'title' => $event->name,
    			'start' => $event->start->toIso8601String()
    		];
    		if(!empty($event->end)) {
    			$event_output['end'] = $event->end->toIso8601String();
    		}
    		if(!empty($event->data)) {
    			if(!empty($event->data['color'])) {
    				$event_output['color'] = $event->data['color'];
    			}
    			if(!empty($event->data['color_text'])) {
    				$event_output['textColor'] = $event->data['color_text'];
    			}
    			if(!empty($event->data['all_day'])) {
    				$event_output['allDay'] = true;
    			}
    		}
    		$output[] = $event_output;
    	}

    	return $output;
    }

    public function create(Request $request)
    {
    	$this->authorize('create', Calendar::class);

    	$start = $end = '';

    	if(!empty($request->start)) {
    		$start = Carbon::parse($request->start)->format('d.m.Y H:i');
    	}

    	if(!empty($request->end)) {
    		$end = Carbon::parse($request->end)->format('d.m.Y H:i');
    	}

       	return view('dashboard.calendar.add-form', compact('start', 'end')); 
    }

    public function store(Request $request)
    {
    	$this->authorize('create', Calendar::class);

        $validator = Validator::make($request->all(), [
        	'name' => ['required'],
            'start' => ['required'],
            'end' => ['required'],
            'picture' => ['nullable', 'image'],
        ])->setAttributeNames([
        	'name' => __('Название'),
        	'start' => __('Дата начала'),
        	'end' => __('Дата конца'),
        	'picture' => __('Картинка')
        ]);

        if ($validator->fails()) {

        	$errors = (array) $validator->errors()->all();

        	$errors = implode('<br>', $errors);

        	return ['success' => false, 'errors' => $errors];
        }

        $calendar = new Calendar;
        $calendar->start = Carbon::parse($request->start);
        if(!empty($request->end)) {
        	$calendar->end = Carbon::parse($request->end);
        }
        $calendar->name = $request->name;
        $calendar->content = $request->content;
        $data = [];
        if(!empty($request->all_day)) {
        	$data['all_day'] = true;
        }
        if(!empty($request->color)) {
        	$data['color'] = $request->color;
        }
        if(!empty($request->color_text)) {
        	$data['color_text'] = $request->color_text;
        }
        $calendar->data = $data;

        if(!empty($request->file('picture'))) {
        	$path = $request->file('picture')->store(
			    'event_picture'. $calendar->id, 'public'
			);
        	$calendar->picture = $path;
        }

        $calendar->save();

        return ['success' => true];
    }

    public function edit(Request $request)
    {
    	$this->authorize('create', Calendar::class);

        $id = $request->id;
        $event = Calendar::findOrFail($id);

        $event->start_formated = $event->start->format('d.m.Y H:i');
        $event->end_formated = $event->end->format('d.m.Y H:i');
        $event->all_day = optional($event->data)['all_day'] ? 1 : 0;
        $event->color = optional($event->data)['color'] ? $event->data['color'] : '';
        $event->color_text = optional($event->data)['color_text'] ? $event->data['color_text'] : '';

        return view('dashboard.calendar.edit-form', compact('event'));
    }

    public function update($id, Request $request)
    {
    	$this->authorize('create', Calendar::class);

        $validator = Validator::make($request->all(), [
        	'name' => ['required'],
            'start' => ['required', 'date'],
            'end' => ['required', 'date'],
            'picture' => ['nullable', 'image'],
        ])->setAttributeNames([
        	'name' => __('Название'),
        	'start' => __('Дата начала'),
        	'end' => __('Дата конца'),
        	'picture' => __('Картинка')
        ]);

        if ($validator->fails()) {

        	$errors = (array) $validator->errors()->all();

        	$errors = implode('<br>', $errors);

        	return ['success' => false, 'errors' => $errors];
        }

        $calendar = Calendar::findOrFail($id);
        if(!empty($request->start)) {
            $calendar->start = $request->start;
        }
        if(!empty($request->end)) {
        	$calendar->end = $request->end;
        }
        $calendar->name = $request->name;
        $calendar->content = $request->content;
        $data = [];
        if(!empty($request->all_day)) {
        	$data['all_day'] = true;
        }
        if(!empty($request->color)) {
        	$data['color'] = $request->color;
        }
        if(!empty($request->color_text)) {
        	$data['color_text'] = $request->color_text;
        }
        $calendar->data = $data;

        if(!empty($request->file('picture'))) {
        	if(!empty($calendar->picture)) {
        		Storage::disk('public')->delete($calendar->picture);
        	}

        	$path = $request->file('picture')->store(
			    'event_picture'. $calendar->id, 'public'
			);
        	$calendar->picture = $path;
        } elseif($request->del_picture == 1) {
        	if(!empty($calendar->picture)) {
        		Storage::disk('public')->delete($calendar->picture);
        	}
            $calendar->picture = '';
        }

        $calendar->save();

        return ['success' => true];
    }

    public function destroy($id, Request $request)
    {
    	$this->authorize('create', Calendar::class);

    	$calendar = Calendar::findOrFail($id);
    	if(!empty($calendar->picture)) {
        	Storage::disk('public')->delete($calendar->picture);
        }

        $calendar->delete();
    }
}
