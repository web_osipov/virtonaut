<?php

namespace App\Http\Controllers\Project;

use App\User;
use App\ProjectPlatform;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class ProjectPlatformController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('configure', User::class);

        $element = 'platform';

        if($request->ajax()) {
            $output = [];
            $elements = new ProjectPlatform;
            $recordsTotal = $elements->count();
            $data = $request->all();
            $output['draw'] = $data['draw'];
            $output['recordsTotal'] = $recordsTotal;

            if(!empty($data['search']['value'])) {
                $searchwheres = [];
                foreach($data['columns'] as $column) {
                    if($column['searchable'] != 'false') {
                        $searchwheres[] = [$column['data'], 'like', '%'.$data['search']['value'].'%'];
                    }
                }
                if(!empty($searchwheres)) {
                    $elements = $elements->where(function ($query) use ($searchwheres) {
                        foreach ($searchwheres as $searchwhere) {
                            $query->orWhere($searchwhere[0], $searchwhere[1], $searchwhere[2]);
                        }
                    });
                }
            }
            if(!empty($data['order'])) {
                foreach($data['order'] as $order) {
                    $elements = $elements->orderBy($data['columns'][$order['column']]['data'], $order['dir']);
                }
            }
            $recordsFiltered = $elements->count();
            $output['recordsFiltered'] = $recordsFiltered;
            $elements = $elements->skip($data['start'])->take($data['length']);
            $elements = $elements->get();
            $output['data'] = [];
            foreach($elements as $element) {
                $output['data'][] = [
                    'id' => $element->id,
                    'name' => $element->name,
                    'buttons' => '<a href="'.route('project.platform.edit', $element->id).'" class="btn btn-sm btn-primary m-r-2">'.(__('Редактировать')).'</a>'.'<a href="javascript:;" data-delete-url="'.route('project.platform.destroy', $element->id).'" data-click="swal-danger" class="btn btn-sm btn-white">'.(__('Удалить')).'</a>'
                ];
            }
      
            return $output;
        }
        
        return view('dashboard.project.platform.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('configure', User::class);

        return view('dashboard.project.platform.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('configure', User::class);

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'unique:project_platforms'],
            'description' => ['required'],
            'icon' => ['nullable', 'file', 'image']
        ])->setAttributeNames(['name' => __('Название'), 'icon' => __('Иконка')]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $platform = ProjectPlatform::create([
            'name' => $request['name'],
            'description' => $request['description']
        ]); 

        if(!empty($request->file('icon'))) {
        	$path = $request->file('icon')->store(
			    'project/platform/'. $platform->id, 'public'
			);
        	$platform->icon = $path;
        	$platform->save();
        }

        return redirect()->route('project.platform.index')->withSuccess(__('Платформа создана!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('configure', User::class);

        $platform = ProjectPlatform::findOrFail($id);

        return view('dashboard.project.platform.edit', compact('platform'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('configure', User::class);

        $platform = ProjectPlatform::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'unique:project_platforms,name,'.$id],
            'description' => ['required'],
            'icon' => ['nullable', 'file', 'image']
        ])->setAttributeNames(['name' => __('Название'), 'icon' => __('Иконка')]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $platform->name = $request['name'];

        $platform->description = $request['description'];

        $platform->save();

        if(!empty($request->file('icon'))) {
        	if(!empty($platform->icon)) {
        		Storage::disk('public')->delete($platform->icon);
        	}

        	$path = $request->file('icon')->store(
			    'project/platform/'. $platform->id, 'public'
			);
        	$platform->icon = $path;
        	$platform->save();
        } elseif($request->del_icon == 1) {
        	if(!empty($platform->icon)) {
        		Storage::disk('public')->delete($platform->icon);
        	}
            $platform->icon = '';
            $platform->save();
        }

        return redirect()->route('project.platform.index')->withSuccess(__('Платформа сохранена!'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('configure', User::class);

        $project = ProjectPlatform::findOrFail($id);

        $project->delete();

        return true;
    }
}
