<?php

namespace App\Http\Controllers\Project;

use App\User;
use App\Project;
use App\ProjectTag;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class ProjectTagController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('configure', User::class);

        if($request->ajax()) {
            $output = [];
            $elements = new ProjectTag;
            $recordsTotal = $elements->count();
            $data = $request->all();
            $output['draw'] = $data['draw'];
            $output['recordsTotal'] = $recordsTotal;

            if(!empty($data['search']['value'])) {
                $searchwheres = [];
                foreach($data['columns'] as $column) {
                    if($column['searchable'] != 'false') {
                        $searchwheres[] = [$column['data'], 'like', '%'.$data['search']['value'].'%'];
                    }
                }
                if(!empty($searchwheres)) {
                    $elements = $elements->where(function ($query) use ($searchwheres) {
                        foreach ($searchwheres as $searchwhere) {
                            $query->orWhere($searchwhere[0], $searchwhere[1], $searchwhere[2]);
                        }
                    });
                }
            }
            if(!empty($data['order'])) {
                foreach($data['order'] as $order) {
                    $elements = $elements->orderBy($data['columns'][$order['column']]['data'], $order['dir']);
                }
            }
            $recordsFiltered = $elements->count();
            $output['recordsFiltered'] = $recordsFiltered;
            $elements = $elements->skip($data['start'])->take($data['length']);
            $elements = $elements->get();
            $output['data'] = [];
            foreach($elements as $element) {
                $output['data'][] = [
                    'id' => $element->id,
                    'name' => $element->name,
                    'buttons' => '<a href="'.route('project.tag.edit', $element->id).'" class="btn btn-sm btn-primary m-r-2">'.(__('Редактировать')).'</a>'.'<a href="javascript:;" data-delete-url="'.route('project.tag.destroy', $element->id).'" data-click="swal-danger" class="btn btn-sm btn-white">'.(__('Удалить')).'</a>'
                ];
            }
      
            return $output;
        }
        
        return view('dashboard.project.tag.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('configure', User::class);

        return view('dashboard.project.tag.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('configure', User::class);

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'unique:project_tags']
        ])->setAttributeNames(['name' => __('Название')]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $tag = ProjectTag::create([
            'name' => $request['name']
        ]); 

        return redirect()->route('project.tag.index')->withSuccess(__('Тег создан!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $tag = ProjectTag::findOrFail($id);

        $status = $request->status;

        $projects = $tag->projects()->with('achivement', 'platforms', 'participants.subjects');

        if($status == 0 || $status == 1) {
            $projects = $projects->where('status', $status);
        }

        $projects = $projects->paginate(5);

        foreach($projects as $project) {
            if(!empty($project->images)) {
                foreach($project->images as $image) {
                    $project->image = $image;
                    break;
                }
            }
        }

        $response = [
            'pagination' => [
                'total' => $projects->total(),
                'per_page' => $projects->perPage(),
                'current_page' => $projects->currentPage(),
                'last_page' => $projects->lastPage(),
                'from' => $projects->firstItem(),
                'to' => $projects->lastItem()
            ],
            'data' => $projects
        ];

        return $response;
    }

    public function participationRequest($id, Request $request)
    {
        $user = Auth::user();

        $tag = ProjectTag::findOrFail($id);

        $project = Project::findOrFail($request->project_id);

        $admins = \App\User::group('admin')->get();

        foreach($admins as $admin) {

            $mail = \App\Mail::with('domain')->where('user_id', $admin->id)->first();

            if(empty($mail)) continue;

            $folders = \App\MailFolder::all()->keyBy('slug')->all();

            $email = [];
            $email['mail'] = $mail->username.'@'.$mail->domain->domain;
            $email['mailbox'] = $mail->username;
            $email['host'] = $mail->domain->domain;
            $email['personal'] = $admin->name;
            $email['full'] = $email['personal'].' <'. $email['mail'].'>';
            $email = [$email];

            $message = new \App\MailMessage;

            $message->subject = 'Новая заявка на участие в проетке';

            $message->message = 'Зарегистрирована новая заявка на участие в проекте "'.$project->name.' (id: '.$project->id.')".<br>
                Преподаватель: '.$user->name.' (id: '.$user->id.')';

            $message->from = $email;

            $message->to = $email;

            $message->reply_to = $email;

            $message->cc = collect([]);

            $message->bcc = collect([]);

            $message->mail_id = $mail->id;

            $message->mail_folder_id = $folders['inbox']->id;

            $message->save();  

            $admin->notify(new \App\Notifications\NewEmail([
                'mail_message_id' => $message->id
            ])); 

        }

        return true;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('configure', User::class);

        $tag = ProjectTag::findOrFail($id);

        return view('dashboard.project.tag.edit', compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('configure', User::class);

        $tag = ProjectTag::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'unique:project_tags,name,'.$id]
        ])->setAttributeNames(['name' => __('Название')]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $tag->name = $request['name'];

        $tag->save();

        return redirect()->route('project.tag.index')->withSuccess(__('Тег сохранен!'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('configure', User::class);

        $tag = ProjectTag::findOrFail($id);

        $tag->delete();

        return true;
    }
}
