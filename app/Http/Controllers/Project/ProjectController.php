<?php

namespace App\Http\Controllers\Project;

use App\User;
use App\Project;
use App\ProjectTag;
use App\ProjectPlatform;
use App\Achivement;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Gate;

class ProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Gate::allows('config', \App\Project::class)) {
			$element = 'project';

	        if($request->ajax()) {
	            $output = [];
	            $elements = new Project;
	            $recordsTotal = $elements->count();
	            $data = $request->all();
	            $output['draw'] = $data['draw'];
	            $output['recordsTotal'] = $recordsTotal;

	            if(!empty($data['search']['value'])) {
	                $searchwheres = [];
	                foreach($data['columns'] as $column) {
	                    if($column['searchable'] != 'false') {
	                        $searchwheres[] = [$column['data'], 'like', '%'.$data['search']['value'].'%'];
	                    }
	                }
	                if(!empty($searchwheres)) {
	                    $elements = $elements->where(function ($query) use ($searchwheres) {
	                        foreach ($searchwheres as $searchwhere) {
	                            $query->orWhere($searchwhere[0], $searchwhere[1], $searchwhere[2]);
	                        }
	                    });
	                }
	            }
	            if(!empty($data['order'])) {
	                foreach($data['order'] as $order) {
	                    $elements = $elements->orderBy($data['columns'][$order['column']]['data'], $order['dir']);
	                }
	            }
	            $recordsFiltered = $elements->count();
	            $output['recordsFiltered'] = $recordsFiltered;
	            $elements = $elements->skip($data['start'])->take($data['length']);
	            $elements = $elements->get();
	            $output['data'] = [];
	            foreach($elements as $element) {
	                $output['data'][] = [
	                    'id' => $element->id,
	                    'name' => $element->name,
	                    'status' => $element->status ? __('Доступен') : __('В работе'),
	                    'tags' => $element->tags->pluck('name')->join(', '),
	                    'buttons' => '<a href="'.route('project.edit', $element->id).'" class="btn btn-sm btn-primary m-r-2">'.(__('Редактировать')).'</a>'.'<a href="javascript:;" data-delete-url="'.route('project.destroy', $element->id).'" data-click="swal-danger" class="btn btn-sm btn-white">'.(__('Удалить')).'</a>'
	                ];
	            }
	      
	            return $output;
	        }
	        
	        return view('dashboard.project.index');
        } else {

        	$tags = ProjectTag::all();

        	return view('dashboard.project.show', compact('tags'));

        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('configure', User::class);

        $tags = ProjectTag::all();

        $platforms = ProjectPlatform::all();

        $teachers = User::group('teacher')->get();

        $achivements = Achivement::all();

        return view('dashboard.project.create', compact('tags', 'platforms', 'teachers', 'achivements'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('configure', User::class);

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'status' => ['required'],
            'content' => ['required'],
            'images.*' => ['nullable', 'file', 'image'],
            'achivement' => ['required'],
            'platforms' => ['required'],
            'tags' => ['required']
        ])->setAttributeNames([
        	'name' => __('Название'), 
        	'status' => __('Статус'),
        	'content' => __('Описание'),
        	'images.*' => __('Картинки'),
        	'achivement' => __('Достижение'),
        	'platforms' => __('Платформы'),
        	'tags' => __('Теги'),
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

    	$project = Project::create([
    	    'name' => $request['name'],
    	    'status' => $request['status'],
    	    'content' => $request['content'],
    	    'achivement_id' => $request['achivement'],
    	]);

    	if(!empty($request['platforms'])) {
    		$project->platforms()->sync($request['platforms']);
    	}

    	if(!empty($request['tags'])) {
    		$project->tags()->sync($request['tags']);
    	}

    	if(!empty($request['participants'])) {
    		$project->participants()->sync($request['participants']);
    	}


    	if(!empty($request->file('images'))) {
    		$images = [];
    		foreach($request->file('images') as $image_key => $image) {
	    		$path = $image->store(
				    'project/project/'. $project->id, 'public'
				);
				$images[$image_key] = $path;
    		}
	    	$project->images = $images;
	    	$project->save();	
    	}

        return redirect()->route('project.index')->withSuccess(__('Проект создан!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('configure', User::class);

        $project = Project::findOrFail($id);

        $tags = ProjectTag::all();

        $platforms = ProjectPlatform::all();

        $teachers = User::group('teacher')->get();

        $achivements = Achivement::all();

        return view('dashboard.project.edit', compact('project', 'tags', 'platforms', 'teachers', 'achivements'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('configure', User::class);

        $project = Project::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'status' => ['required'],
            'content' => ['required'],
            'images.*' => ['file', 'image'],
            'achivement' => ['required'],
            'platforms' => ['required'],
            'tags' => ['required']
        ])->setAttributeNames([
        	'name' => __('Название'), 
        	'status' => __('Статус'),
        	'content' => __('Описание'),
        	'images.*' => __('Картинки'),
        	'achivement' => __('Достижение'),
        	'platforms' => __('Платформы'),
        	'tags' => __('Теги'),
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $project->name = $request['name'];

        $project->status = $request['status'];

        $project->content = $request['content'];

        $project->achivement_id = $request['achivement'];

        $project->save();

    	if(!empty($request['platforms'])) {
    		$project->platforms()->sync($request['platforms']);
    	}

    	if(!empty($request['tags'])) {
    		$project->tags()->sync($request['tags']);
    	}

    	if(!empty($request['participants'])) {
    		$project->participants()->sync($request['participants']);
    	}

    	if(!empty($request->del_images)) {
    		$images = $project->images;
    		foreach($request->del_images as $image_key => $del_status) {
	    		if(!empty($images[$image_key])) {
	        		Storage::disk('public')->delete($images[$image_key]);
	        		unset($images[$image_key]);
	        	}
    		}
    		$project->images = $images;
	    	$project->save();
        }

    	if(!empty($request->file('images'))) {
    		$images = $project->images;
    		foreach($request->file('images') as $image_key => $image) {
	    		$path = $image->store(
				    'project/project/'. $project->id, 'public'
				);
				$images[$image_key] = $path;
    		}
	    	$project->images = $images;
	    	$project->save();	
    	}

        return redirect()->route('project.index')->withSuccess(__('Платформа сохранена!'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('configure', User::class);

        $project = Project::findOrFail($id);

        $project->delete();

        return true;
    }
}
