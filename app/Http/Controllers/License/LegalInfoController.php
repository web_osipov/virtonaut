<?php

namespace App\Http\Controllers\License;

use App\User;
use App\LegalInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LegalInfoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $output = [];
            $legalinfos = new LegalInfo;
            $recordsTotal = $legalinfos->count();
            $data = $request->all();
            $output['draw'] = $data['draw'];
            $output['recordsTotal'] = $recordsTotal;

            if(!empty($data['search']['value'])) {
                $searchwheres = [];
                $institutions_search = false;
                $classroom_search = false;
                $type_search = false;
                foreach($data['columns'] as $column) {
                    if($column['searchable'] != 'false') {
                        $searchwheres[] = [$column['data'], 'like', '%'.$data['search']['value'].'%']; 
                    }
                }
                if(!empty($searchwheres)) {
                    $legalinfos = $legalinfos->where(function ($query) use ($searchwheres) {
                        foreach ($searchwheres as $searchwhere) {
                            $query->orWhere($searchwhere[0], $searchwhere[1], $searchwhere[2]);
                        }
                    });
                } 
            }
            if(!empty($data['order'])) {
                foreach($data['order'] as $order) {
                    $legalinfos = $legalinfos->orderBy($data['columns'][$order['column']]['data'], $order['dir']);
                }
            }
            $recordsFiltered = $legalinfos->count();
            $output['recordsFiltered'] = $recordsFiltered;
            $legalinfos = $legalinfos->skip($data['start'])->take($data['length']);
            $legalinfos = $legalinfos->get();
            $output['data'] = [];
            foreach($legalinfos as $legalinfo) {
                $output['data'][] = [
                    'id' => $legalinfo->id,
                    'name' => $legalinfo->name,
                    'groups' => $legalinfo->user_groups()->pluck('name')->join(', '),
                    'buttons' => '<a href="'.route('license.legalinfo.edit', $legalinfo->id).'" class="btn btn-sm btn-primary m-r-2">'.(__('Редактировать')).'</a>'.'<a href="javascript:;" data-delete-url="'.route('license.legalinfo.destroy', $legalinfo->id).'" data-click="swal-danger" class="btn btn-sm btn-white">'.(__('Удалить')).'</a>'
                ];
            }
        
            return $output;
        }
        
        return view('dashboard.license.legalinfo.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('configure', User::class);

        $groupModel = app(config('acl.models.group'));

        $groups = $groupModel->all();

        return view('dashboard.license.legalinfo.create', compact('groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('configure', User::class);

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'short_description' => ['nullable'],
            'description' => ['required'],
            'groups' => ['required']
        ])->setAttributeNames([
            'name' => __('Наименование'),
            'short_description' => __('Краткое описание'),
            'description' => __('Текст'),
            'groups' => __('Группы пользователей')
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
        }

        $legalinfo = LegalInfo::create([
            'name' => $request['name'],
            'short_description' => $request['short_description'],
            'description' => $request['description']
        ]);

        $legalinfo->user_groups()->sync($request['groups']);


        return redirect()->route('license.legalinfo.index')->withSuccess(__('Правовая информация добавлена!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('configure', User::class);

        $legalinfo = LegalInfo::findOrFail($id);

        $groupModel = app(config('acl.models.group'));

        $groups = $groupModel->all();

        return view('dashboard.license.legalinfo.edit', compact('legalinfo', 'groups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('configure', User::class);

        $legalinfo = LegalInfo::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'short_description' => ['nullable'],
            'description' => ['required'],
            'groups' => ['required']
        ])->setAttributeNames([
            'name' => __('Наименование'),
            'short_description' => __('Краткое описание'),
            'description' => __('Текст'),
            'groups' => __('Группы пользователей')
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
        }

        $legalinfo->name = $request['name'];

        $legalinfo->short_description = $request['short_description'];

        $legalinfo->description = $request['description'];

        $legalinfo->save();

        $legalinfo->user_groups()->sync($request['groups']);

        return redirect()->route('license.legalinfo.index')->withSuccess(__('Правовая информация сохранена!'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('configure', User::class);

        $legalinfo = LegalInfo::findOrFail($id);

        $legalinfo->delete();

        return true;
    }

    public function toggle($id)
    {
        $legalinfo = LegalInfo::findOrFail($id);

        $user = Auth::user();

        $legalinfo->users()->toggle([$user->id]);

        return true;
    }
}
