<?php

namespace App\Http\Controllers\License;

use Gate;
use App\User;
use App\License;
use App\Classroom;
use App\LegalInfo;
use Carbon\Carbon;
use App\LicenseType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Validator;

class LicenseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Gate::allows('configure', User::class)) {

            if($request->ajax()) {
                $output = [];
                $licenses = new License;
                $recordsTotal = $licenses->count();
                $data = $request->all();
                $output['draw'] = $data['draw'];
                $output['recordsTotal'] = $recordsTotal;

                if(!empty($data['search']['value'])) {
                    $searchwheres = [];
                    $institutions_search = false;
                    $classroom_search = false;
                    $type_search = false;
                    foreach($data['columns'] as $column) {
                        if($column['searchable'] != 'false') {
                            if($column['data'] == 'institution') {
                                $institutions_search = true;
                            } elseif($column['data'] == 'classroom') {
                                $classroom_search = true;
                            } elseif($column['data'] == 'type') {
                                $type_search = true;
                            } else {
                                $searchwheres[] = [$column['data'], 'like', '%'.$data['search']['value'].'%']; 
                            }
                        }
                    }
                    if(!empty($searchwheres)) {
                        $licenses = $licenses->where(function ($query) use ($searchwheres) {
                            foreach ($searchwheres as $searchwhere) {
                                $query->orWhere($searchwhere[0], $searchwhere[1], $searchwhere[2]);
                            }
                        });
                    } 
                    if($institutions_search) {
                        $licenses = $licenses->orWhereHas('classroom', function ($query) use ($data) {
                            $query->whereHas('institution', function ($query) use ($data) {
                                $query->where('short_name', 'like', '%'.$data['search']['value'].'%');
                            });
                        });
                    }
                    if($classroom_search) {
                        $licenses = $licenses->orWhereHas('classroom', function ($query) use ($data) {
                            $query->where('name', 'like', '%'.$data['search']['value'].'%');
                        });
                    }
                    if($type_search) {
                        $licenses = $licenses->orWhereHas('type', function ($query) use ($data) {
                            $query->where('name', 'like', '%'.$data['search']['value'].'%');
                        });
                    }
                }
                if(!empty($data['order'])) {
                    foreach($data['order'] as $order) {
                        if($data['columns'][$order['column']]['data'] == 'institution') {
                            
                        } elseif($data['columns'][$order['column']]['data'] == 'classroom') {

                        } elseif($data['columns'][$order['column']]['data'] == 'type') {

                        } elseif($data['columns'][$order['column']]['data'] == 'expires') {

                        } else {
                            $licenses = $licenses->orderBy($data['columns'][$order['column']]['data'], $order['dir']);
                        }
                    }
                }
                $recordsFiltered = $licenses->count();
                $output['recordsFiltered'] = $recordsFiltered;
                $licenses = $licenses->skip($data['start'])->take($data['length']);
                $licenses = $licenses->get();
                $output['data'] = [];
                foreach($licenses as $license) {
                    $output['data'][] = [
                        'id' => $license->id,
                        'name' => $license->name,
                        'institution' => $license->classroom->first()->institution->pluck('short_name')->join(', '),
                        'classroom' => $license->classroom->pluck('name')->join(', '),
                        'type' => $license->type->pluck('name')->join(', '),
                        'expires' => $license->classroom->first()->pivot->expires,
                        'buttons' => '<a href="'.route('license.edit', $license->id).'" class="btn btn-sm btn-primary m-r-2">'.(__('Редактировать')).'</a>'.'<a href="javascript:;" data-delete-url="'.route('license.destroy', $license->id).'" data-click="swal-danger" class="btn btn-sm btn-white">'.(__('Удалить')).'</a>'
                    ];
                }
            
                return $output;
            }
            
            return view('dashboard.license.index');

        } else {
            $this->authorize('viewAny', License::class);

            $user = Auth::user();

            $institution = $user->institution;

            $classrooms = $institution->classrooms;

            $licenses = [];


            $user_groups = $user->groups()->pluck('id')->toArray();
            
            $legalinfos = LegalInfo::whereHas('user_groups', function (Builder $query) use ($user_groups) {
                $query->whereIn('id', $user_groups);
            })->get();

            foreach($classrooms as $classroom) {
                $classroom_licenses = $classroom->licenses;

                foreach($classroom_licenses as $classroom_license) {
                    $expires = Carbon::parse($classroom_license->pivot->expires);
                    $licenses[] = [
                        'id' => $classroom_license->id,
                        'name' => $classroom_license->name,
                        'type' => $classroom_license->type->first()->name,
                        'description' => $classroom_license->description,
                        'image_url' => $classroom_license->type->first()->icon_url,
                        'expires' => 'До '.$expires->format('d.m.Y').' '.$expires->diffForHumans()
                    ];
                }
            }

            return view('dashboard.license.show', compact('legalinfos', 'classrooms', 'licenses'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('configure', User::class);

        $types = LicenseType::all();

        $classrooms = Classroom::all();

        return view('dashboard.license.create', compact('types', 'classrooms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('configure', User::class);

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'type' => ['required', 'exists:license_types,id'],
            'classroom' => ['required', 'exists:classrooms,id'],
            'expires' => ['required'],
        ])->setAttributeNames([
            'name' => __('Название'),
            'type' => __('Тип'),
            'classroom' => __('Учебный класс'),
            'expires' => __('Дата окончания')
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
        }

        $license = License::create([
            'name' => $request['name'],
            'description' => $request['description']
        ]);

        $license->type()->sync($request['type']);

        $license->classroom()->sync([
            $request['classroom'] => [
                'expires' => Carbon::parse($request['expires'])
            ]
        ]);

        return redirect()->route('license.index')->withSuccess(__('Лицензия добавлена!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('configure', User::class);

        $license = License::findOrFail($id);

        $types = LicenseType::all();

        $classrooms = Classroom::all();

        $expires = Carbon::parse($license->classroom->first()->pivot->expires)->format('d.m.Y H:i');

        return view('dashboard.license.edit', compact('license', 'types', 'classrooms', 'expires'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('configure', User::class);

        $license = License::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'type' => ['required', 'exists:license_types,id'],
            'classroom' => ['required', 'exists:classrooms,id'],
            'expires' => ['required'],
        ])->setAttributeNames([
            'name' => __('Название'),
            'type' => __('Тип'),
            'classroom' => __('Учебный класс'),
            'expires' => __('Дата окончания')
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
        }

        $license->name = $request['name'];

        $license->description = $request['description']; 

        $license->save();

        $license->type()->sync($request['type']);

        $license->classroom()->sync([
            $request['classroom'] => [
                'expires' => Carbon::parse($request['expires'])
            ]
        ]);

        return redirect()->route('license.index')->withSuccess(__('Лицензия сохранена!'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('configure', User::class);

        $license = License::findOrFail($id);

        $license->delete();

        return true;
    }

    public function extend($id)
    {
        $license = License::findOrFail($id);

        $user = Auth::user();

        $institution = $user->institution;

        $classrooms = $institution->classrooms;

        $licenses = [];

        foreach($classrooms as $classroom) {
            $classroom_licenses = $classroom->licenses;

            foreach($classroom_licenses as $classroom_license) {
                $expires = Carbon::parse($classroom_license->pivot->expires);
                $licenses[$classroom_license->id] = [
                    'id' => $classroom_license->id,
                    'name' => $classroom_license->name,
                    'type' => $classroom_license->type->first()->name,
                    'description' => $classroom_license->description,
                    'expires' => 'До '.$expires->format('d.m.Y').' '.$expires->diffForHumans()
                ];
            }
        }

        if($licenses[$classroom_license->id]) {
            $admins = \App\User::group('admin')->get();

            foreach($admins as $admin) {

                $mail = \App\Mail::with('domain')->where('user_id', $admin->id)->first();

                if(empty($mail)) continue;

                $folders = \App\MailFolder::all()->keyBy('slug')->all();

                $email = [];
                $email['mail'] = $mail->username.'@'.$mail->domain->domain;
                $email['mailbox'] = $mail->username;
                $email['host'] = $mail->domain->domain;
                $email['personal'] = $admin->name;
                $email['full'] = $email['personal'].' <'. $email['mail'].'>';
                $email = [$email];

                $message = new \App\MailMessage;

                $message->subject = 'Заявка на продление лицензии';

                $message->message = 'Зарегистрирована новая заявка на продление лицензии <a href="'.route("license.edit", $license->id).'">'.$license->name.'</a> от участника <a href="'.route("user.edit", $user->id).'">'.$user->name.'</a>.';

                $message->from = $email;

                $message->to = $email;

                $message->reply_to = $email;

                $message->cc = collect([]);

                $message->bcc = collect([]);

                $message->mail_id = $mail->id;

                $message->mail_folder_id = $folders['inbox']->id;

                $message->save();  

                $admin->notify(new \App\Notifications\NewEmail([
                    'mail_message_id' => $message->id
                ])); 

            }
        }

        return true;
    }
}
