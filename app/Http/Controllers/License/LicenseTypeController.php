<?php

namespace App\Http\Controllers\License;

use App\User;
use App\LicenseType;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class LicenseTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('configure', User::class);

        if($request->ajax()) {
            $output = [];
            $types = new LicenseType;
            $recordsTotal = $types->count();
            $data = $request->all();
            $output['draw'] = $data['draw'];
            $output['recordsTotal'] = $recordsTotal;

            if(!empty($data['search']['value'])) {
                $searchwheres = [];
                $institutions_search = false;
                foreach($data['columns'] as $column) {
                    if($column['searchable'] != 'false') {
                        if($column['data'] == 'institution') {
                            $institutions_search = true;
                        } else {
                            $searchwheres[] = [$column['data'], 'like', '%'.$data['search']['value'].'%']; 
                        }
                    }
                }
                if(!empty($searchwheres)) {
                    $types = $types->where(function ($query) use ($searchwheres) {
                        foreach ($searchwheres as $searchwhere) {
                            $query->orWhere($searchwhere[0], $searchwhere[1], $searchwhere[2]);
                        }
                    });
                } 
                if($institutions_search) {
                    $types = $types->orWhereHas('institution', function ($query) use ($data) {
                        $query->where('short_name', 'like', '%'.$data['search']['value'].'%');
                    });
                }
            }
            if(!empty($data['order'])) {
                foreach($data['order'] as $order) {
                    $types = $types->orderBy($data['columns'][$order['column']]['data'], $order['dir']);
                }
            }
            $recordsFiltered = $types->count();
            $output['recordsFiltered'] = $recordsFiltered;
            $types = $types->skip($data['start'])->take($data['length']);
            $types = $types->get();
            $output['data'] = [];
            foreach($types as $type) {
                $output['data'][] = [
                    'id' => $type->id,
                    'name' => $type->name,
                    'buttons' => '<a href="'.route('license.type.edit', $type->id).'" class="btn btn-sm btn-primary m-r-2">'.(__('Редактировать')).'</a>'.'<a href="javascript:;" data-delete-url="'.route('license.type.destroy', $type->id).'" data-click="swal-danger" class="btn btn-sm btn-white">'.(__('Удалить')).'</a>'
                ];
            }
      
            return $output;
        }
        
        return view('dashboard.license.type.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('configure', User::class);

        return view('dashboard.license.type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('configure', User::class);

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'icon' => ['nullable', 'file', 'image'],
            'blocks' => ['nullable'],
        ])->setAttributeNames([
            'name' => __('Название'),
            'icon' => __('Иконка')
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $type = LicenseType::create([
            'name' => $request['name'],
            'blocks' => $request['blocks']
        ]);

        if(!empty($request->file('icon'))) {
            $path = $request->file('icon')->store(
                'license/type/'. $type->id, 'public'
            );
            $type->icon = $path;
            $type->save();
        }


        return redirect()->route('license.type.index')->withSuccess(__('Тип лицензии создан!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('configure', User::class);

        $type = LicenseType::findOrFail($id);

        return view('dashboard.license.type.edit', compact('type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('configure', User::class);

        $type = LicenseType::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'icon' => ['nullable', 'file', 'image'],
            'blocks' => ['nullable'],
        ])->setAttributeNames([
            'name' => __('Название'),
            'icon' => __('Иконка')
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
        }

        $type->name = $request['name']; 

        $type->blocks = $request['blocks']; 

        $type->save();

        if(!empty($request->file('icon'))) {
            if(!empty($type->icon)) {
                Storage::disk('public')->delete($type->icon);
            }

            $path = $request->file('icon')->store(
                'license/type/'. $type->id, 'public'
            );
            $type->icon = $path;
            $type->save();
        } elseif($request->del_icon == 1) {
            if(!empty($type->icon)) {
                Storage::disk('public')->delete($type->icon);
            }
            $type->icon = '';
            $type->save();
        }

        return redirect()->route('license.type.index')->withSuccess(__('Тип лицензии сохранен!'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('configure', User::class);

        $type = LicenseType::findOrFail($id);

        $type->delete();

        return true;
    }
}
