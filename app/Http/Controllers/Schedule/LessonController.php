<?php

namespace App\Http\Controllers\Schedule;

use App\User;
use App\Lesson;
use App\Subject;
use App\StudyClass;
use App\Institution;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use Illuminate\Database\Eloquent\Builder;

class LessonController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('create', Lesson::class);

        $user = Auth::user();

        if($request->ajax()) {
            $output = [];
            $lessons = new Lesson;

            if(!$user->hasGroup('admin')) {
                if(!empty($user->institution)) {
                    $lessons = $lessons->whereHas('institution', function (\Illuminate\Database\Eloquent\Builder $query) use ($user) {
                        $query->where('id', $user->institution->id);
                    });
                } else {
                    $lessons = $lessons->whereHas('institution', function (\Illuminate\Database\Eloquent\Builder $query) {
                        $query->where('id', 0);
                    });    
                }
            }

            $recordsTotal = $lessons->count();
            $data = $request->all();
            $output['draw'] = $data['draw'];
            $output['recordsTotal'] = $recordsTotal;

            if(!empty($data['search']['value'])) {
                $searchwheres = [];
                foreach($data['columns'] as $column) {
                    if($column['searchable'] != 'false') {
                        $searchwheres[] = [$column['data'], 'like', '%'.$data['search']['value'].'%'];
                    }
                }
                if(!empty($searchwheres)) {
                    $lessons = $lessons->where(function ($query) use ($searchwheres) {
                        foreach ($searchwheres as $searchwhere) {
                            $query->orWhere($searchwhere[0], $searchwhere[1], $searchwhere[2]);
                        }
                    });
                }
            }
            if(!empty($data['order'])) {
                foreach($data['order'] as $order) {
                    $lessons = $lessons->orderBy($data['columns'][$order['column']]['data'], $order['dir']);
                }
            }
            $recordsFiltered = $lessons->count();
            $output['recordsFiltered'] = $recordsFiltered;
            $lessons = $lessons->skip($data['start'])->take($data['length']);
            $lessons = $lessons->get();
            $output['data'] = [];
            foreach($lessons as $lesson) {
                $output['data'][] = [
                    'id' => $lesson->id,
                    'start' => Carbon::parse($lesson->start)->isoFormat('LLLL'),
                    'class' => $lesson->class->name,
                    'subject' => $lesson->subject->name,
                    'teacher' => $lesson->teacher->name,
                    'institution' => !empty($lesson->getInstitution()) ? $lesson->getInstitution()->short_name : '',
                    'buttons' => '<a href="'.route('schedule.lesson.edit', $lesson->id).'" class="btn btn-sm btn-primary m-r-2">'.(__('Редактировать')).'</a>'.'<a href="javascript:;" data-delete-url="'.route('schedule.lesson.destroy', $lesson->id).'" data-click="swal-danger" class="btn btn-sm btn-white">'.(__('Удалить')).'</a>'
                ];
            }
      
            return $output;
        }
        
        return view('dashboard.schedule.lesson.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Lesson::class);

        $user = Auth::user();

        $institutions = $classes = $subjects = $teachers = $classrooms = [];
        if($user->hasGroup('admin')) {
            $institutions = Institution::all();
        } else {
            if(!empty($user->institution)) {
                $institutions = Institution::where('id', $user->institution->id)->get();
            }
        }

        foreach($institutions as $institution) {
            $classrooms[$institution->id] = $institution->classrooms;
            $classes[$institution->id] = $institution->classes;
            $teachers[$institution->id] = $institution->teachers();
        }

        $subjects = Subject::all();

        return view('dashboard.schedule.lesson.create', compact('institutions', 'classes', 'classrooms', 'subjects', 'teachers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Lesson::class);

        $validator = Validator::make($request->all(), [
            'institution' => ['required', 'exists:institutions,id'],
            'teacher' => ['required'],
            'subject' => ['required'],
            'class' => ['required'],
            'classroom' => ['required'],
            'start' => ['required']
        ])->setAttributeNames([
            'institution' => __('Учебное учреждение'),
            'teacher' => __('Преподаватель'),
            'subject' => __('Предмет'),
            'class' => __('Класс'),
            'classroom' => __('Учебный класс'),
            'start' => __('Начало'),
            'topic' => __('Тема')
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $lesson = Lesson::create([
            'user_id' => $request['teacher'],
            'subject_id' => $request['subject'],
            'study_class_id' => $request['class'],
            'start' => Carbon::parse($request['start']),
            'topic' => $request['topic']
        ]);

        $lesson->institution()->sync($request->institution);

        $lesson->classroom()->sync($request->classroom);

        return redirect()->route('schedule.lesson.index')->withSuccess(__('Урок добавлен!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('create', Lesson::class);

        $lesson = Lesson::findOrFail($id);

        $user = Auth::user();

        $institutions = $classes = $subjects = $teachers = $classrooms = [];
        if($user->hasGroup('admin')) {
            $institutions = Institution::all();
        } else {
            if(!empty($user->institution)) {
                $institutions = Institution::where('id', $user->institution->id)->get();
            }
        }

        foreach($institutions as $institution) {
            $classrooms[$institution->id] = $institution->classrooms;
            $classes[$institution->id] = $institution->classes;
            $teachers[$institution->id] = $institution->teachers();
        }

        $subjects = Subject::all();

        $lesson->start = Carbon::parse($lesson->start)->format('d.m.Y H:i');

        return view('dashboard.schedule.lesson.edit', compact('lesson', 'institutions', 'classes', 'classrooms', 'subjects', 'teachers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('create', Lesson::class);

        $lesson = Lesson::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'institution' => ['required', 'exists:institutions,id'],
            'teacher' => ['required'],
            'subject' => ['required'],
            'class' => ['required'],
            'classroom' => ['required'],
            'start' => ['required']
        ])->setAttributeNames([
            'institution' => __('Учебное учреждение'),
            'teacher' => __('Преподаватель'),
            'subject' => __('Предмет'),
            'class' => __('Класс'),
            'classroom' => __('Учебный класс'),
            'start' => __('Начало'),
            'topic' => __('Тема')
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $lesson->user_id = $request['teacher'];
        $lesson->subject_id = $request['subject'];
        $lesson->study_class_id = $request['class'];
        $lesson->start = Carbon::parse($request['start']);
        $lesson->topic = $request['topic'];

        $lesson->save();

        $lesson->institution()->sync($request->institution);
        $lesson->classroom()->sync($request->classroom);

        return redirect()->route('schedule.lesson.index')->withSuccess(__('Урок сохранен!'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('create', Lesson::class);

        $lesson = Lesson::findOrFail($id);

        $lesson->delete();

        return true;
    }
}
