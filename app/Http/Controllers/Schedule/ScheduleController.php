<?php

namespace App\Http\Controllers\Schedule;

use Carbon\Carbon;
use App\StudyClass;
use App\Lesson;
use App\Institution;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ScheduleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();

        $classes = collect([]);

        $institution_id = false;

        if($user->hasGroup('admin')) {
            if($request->institution) {
                $institution_id = $request->institution;
            }
        } else {
            if(!empty($user->institution)) {
                $institution_id = $user->institution->id;
            }
        }

        if($institution_id) {
            $classes = StudyClass::whereHas('institution', function (\Illuminate\Database\Eloquent\Builder $query) use ($institution_id) {
                $query->where('id', $institution_id);
            })->get()->sortBy(function ($class, $key) {
                return intval($class['name']);
            });
        }

        $institutions = Institution::all();

        return view('dashboard.schedule.index', compact('classes', 'institutions'));
    }

    public function loadAjax(Request $request)
    {
        if(empty($request->date)) {
            $date = Carbon::now();
        } else {
            $dateStarted = \DateTime::createFromFormat('D M d Y H:i:s e+', $request->date);
            $date = Carbon::parse($dateStarted);    
        }

        $class = $request->class;

        $user = Auth::user();

        $institution_id = 0;

        if($user->hasGroup('admin')) {
            if($request->institution) {
                $institution_id = $request->institution;
            }
        } else {
            if(!empty($user->institution)) {
                $institution_id = $user->institution->id;
            }
        }
        
        $lessons = Lesson::whereHas('institution', function (\Illuminate\Database\Eloquent\Builder $query) use ($institution_id) {
            $query->where('id', $institution_id);
        })->where('study_class_id', $class)->whereDate('start', $date->toDateString())->orderBy('start', 'asc')->get();

        $counter = 1;
        foreach($lessons as $lesson) {
            $lesson->number = $counter;
            $lesson->time = Carbon::parse($lesson->start)->format('H:i');
            $counter++;
        }

        $output = ['error' => false];

        $output['html'] = view('dashboard.schedule.lessons', compact('lessons'))->render();

        return $output;
    }

    public function mySchedule(Request $request)
    {
        $user = Auth::user();

        return view('dashboard.schedule.myschedule');
    }

    public function loadAjaxMySchedule(Request $request)
    {
        if(empty($request->date)) {
            $date = Carbon::now();
        } else {
            $dateStarted = \DateTime::createFromFormat('D M d Y H:i:s e+', $request->date);
            $date = Carbon::parse($dateStarted);    
        }

        $user = Auth::user();

        $institution_id = 0;

        if(!empty($user->institution)) {
            $institution_id = $user->institution->id;
        }

        $lessons = collect([]);

        if($user->hasGroup('teacher')) {
            $lessons = Lesson::whereHas('institution', function (\Illuminate\Database\Eloquent\Builder $query) use ($institution_id) {
                $query->where('id', $institution_id);
            })->where('user_id', $user->id)->whereDate('start', $date->toDateString())->orderBy('start', 'asc')->get();
        } elseif($user->hasGroup('student')) {
            $class = $user->classes()->first();
            $class_id = !empty($class) ? $class->id : 0;
            $lessons = Lesson::whereHas('institution', function (\Illuminate\Database\Eloquent\Builder $query) use ($institution_id) {
                $query->where('id', $institution_id);
            })->where('study_class_id', $class_id)->whereDate('start', $date->toDateString())->orderBy('start', 'asc')->get();    
        }

        $counter = 1;
        foreach($lessons as $lesson) {
            $lesson->number = $counter;
            $lesson->time = Carbon::parse($lesson->start)->format('H:i');
            $counter++;
        }

        $output = ['error' => false];

        if($user->hasGroup('student')) {
            $output['html'] = view('dashboard.schedule.lessons', compact('lessons'))->render();
        } elseif($user->hasGroup('teacher')) {
            $output['html'] = view('dashboard.schedule.teacher_lessons', compact('lessons'))->render();
        }
        

        return $output;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
