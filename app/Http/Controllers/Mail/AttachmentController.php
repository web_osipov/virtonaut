<?php

namespace App\Http\Controllers\Mail;

use Illuminate\Http\Request;
use App\Mail;
use App\MailDomain;
use App\MailFolder;
use App\MailMessage;
use App\MailAttachment;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class AttachmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($id, Request $request)
    {
        $attachment = MailAttachment::findOrFail($id);

        $message = $attachment->message;

        $mail = $message->mail;

        $this->authorize('view', $mail);

        $path = storage_path('app/'.$attachment->file);

        if($request->view && $request->view == 'y' && $attachment->type = 'image') {

            $file = \File::get($path);
            $type = \File::mimeType($path);
            return \Response::stream(function() use ($file) {
                echo $file;
            }, 200, ["Content-Type"=> $type]);
        }

        return response()->download($path, $attachment->name);
    }

    public function store(Request $request)
    {
    	$user = Auth::user();

        $path = $request->file('file')->store('public/attachment/tmp/'.$user->id);

        $attachment = new MailAttachment;

        $attachment->mail_message_id = 0;

        $attachment->user_id = $user->id;

        $attachment->name = $request->file->getClientOriginalName();

        $attachment->size = $request->file->getSize();;

        $attachment->file = $path;
        
		$contentType = mime_content_type(storage_path('app/'.$path));

        $attachment->type = in_array($contentType, ['image/jpeg', 'image/png']) ? 'image' : 'application';

        $attachment->save();

        return $attachment->id;
    }
}
