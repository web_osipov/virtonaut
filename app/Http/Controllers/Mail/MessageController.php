<?php

namespace App\Http\Controllers\Mail;

use Illuminate\Http\Request;
use App\Mail;
use App\MailDomain;
use App\MailFolder;
use App\MailMessage;
use App\MailAttachment;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class MessageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($folder, Request $request)
    {
        $this->authorize('use', Mail::class);

        $folder = MailFolder::where('slug', $folder)->firstOrFail();

        $mail = Mail::where('user_id', Auth::user()->id)->firstOrFail();

        if($request->unread == 1) {
        	$messages = $mail->messages()->where('read', 0)->where('mail_folder_id', $folder->id)->latest()->paginate(20);
        } else {
        	$messages = $mail->messages()->where('mail_folder_id', $folder->id)->latest()->paginate(20);
        } 

        if(!empty($request->page) && $request->page > $messages->lastPage()) {
            return redirect(route('mail.index', $folder->slug).'?page='.$messages->lastPage());
        }

        $folders = \App\Helpers\MailHelper::getFolders($mail);

    	return view('dashboard.mail.index', compact('folder', 'folders', 'messages'));
    }

    public function delete($folder, Request $request)
    {
    	$mail = Mail::where('user_id', Auth::user()->id)->firstOrFail();

    	$trash = MailFolder::where('slug', 'trash')->first();

    	foreach((array) $request->messages as $message_id) {
    		$message = MailMessage::where('mail_id', $mail->id)->where('id', $message_id)->first();
    		if($message) {
    			if(in_array($message->folder->slug, ['draft', 'trash'])) {
    				$message->delete();
    			} else {
    				$message->mail_folder_id = $trash->id;
    				$message->save();
    			}
    		}
    	}

    	return ['error' => false, 'url' => route('mail.index', $folder)];
    }

    public function action($folder, Request $request)
    {
    	$action = $request->action;

    	$mail = Mail::where('user_id', Auth::user()->id)->firstOrFail();

    	$spam = MailFolder::where('slug', 'spam')->first();

    	foreach((array) $request->messages as $message_id) {
    		$message = MailMessage::where('mail_id', $mail->id)->where('id', $message_id)->first();
    		if($message) {
    			if($action == 'spam') {
    				if(in_array($message->folder->slug, ['inbox'])) {
    					$message->mail_folder_id = $spam->id;
    					$message->save();
    				} 
    			}
    		}
    	}

    	return ['error' => false, 'url' => route('mail.index', $folder)];
    }

    public function message($id, Request $request)
    {
        $message = MailMessage::findOrFail($id);

        if($message->folder->slug == 'draft') return abort('403');

        $mail = $message->mail;

        $this->authorize('view', $mail);

        $message->read = 1;

        $message->save();

        $folder = $message->folder;

        $folders = \App\Helpers\MailHelper::getFolders($mail);

        return view('dashboard.mail.message', compact('folder', 'folders', 'message'));
    }

    public function deleteMessage($id, Request $request)
    {
    	$message = MailMessage::findOrFail($id);

    	$mail = $message->mail;

        $this->authorize('view', $mail);

    	$folder = $message->folder;

    	$url = route('mail.message', $id);

    	$trash = MailFolder::where('slug', 'trash')->first();

    	if(in_array($folder->slug, ['draft', 'trash'])) {
    		$message->delete();
    		$url = route('mail.index', $folder->slug);
    	} else {
    		$message->mail_folder_id = $trash->id;
    		$message->save();
    		$url = route('mail.index', $trash->slug);
    	}

    	return ['error' => false, 'url' => $url];
    }

    public function actionMessage($id, Request $request)
    {
    	$message = MailMessage::findOrFail($id);

    	$mail = $message->mail;

        $this->authorize('view', $mail);

    	$folder = $message->folder;

    	$url = route('mail.message', $id);

    	$spam = MailFolder::where('slug', 'spam')->first();

    	if($action == 'spam') {
    		if(in_array($folder->slug, ['inbox'])) {
    			$message->mail_folder_id = $spam->id;
    			$message->save();
    		} 
    	}

    	return ['error' => false, 'url' => $url];
    }

    public function compose($id = null, Request $request)
    {
        $this->authorize('use', Mail::class);

        $message = false;

        $is_draft = false;

        if($id) {
        	$message = MailMessage::findOrFail($id);

        	$mail = $message->mail;

        	$message->attachments = $message->attachments()->select('id', 'name', 'size', 'type')->get();

        	foreach($message->attachments as $attachment) {
        		$attachment->image = $attachment->type == 'image' ? route('mail.attachment', $attachment->id).'?view=y' : false;
        	}

        	$this->authorize('view', $mail);

        	if($message->folder->slug == 'draft') $is_draft = true;
        }

        $folder = MailFolder::where('slug', $is_draft ? 'draft' : 'inbox')->firstOrFail();

        $mail = Mail::where('user_id', Auth::user()->id)->firstOrFail();

        $folders = \App\Helpers\MailHelper::getFolders($mail);

        return view('dashboard.mail.compose', compact('folder', 'folders', 'message', 'is_draft'));
    }

    public function send(Request $request)
    {
    	$this->authorize('use', Mail::class);

    	$mail = Mail::with('domain')->where('user_id', Auth::user()->id)->firstOrFail();

    	$folders = MailFolder::all()->keyBy('slug')->all();

    	$message = new MailMessage;

    	$message->mail_folder_id = $folders['sent']->id;

    	$message->mail_id = $mail->id;

        $message->from = \App\Helpers\MailHelper::getEmailArray();

        $message->to = $request->to ? \App\Helpers\MailHelper::getEmailArray($request->to) : collect([]);

        if($message->to->isEmpty()) return ['error' => __('Вы не указали получателя!')];

        $message->cc = $request->cc ? \App\Helpers\MailHelper::getEmailArray($request->cc) : collect([]);

        $message->bcc = $request->bcc ? \App\Helpers\MailHelper::getEmailArray($request->bcc) : collect([]);

        $message->reply_to = \App\Helpers\MailHelper::getEmailArray();

        $message->subject = $request->subject ?: __('(без темы)');

       	$message->message = $request->message ?: '';

       	$message->read = 1;

       	$message->send = 1;

       	$message->save();

       	if(!empty($request->attachments)) {
       		$countAttachments = 1;
       		foreach((array) $request->attachments as $attachment_id) {
       			$attachmentTmp = MailAttachment::where('id', $attachment_id)->where('user_id', Auth::user()->id)->first();
                $attachment_tmp_path = storage_path('app/'.$attachmentTmp->file);
       			$info = pathinfo($attachment_tmp_path);
				$ext = $info['extension'];
       			$file_name = $countAttachments.'.'.$ext;
       			$attachment = new MailAttachment;
       			$attachment->name = $attachmentTmp->name;
       			$attachment->size = $attachmentTmp->size;
       			$attachment->type = $attachmentTmp->type;
       			$attachment->user_id = $attachmentTmp->user_id;
       			$attachment->file = 'attachment/'.$message->id.'/'.$file_name;
       			try {
                    \Storage::disk('local')->put('attachment/'.$message->id.'/'.$file_name, file_get_contents($attachment_tmp_path));
                } catch (\Throwable $e) {
                    continue;
                }
       			$message->attachments()->save($attachment);
       			$countAttachments++;
       		}
       	}

    	$draftId = $request->draft_id;

    	if(!empty($draftId)) {
    		$draftDb = MailMessage::where('id', $draftId)->where('mail_id', $mail->id)->where('mail_folder_id', $folders['draft']->id)->first();
    		$draftDb->delete();
    	}

    	return ['error' => false, 'id' => $message->id, 'url' => route('mail.index', 'sent')];
    }

    public function draft(Request $request)
    {
    	$this->authorize('use', Mail::class);

    	$mail = Mail::with('domain')->where('user_id', Auth::user()->id)->firstOrFail();

    	$folders = MailFolder::all()->keyBy('slug')->all();

    	$draftId = $request->draft_id;

    	$draft = new MailMessage;

    	if(!empty($draftId)) {
    		$draftDb = MailMessage::where('id', $draftId)->where('mail_id', $mail->id)->where('mail_folder_id', $folders['draft']->id)->first();
    		if($draftDb) $draft = $draftDb;
    	}

    	$draft->mail_folder_id = $folders['draft']->id;

    	$draft->mail_id = $mail->id;

        $draft->from = \App\Helpers\MailHelper::getEmailArray();

        $draft->to = $request->to ? \App\Helpers\MailHelper::getEmailArray($request->to) : collect([]);

        $draft->cc = $request->cc ? \App\Helpers\MailHelper::getEmailArray($request->cc) : collect([]);

        $draft->bcc = $request->bcc ? \App\Helpers\MailHelper::getEmailArray($request->bcc) : collect([]);

        $draft->reply_to = \App\Helpers\MailHelper::getEmailArray();

        $draft->subject = $request->subject ?: '';

       	$draft->message = $request->message ?: '';

       	$draft->read = 1;

       	$draft->save();

       	MailAttachment::where('mail_message_id', $draft->id)->update(['mail_message_id' => 0]);

       	if(!empty($request->attachments)) {
       		foreach((array) $request->attachments as $attachment) {
       			MailAttachment::where('id', $attachment)->where('user_id', Auth::user()->id)->where('mail_message_id', 0)->update(['mail_message_id' => $draft->id]);
       		}
       	}

       	return ['id' => $draft->id, 'url' => route('mail.compose', $draft->id, false)];
    }
}
