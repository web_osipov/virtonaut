<?php

namespace App\Http\Controllers\Mail;

use Illuminate\Http\Request;
use App\Mail;
use App\MailDomain;
use App\MailFolder;
use App\MailMessage;
use App\MailAttachment;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function settings()
    {
    	$mail_domains = MailDomain::all();

    	return view('dashboard.mail.settings', compact('mail_domains'));
    }

    public function addDomain(Request $request)
    {
        $this->authorize('config', Mail::class);

        $validator = Validator::make($request->all(), [
            'domain' => ['required', 'string', 'unique:App\MailDomain,domain'],
        ])->setAttributeNames(['domain' => __('Домен')]);

        if ($validator->fails()) {
            return redirect(route('settings.mail'))
                        ->withErrors($validator)
                        ->withInput();
        }

        $domain = new MailDomain;

        $domain->domain = $request->domain;

        $domain->save();

        return redirect()->route('settings.mail')->with('status', __('Домен добавлен!'));
    }

    public function delDomain($id, Request $request)
    {
        $this->authorize('config', Mail::class);

        $domain = MailDomain::findOrFail($id);

        $domain->delete();

        return true;
    }
}
