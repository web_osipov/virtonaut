<?php

namespace App\Http\Middleware;

use Request;
use Closure;

class CheckLicense
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $unavailable_blocks = \App\Helpers\LicenseHelper::getUnavailableBlocks();

        foreach($unavailable_blocks as $unavailable_block) {
            if(Request::routeIs($unavailable_block['route'])) {
                return redirect()->route('license.index');
            }
        }

        return $next($request);
    }
}
