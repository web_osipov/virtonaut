<?php

namespace App\Http\Middleware;

use Closure;
use App\LegalInfo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Database\Eloquent\Builder;

class CheckLegalInfos
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if(!empty($user) && $user->hasGroup('admin')) {
            return $next($request);
        }

        if(!empty($user) && $user->hasGroup('student')) {
            return $next($request);
        }

        if(
            !empty($user)
            && !Route::is('license*')
            && !Route::is('logout')
        ) {
            $user_groups = $user->groups()->pluck('id')->toArray();
            
            $legalinfos = LegalInfo::whereHas('user_groups', function (Builder $query) use ($user_groups) {
                $query->whereIn('id', $user_groups);
            })->whereDoesntHave('users', function (Builder $query) use ($user) {
                $query->where('id', $user->id);
            })->count();

            if($legalinfos > 0 && !$user->hasGroup('admin')) {
                return redirect()->route('license.index')->withDanger('Для использования сервиса, примете условия использования.');
            }
        }

        return $next($request);
    }
}
