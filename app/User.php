<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Junges\ACL\Traits\UsersTrait;

class User extends Authenticatable
{
    use Notifiable, UsersTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'icon',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'last_activity' => 'datetime'
    ];

    protected $appends = ['icon_url', 'correct_email'];

    protected $with = ['institution'];

    public function getIconUrlAttribute($value = null)
    {
        if(empty($this->icon)) {
            return '/images/no-icon.svg';
        }

        return '/storage/' . $this->icon;
    }

    public function getCorrectEmailAttribute($value = null)
    {
        $mail = $this->mails()->first();

        if(!empty($mail) && $mail->active) {
            return $mail->username.'@'.$mail->domain->domain;
        }

        return $this->email;
    }

    public function getIsOnlineAttribute()
    {
        $session = $this->sessions()->orderBy('last_activity')->first();

        if($session) {
            return $session->last_activity > Carbon::now()->subMinutes(5);
        } else {
            return false;
        }
    }

    public function getValidSessionsAttribute()
    {
        $sessions = $this->sessions()->where('last_activity', '>', Carbon::now()->subMinutes(config('session.lifetime')))->count();

        return $sessions;
    }

    public function enabledEmail()
    {
        $mails = $this->mails()->where('active', 1)->count();

        return $mails > 0;
    }

    public function availableRecordings()
    {
        $class = $this->classes()->first();

        if(empty($class)) return 0;

        $recordings = Lesson::with('recording')->whereHas('recording', function ($query) {
            $query->where('status', 'finished');
        })->where('study_class_id', $class->id)->orderBy('start', 'desc')->count();

        return $recordings;
    }

    public function institution()
    {
        return $this->belongsTo('App\Institution');
    }

    public function mails()
    {
        return $this->hasMany('App\Mail');
    }

    public function classes()
    {
        return $this->belongsToMany('App\StudyClass');
    }

    public function hasClass($class)
    {
        $where = null;

        if (is_numeric($class)) {
            $where = ['id', $class];
        } elseif (is_string($class)) {
            $where = ['name', $class];
        }

        if ($class != null && $where != null) {
            return null !== $this->classes->where(...$where)->first();
        }

        return false;
    }

    public function achivements()
    {
        return $this->belongsToMany('App\Achivement')->withPivot('received');
    }

    public function hasAchivement($achivement)
    {
        $where = null;

        if (is_numeric($achivement)) {
            $where = ['id', $achivement];
        } elseif (is_string($achivement)) {
            $where = ['name', $achivement];
        }

        if ($achivement != null && $where != null) {
            return null !== $this->achivements->where(...$where)->first();
        }

        return false;
    }

    public function subjects()
    {
        return $this->belongsToMany('App\Subject');
    }

    public function hasSubject($subject)
    {
        $where = null;

        if (is_numeric($subject)) {
            $where = ['id', $subject];
        } elseif (is_string($subject)) {
            $where = ['name', $subject];
        }

        if ($subject != null && $where != null) {
            return null !== $this->subjects->where(...$where)->first();
        }

        return false;
    }

    public function datas()
    {
        return $this->hasMany('App\UserData');
    }

    public function hasFavorited($id) 
    {
        return !empty($this->favorites()->where('id', $id)->first());
    }

    public function favorites()
    {
        return $this->belongsToMany('App\User', 'favorites', 'user_id', 'favorited_id');
    }

    public function sessions()
    {
        return $this->hasMany('App\Session');
    }

    public function getData($type, $onlyValue = false)
    {
        $data = $this->datas()->where('type', $type)->first();

        if($data) {
            $data->value = unserialize($data->value);
            if($onlyValue) {
                return $data->value;
            }
        }

        return $data;
    }

    public function setData($data)
    {
        if(!is_array($data)) return false;

        foreach($data as $type => $value) {
            $data = UserData::updateOrCreate([
                'user_id' => $this->id,
                'type' => $type
            ], [
                'value' => $value ? serialize($value) : serialize(null)
            ]);
        }

        return true;
    }
}
