<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailDomain extends Model
{
	protected $guarded = [];
	
    public function mails()
    {
        return $this->hasMany('App\Mail');
    }

    public static function boot() {
        parent::boot();
        self::deleting(function($domain) {
            $domain->mails()->each(function($mail) {
                $mail->delete(); 
            });
        });
    }
}
