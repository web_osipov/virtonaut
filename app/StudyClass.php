<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudyClass extends Model
{
    protected $guarded = [];

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function institution()
    {
        return $this->belongsToMany('App\Institution');
    }

    public function getInstitution()
    {
        $institution = $this->institution()->first();

        if(!empty($institution)) return $institution;

        return false;
    }

    public function getInstitutionId()
    {
        $institution = $this->institution()->first();

        if(!empty($institution)) return $institution->id;

        return 0;
    }
}
