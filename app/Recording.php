<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recording extends Model
{
    protected $guarded = [];

    public function classroom()
    {
        return $this->belongsTo('App\Classroom');
    }

    public function lesson()
    {
        return $this->belongsTo('App\Lesson');
    }
}
