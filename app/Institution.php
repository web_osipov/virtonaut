<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Institution extends Model
{
    protected $guarded = [];

    protected $appends = ['icon_url'];

    public function getIconUrlAttribute($value = null)
    {
        if(empty($this->icon)) {
            return '/images/no-icon.svg';
        }

        return '/storage/' . $this->icon;
    }

    public function classes()
    {
        return $this->belongsToMany('App\StudyClass');
    }

    public function classrooms()
    {
        return $this->belongsToMany('App\Classroom');
    }

    public function members()
    {
        return $this->hasMany('App\User');
    }

    public function teachers()
    {
        return $this->members()->group('teacher')->get();
    }

    public function students()
    {
        return $this->members()->group('student')->get();
    }
}
