<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    protected $guarded = [];

    protected $casts = [
        'data' => 'array',
        'start' => 'datetime',
        'end' => 'datetime'
    ];
}
