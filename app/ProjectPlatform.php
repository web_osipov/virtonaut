<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectPlatform extends Model
{
    protected $guarded = [];

    protected $appends = ['icon_url'];

    public function getIconUrlAttribute($value = null)
    {
        if(empty($this->icon)) {
            return '/images/no-icon.svg';
        }

        return '/storage/' . $this->icon;
    }
}
