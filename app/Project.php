<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $guarded = [];

    protected $casts = [
        'images' => 'array',
    ];

    public function achivement()
    {
        return $this->belongsTo('App\Achivement');
    }

    public function tags()
    {
        return $this->belongsToMany('App\ProjectTag');
    }

    public function platforms()
    {
        return $this->belongsToMany('App\ProjectPlatform');
    }

    public function participants()
    {
        return $this->belongsToMany('App\User');
    }
}
