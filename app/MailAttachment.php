<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailAttachment extends Model
{
    protected $guarded = [];

    public function message()
    {
        return $this->belongsTo('App\MailMessage', 'mail_message_id');
    }

}
