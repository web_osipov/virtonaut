<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VirtualDomain extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    protected $connection = 'mysql_mail';

    public function users()
    {
        return $this->hasMany('App\Models\VirtualUser', 'domain_id');
    }

    public function aliases()
    {
        return $this->hasMany('App\Models\VirtualAliase', 'domain_id');
    }

    public static function boot() {
        parent::boot();
        self::deleting(function($domain) {
            $domain->users()->each(function($user) {
                $user->delete(); 
            });
            $domain->aliases()->each(function($aliase) {
                $aliase->delete(); 
            });
        });
    }
}
