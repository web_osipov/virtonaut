<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VirtualUser extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    protected $connection = 'mysql_mail';

    public function domain()
    {
        return $this->belongsTo('App\Models\VirtualDoman', 'domain_id');
    }
}
