<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VirtualAliase extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    protected $connection = 'mysql_mail';

    public function domain()
    {
        return $this->belongsTo('App\VirtualDoman', 'domain_id');
    }
}
