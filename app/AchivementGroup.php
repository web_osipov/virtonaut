<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AchivementGroup extends Model
{
    protected $guarded = [];

    public function user_groups()
    {
        return $this->belongsToMany(config('acl.models.group'), 'achivement_group_group');
    }

    public function achivements()
    {
        return $this->belongsToMany('App\Achivement', 'achivement_achivement_group', 'group_id', 'achivement_id');
    }
}
