<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailMessage extends Model
{
    protected $guarded = [];

    protected $casts = [
        'from' => 'collection',
        'to' => 'collection',
        'reply_to' => 'collection',
        'cc' => 'collection',
        'bcc' => 'collection'
    ];

    public function attachments()
    {
        return $this->hasMany('App\MailAttachment');
    }

    public function mail()
    {
        return $this->belongsTo('App\Mail', 'mail_id');
    }

    public function folder()
    {
        return $this->belongsTo('App\MailFolder', 'mail_folder_id');
    }

    public static function boot() {
        parent::boot();
        self::deleting(function($message) {
            $message->attachments()->each(function($attachment) {
                $attachment->mail_message_id = 0; 
                $attachment->save();
            });
        });
    }
}
