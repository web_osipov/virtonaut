<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    protected $guarded = [];

    public function getStartCarbonAttribute()
    {
        return Carbon::parse($this->start);
    }

    public function class()
    {
        return $this->belongsTo('App\StudyClass', 'study_class_id');
    }

    public function teacher()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function subject()
    {
        return $this->belongsTo('App\Subject');
    }

    public function recording()
    {
        return $this->hasOne('App\Recording');
    }

    public function institution()
    {
        return $this->belongsToMany('App\Institution');
    }

    public function classroom()
    {
        return $this->belongsToMany('App\Classroom');
    }

    public function getInstitution()
    {
        $institution = $this->institution()->first();

        if(!empty($institution)) return $institution;

        return false;
    }

    public function getInstitutionId()
    {
        $institution = $this->institution()->first();

        if(!empty($institution)) return $institution->id;

        return 0;
    }
}
