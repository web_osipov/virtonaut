<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mail extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function domain()
    {
        return $this->belongsTo('App\MailDomain', 'mail_domain_id');
    }

    public function messages()
    {
        return $this->hasMany('App\MailMessage');
    }

    public static function boot() {
        parent::boot();
        self::deleting(function($mail) {
            $mail->messages()->each(function($message) {
                $message->delete(); 
            });
        });
    }
}
