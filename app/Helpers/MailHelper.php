<?php

namespace App\Helpers;

use Gate;
use App\Mail;
use App\MailFolder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class MailHelper
{
	public static function getFolders($mail)
    {
    	$folders = collect(MailFolder::all()->keyBy('slug')->all());

        foreach($folders as $folder) {
            $folder_id = $folder->id;
            $folder->total_messages = $mail->messages()->where('mail_folder_id', $folder_id)->count();
            $folder->unread_messages = $mail->messages()->where('mail_folder_id', $folder_id)->where('read', 0)->count();
        }

        return $folders;
    }

    public static function getSenderLetter($mail)
    {
    	$firstChar = mb_substr($mail, 0, 1, "UTF-8");

    	return strtoupper($firstChar);
    }

    public static function getTruncateContent($message)
    {
    	$message = str_replace(['<div>', '</div>', '<p>', '</p>'], ' ', $message);
    	return Str::limit(strip_tags($message), 100);
    }

    public static function getMessagePlural($n)
    {
    	$n  = abs($n) % 100;
	    $n1 = $n % 10;
	    if ($n > 10 && $n < 20)      return __('сообщений');
	    else if ($n1 > 1 && $n1 < 5) return __('сообщения');
	    else if ($n1 == 1)           return __('сообщение');
	 
	    return __('сообщений');
    }

    public static function getEmailArray($email = false)
    {  
        $output = [];

        if($email) {
            if(is_array($email)) {
                foreach($email as $aEmail) {
                    $email_parts = explode('@', $aEmail);
                    $output[] = [
                        'mail' => $aEmail,
                        'mailbox' => $email_parts[0],
                        'host' => !empty($email_parts[1]) ?: $email_parts[0],
                        'personal' => '',
                        'full' => $aEmail
                    ];     
                }
            } else {
                $email_parts = explode('@', $email);
                $output[] = [
                    'mail' => $email,
                    'mailbox' => $email_parts[0],
                    'host' => !empty($email_parts[1]) ?: $email_parts[0],
                    'personal' => '',
                    'full' => $email
                ];   
            }
        } else {
            $mail = Mail::with('domain')->where('user_id', Auth::user()->id)->firstOrFail();
            $output['mail'] = $mail->username.'@'.$mail->domain->domain;
            $output['mailbox'] = $mail->username;
            $output['host'] = $mail->domain->domain;
            $output['personal'] = Auth::user()->name;
            $output['full'] = $output['personal'].' <'. $output['mail'].'>';
            $output = [$output];
        }

        return collect($output);
    }
}