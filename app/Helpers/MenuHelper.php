<?php

namespace App\Helpers;

use Gate;
use App\Mail;
use Illuminate\Support\Facades\Auth;

class MenuHelper
{
	public static function generateMenuArray()
    {
    	$user = Auth::user();

    	$menu = [];

    	$menu['infoblock'] = [
    		'icon' => 'fa fa-th-large',
			'title' => __('Инфоблок'),
			'url' => route('home'),
			'route' => 'home',
    	];

    	if(Gate::allows('create', \App\User::class)) {
            $menu['users'] = [
				'icon' => 'fa fa-user',
				'title' => __('Пользователи'),
				'url' => 'javascript:;',
				'route' => 'user*',
				'caret' => true,
				'sub_menu' => [
					[
						'url' => route('user.index'),
						'title' => __('Список пользователей'),
						'route' => 'user.index',
					],
					[
						'url' => route('user.create'),
						'title' => __('Добавить пользователя'),
						'route' => 'user.create',
					]
				],
			];
        }

        $menu['members'] = [
			'icon' => 'fa fa-users',
			'title' => __('Участники'),
			'url' => route('members'),
			'route' => 'members',
		];

        if(Gate::allows('viewAny', \App\Project::class)) {
        	$menu['projects'] = [
				'icon' => 'fa fa-table',
				'title' => __('Проекты'),
				'url' => route('project.index'),
				'route' => 'project.index',
			];
        }

        if(Gate::allows('config', \App\Project::class)) {
        	$menu_element = [
				'icon' => 'fa fa-table',
				'title' => __('Проекты'),
				'url' => 'javascript:;',
				'route' => 'project*',
				'caret' => true,
				'sub_menu' => [
					[
						'url' => route('project.index'),
						'title' => __('Список'),
						'route' => 'project.index',
					],
					[
						'url' => route('project.create'),
						'title' => __('Добавить'),
						'route' => 'project.create',
					]
				]
			];

			$menu_element['sub_menu'][] = [
				'url' => 'javascript:;',
				'title' => __('Платформы'),
				'route' => 'project.platform*',
				'caret' => true,
				'sub_menu' => [
					[
						'url' => route('project.platform.index'),
						'title' => __('Список'),
						'route' => 'project.platform.index',
					],
					[
						'url' => route('project.platform.create'),
						'title' => __('Добавить'),
						'route' => 'project.platform.create',
					]
				]
			];

			$menu_element['sub_menu'][] = [
				'url' => 'javascript:;',
				'title' => __('Теги'),
				'route' => 'project.tag*',
				'caret' => true,
				'sub_menu' => [
					[
						'url' => route('project.tag.index'),
						'title' => __('Список'),
						'route' => 'project.tag.index',
					],
					[
						'url' => route('project.tag.create'),
						'title' => __('Добавить'),
						'route' => 'project.tag.create',
					]
				]
			];

			$menu['projects'] = $menu_element;
        }

        if(Gate::allows('viewAny', \App\License::class)) {
        	$menu['licenses'] = [
				'icon' => 'fa fa-key',
				'title' => __('Лицензия'),
				'url' => route('license.index'),
				'route' => 'license.index',
			];
        }

        if(Gate::allows('configure', \App\User::class)) {
        	$menu_element = [
				'icon' => 'fa fa-key',
				'title' => __('Лицензия'),
				'url' => 'javascript:;',
				'route' => 'license*',
				'caret' => true,
				'sub_menu' => [
					[
						'url' => route('license.index'),
						'title' => __('Список'),
						'route' => 'license.index',
					],
					[
						'url' => route('license.create'),
						'title' => __('Добавить'),
						'route' => 'license.create',
					]
				]
			];

			$menu_element['sub_menu'][] = [
				'url' => 'javascript:;',
				'title' => __('Типы'),
				'route' => 'license.type*',
				'caret' => true,
				'sub_menu' => [
					[
						'url' => route('license.type.index'),
						'title' => __('Список'),
						'route' => 'license.type.index',
					],
					[
						'url' => route('license.type.create'),
						'title' => __('Добавить'),
						'route' => 'license.type.create',
					]
				]
			];

			$menu_element['sub_menu'][] = [
				'url' => 'javascript:;',
				'title' => __('Правовая информация'),
				'route' => 'license.legalinfo*',
				'caret' => true,
				'sub_menu' => [
					[
						'url' => route('license.legalinfo.index'),
						'title' => __('Список'),
						'route' => 'license.legalinfo.index',
					],
					[
						'url' => route('license.legalinfo.create'),
						'title' => __('Добавить'),
						'route' => 'license.legalinfo.create',
					]
				]
			];

			$menu['licenses'] = $menu_element;
		}
		

		if(Gate::allows('viewAny', \App\License::class)) {
        	$menu['updates'] = [
				'icon' => 'fa fa-cogs',
				'title' => __('Обновления'),
				'url' => route('update.index'),
				'route' => 'update.index',
			];
        }

        if(Gate::allows('configure', \App\User::class)) {
        	$menu_element = [
				'icon' => 'fa fa-cogs',
				'title' => __('Обновления'),
				'url' => 'javascript:;',
				'route' => 'update*',
				'caret' => true,
				'sub_menu' => [
					[
						'url' => route('update.index'),
						'title' => __('Список'),
						'route' => 'update.index',
					],
					[
						'url' => route('update.create'),
						'title' => __('Добавить'),
						'route' => 'update.create',
					]
				]
			];

			$menu['updates'] = $menu_element;
        }

        $menu['achivements'] = [
			'icon' => 'fa fa-star',
			'title' => __('Достижения'),
			'url' => route('achivement.index'),
			'route' => 'achivement.index'
		];

        if(Gate::allows('use', \App\Mail::class)) {
        	$menu_element = [
				'icon' => 'fa fa-envelope',
				'title' => __('Почта'),
				'url' => 'javascript:;',
				'route' => 'mail*',
				'caret' => true,
				'sub_menu' => [],
			];

        	if(Gate::allows('use', \App\Mail::class)) {

        		$mail = Mail::where('user_id', Auth::user()->id)->firstOrFail();
        		$folders = \App\Helpers\MailHelper::getFolders($mail);
        		$unread_messags = $folders['inbox']->unread_messages;
        		$menu_element['badge'] = $unread_messags;

        		$menu_element['sub_menu'][] = [
					'url' => route('mail.index', 'inbox'),
					'title' => __('Письма'),
					'route' => 'mail.index',
				];
				$menu_element['sub_menu'][] = [
					'url' => route('mail.compose'),
					'title' => __('Написать'),
					'route' => 'mail.compose',
				];
        	}

        	$menu['email'] = $menu_element;
        }

        $menu['calendar'] = [
			'icon' => 'fa fa-calendar',
			'title' => __('Календарь'),
			'url' => route('calendar'),
			'route' => 'calendar'
		];

		if(!$user->hasGroup('admin')) {
			$menu['remotelearning'] = [
				'icon' => 'fa fa-globe',
				'title' => __('Удаленное обучение'),
				'url' => route('remotelearning.index'),
				'route' => 'remotelearning.index'
			];	
		}

        if(Gate::allows('viewAny', \App\Lesson::class)) {
        	$menu_element = [
				'icon' => 'fa fa-list-ol',
				'title' => __('Школьное расписание'),
				'url' => 'javascript:;',
				'route' => 'schedule*',
				'caret' => true,
				'sub_menu' => [
					[
	        			'title' => __('Школьное расписание'),
						'url' => route('schedule.index'),
						'route' => 'schedule.index'
	        		]
				],
			];

			if($user->hasGroup('student') || $user->hasGroup('teacher')) {
				$menu_element['sub_menu'][] = [
	        		'title' => __('Ваше расписание'),
					'url' => route('schedule.myschedule'),
					'route' => 'schedule.myschedule'
	        	];
			}

        	if(Gate::allows('create', \App\Lesson::class)) {
	        	$menu_element['sub_menu'][] = [
	        		'title' => __('Уроки'),
					'url' => 'javascript:;',
					'route' => 'schedule.lesson*',
					'caret' => true,
					'sub_menu' => [
						[
							'title' => __('Список'),
							'url' => route('schedule.lesson.index'),
							'route' => 'schedule.lesson.index'
						],
						[
							'title' => __('Добавить'),
							'url' => route('schedule.lesson.create'),
							'route' => 'schedule.lesson.create'
						]
					]
	        	];
        	}

        	$menu['schedule'] = $menu_element;
        }

        if(Gate::allows('configure', \App\User::class)) {
            $menu_element = [
				'icon' => 'fa fa-cogs',
				'title' => __('Настройки'),
				'url' => 'javascript:;',
				'route' => 'settings*',
				'caret' => true,
				'sub_menu' => [],
			];

			$menu_element['sub_menu'][] = [
				'url' => 'javascript:;',
				'title' => __('Учебные классы'),
				'route' => 'settings.classroom*',
				'caret' => true,
				'sub_menu' => [
					[
						'url' => route('settings.classroom.index'),
						'title' => __('Список'),
						'route' => 'settings.classroom.index',
					],
					[
						'url' => route('settings.classroom.create'),
						'title' => __('Добавить'),
						'route' => 'settings.classroom.create',
					]
				]
			];

			$menu_element['sub_menu'][] = [
				'url' => 'javascript:;',
				'title' => __('Классы'),
				'route' => 'settings.class.*',
				'caret' => true,
				'sub_menu' => [
					[
						'url' => route('settings.class.index'),
						'title' => __('Список'),
						'route' => 'settings.class.index',
					],
					[
						'url' => route('settings.class.create'),
						'title' => __('Добавить'),
						'route' => 'settings.class.create',
					]
				]
			];

			$menu_element['sub_menu'][] = [
				'url' => 'javascript:;',
				'title' => __('Предметы'),
				'route' => 'settings.subject*',
				'caret' => true,
				'sub_menu' => [
					[
						'url' => route('settings.subject.index'),
						'title' => __('Список'),
						'route' => 'settings.subject.index',
					],
					[
						'url' => route('settings.subject.create'),
						'title' => __('Добавить'),
						'route' => 'settings.subject.create',
					]
				]
			];

			$menu_element['sub_menu'][] = [
				'url' => 'javascript:;',
				'title' => __('Достижения'),
				'route' => 'settings.achivement*',
				'caret' => true,
				'sub_menu' => [
					[
						'url' => route('settings.achivement.index'),
						'title' => __('Список'),
						'route' => 'settings.achivement.index',
					],
					[
						'url' => route('settings.achivement.create'),
						'title' => __('Добавить'),
						'route' => 'settings.achivement.create',
					],
					[
	        			'title' => __('Группы'),
						'url' => 'javascript:;',
						'route' => 'settings.achivement.group*',
						'caret' => true,
						'sub_menu' => [
							[
								'title' => __('Список'),
								'url' => route('settings.achivement.group.index'),
								'route' => 'settings.achivement.group.index'
							],
							[
								'title' => __('Добавить'),
								'url' => route('settings.achivement.group.create'),
								'route' => 'settings.achivement.group.create'
							]
						]
	        		],
				]
			];

			$menu_element['sub_menu'][] = [
				'url' => 'javascript:;',
				'title' => __('Учебные учреждения'),
				'route' => 'settings.institution*',
				'caret' => true,
				'sub_menu' => [
					[
						'url' => route('settings.institution.index'),
						'title' => __('Список'),
						'route' => 'settings.institution.index',
					],
					[
						'url' => route('settings.institution.create'),
						'title' => __('Добавить'),
						'route' => 'settings.institution.create',
					]
				]
			];

			$menu_element['sub_menu'][] = [
				'url' => route('settings.mail'),
				'title' => __('Почта'),
				'route' => 'settings.mail',
			];

			$menu_element['sub_menu'][] = [
				'url' => route('settings.design'),
				'title' => __('Настройки дизайна'),
				'route' => 'settings.design',
			];

			$menu['settings'] = $menu_element;
        }

        $unavailable_blocks = \App\Helpers\LicenseHelper::getUnavailableBlocks();

        foreach($unavailable_blocks as $block_key => $unavailable_block) {
        	if(isset($menu[$block_key])) {
        		unset($menu[$block_key]);
        	}
        }

        if($user->hasGroup('teacher')) {
        	$order = [
        		'infoblock', 
        		'schedule', 
        		'email', 
        		'calendar', 
        		'remotelearning', 
        		'members', 
        		'projects', 
        		'achivements', 
				'licenses',
				'updates'
        	];
        	foreach($order as $order_key => $order_value) {
        		if(!isset($menu[$order_value])) {
        			unset($order[$order_key]);
        		}
        	}
        	$ordered_menu = array_merge(array_flip($order), $menu);
    		return $ordered_menu;
    	} elseif($user->hasGroup('student')) {
    		$order = [
        		'infoblock', 
        		'schedule', 
        		'remotelearning', 
        		'calendar',
        		'members',
				'updates'
        	];
        	foreach($order as $order_key => $order_value) {
        		if(!isset($menu[$order_value])) {
        			unset($order[$order_key]);
        		}
        	}
        	$menu['members']['title'] = __('Преподаватели');
        	$ordered_menu = array_merge(array_flip($order), $menu);
        	$ordered_menu = array_values($ordered_menu);
    		return $ordered_menu;
    	} elseif($user->hasGroup('admin')) {
    		return $menu;
    	} else {
    		return [];
    	}

    	return $menu;
    }
}