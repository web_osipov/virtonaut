<?php

namespace App\Helpers;

use Gate;
use App\LicenseType;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class LicenseHelper
{
	public static function getBlocks() 
    {
        $blocks = [
            'members' => [
                'name' => 'Участники',
                'route' => 'members'
            ],
            'projects' => [
                'name' => 'Проекты',
                'route' => 'project*'
            ],
            'email' => [
                'name' => 'Почта',
                'route' => 'mail*'
            ],
            'achivements' => [
                'name' => 'Достижения',
                'route' => 'achivement.index'
            ],
            'remotelearning' => [
                'name' => 'Удаленное обучение',
                'route' => 'remotelearning.index'
            ],
            'calendar' => [
                'name' => 'Календарь',
                'route' => 'calendar'
            ],
            'schedule' => [
                'name' => 'Школьное расписание',
                'route' => 'schedule*'
            ],
        ];

        return $blocks;
    }

    public static function getUnavailableBlocks() 
    {
        $user = Auth::user();

        if(empty($user)) return [];

        if($user->hasGroup('admin')) return [];

        $blocks = self::getBlocks();

        $unavailableBlocks = [];

        $licenseTypes = LicenseType::all();

        foreach($licenseTypes as $licenseType) {
            foreach((array) $licenseType->blocks as $block) {
                if(isset($blocks[$block])) {
                    $unavailableBlocks[$block] = $blocks[$block];
                }
            }
        }

        $blocks = $unavailableBlocks;

        $institution = $user->institution;

        $classrooms = !empty($institution) ? $institution->classrooms : [];

        $licenses = [];

        $license_expires = false;

        foreach($classrooms as $classroom) {
            $classroom_licenses = $classroom->licenses;

            foreach($classroom_licenses as $classroom_license) {
                $expires = Carbon::parse($classroom_license->pivot->expires);
                if($expires > Carbon::now())
                    $licenses[] = [
                        'id' => $classroom_license->id,
                        'name' => $classroom_license->name,
                        'blocks' => (array) $classroom_license->type->first()->blocks,
                        'type' => $classroom_license->type->first()->name,
                        'description' => $classroom_license->description,
                        'image_url' => $classroom_license->type->first()->icon_url,
                        'expires' => 'До '.$expires->format('d.m.Y').' '.$expires->diffForHumans()
                    ];
            }
        }

        foreach($licenses as $license) {
            foreach($license['blocks'] as $block) {
                if(isset($blocks[$block])) {
                    unset($blocks[$block]);
                }
            }
        }

        return $blocks;
    }
}