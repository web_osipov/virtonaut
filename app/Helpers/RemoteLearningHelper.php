<?php

namespace App\Helpers;

use Gate;
use App\Lesson;
use App\Classroom;
use App\Recording;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class RemoteLearningHelper
{
	public static function createLesson($lesson_id) 
	{
		$storage = storage_path('app/public/lesson/'.$lesson_id);

		if(!file_exists($storage)) mkdir($storage, 0755, true);

		$lesson = Lesson::find($lesson_id);

		$classroom = $lesson->classroom()->first();

		if(empty($classroom) && $classroom->no_cam != 1) return false;

		$recording = new Recording;

		$recording->classroom_id = $classroom->id;

		$recording->lesson_id = $lesson_id;

		$recording->status = 'created';

		$class_cam_command = config('ffmpeg.path')." ".config('ffmpeg.transport')." -i '".$classroom->class_cam."' -acodec ".config('ffmpeg.stream.acodec')." -vcodec ".config('ffmpeg.stream.vcodec')." -f hls -hls_flags delete_segments '".$storage."/class.m3u8'" . ' > /dev/null 2>&1 & echo $!; ';
		$class_cam_pid = exec($class_cam_command, $output);

		$board_cam_command = config('ffmpeg.path')." ".config('ffmpeg.transport')." -i '".$classroom->board_cam."' -acodec ".config('ffmpeg.stream.acodec')." -vcodec ".config('ffmpeg.stream.vcodec')." -f hls -hls_flags delete_segments '".$storage."/board.m3u8'" . ' > /dev/null 2>&1 & echo $!; ';
		$board_cam_pid = exec($board_cam_command, $output);

		$recording->stream_class_process_id = $class_cam_pid;

		$recording->stream_board_process_id = $board_cam_pid;

		$recording->save();

		return $recording->id;
	}

	public static function startLesson($lesson_id) 
	{
		$storage = storage_path('app/public/lesson/'.$lesson_id);

		$lesson = Lesson::find($lesson_id);

		$recording = $lesson->recording;

		$classroom = $recording->classroom;

		$class_cam_command = config('ffmpeg.path')." ".config('ffmpeg.transport')." -i '".$classroom->class_cam."' -acodec ".config('ffmpeg.record.acodec')." -vcodec ".config('ffmpeg.record.vcodec')." -f mp4 '".$storage."/class.mp4'" . ' > /dev/null 2>&1 & echo $!; ';
		$class_cam_pid = exec($class_cam_command, $output);

		$board_cam_command = config('ffmpeg.path')." ".config('ffmpeg.transport')." -i '".$classroom->board_cam."' -acodec ".config('ffmpeg.record.acodec')." -vcodec ".config('ffmpeg.record.vcodec')." -f mp4 '".$storage."/board.mp4'" . ' > /dev/null 2>&1 & echo $!; ';
		$board_cam_pid = exec($board_cam_command, $output);

		$recording->record_class_process_id = $class_cam_pid;

		$recording->record_board_process_id = $board_cam_pid;

		$recording->status = 'started';

		$recording->save();
	}

	public static function endLesson($lesson_id) 
	{
		$storage = storage_path('app/public/lesson/'.$lesson_id);

		$lesson = Lesson::find($lesson_id);

		$recording = $lesson->recording;

		$classroom = $recording->classroom;

		if(!empty($recording->stream_class_process_id))
			exec('kill '.$recording->stream_class_process_id, $output);
		if(!empty($recording->stream_board_process_id))
			exec('kill '.$recording->stream_board_process_id, $output);
		if(!empty($recording->record_class_process_id))
			exec('kill '.$recording->record_class_process_id, $output);
		if(!empty($recording->record_board_process_id))
			exec('kill '.$recording->record_board_process_id, $output);

		exec(config('ffmpeg.path')." -i '".$storage."/class.mp4' -ss 00:00:01.000 -vframes 1 '".$storage."/class.png'", $output);

		exec(config('ffmpeg.path')." -i '".$storage."/board.mp4' -ss 00:00:01.000 -vframes 1 '".$storage."/board.png'", $output);

		$recording->status = 'finished';

		$recording->save();
	}
}