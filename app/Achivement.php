<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Achivement extends Model
{
    protected $guarded = [];

    protected $appends = ['icon_url', 'unactive_icon_url'];

    public function getIconUrlAttribute($value = null)
    {
        if(empty($this->icon)) {
            return '/images/no-icon.svg';
        }

        return '/storage/' . $this->icon;
    }

    public function getUnactiveIconUrlAttribute($value = null)
    {
        return '/images/no-icon.svg';
    }

    public function groups()
    {
        return $this->belongsToMany('App\AchivementGroup', 'achivement_achivement_group', 'achivement_id', 'group_id');
    }
}
