<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectTag extends Model
{
    protected $guarded = [];

    public function projects()
    {
        return $this->belongsToMany('App\Project');
    }
}
