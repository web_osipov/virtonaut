<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LegalInfo extends Model
{
    protected $guarded = [];

    public function user_groups()
    {
        return $this->belongsToMany(config('acl.models.group'), 'group_legal_info');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'legal_info_user');
    }
}
