<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class License extends Model
{
    protected $guarded = [];

    public function classroom()
    {
        return $this->belongsToMany('App\Classroom')->withPivot('expires');
    }

    public function type()
    {
        return $this->belongsToMany('App\LicenseType');
    }
}
