<?php

namespace App\Console\Commands;

use App\Recording;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Helpers\RemoteLearningHelper;

class CheckRecording extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'recording:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check recordings';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $recordings = Recording::where('status', '<>', 'finished')->get();

        $end_time = Carbon::now()->subMinutes(50);

        foreach($recordings as $recording) {
            $lesson_id = $recording->lesson_id;

            if($recording->updated_at < $end_time) {
                if($recording->status == 'created') {
                    if(!empty($recording->stream_class_process_id))
                        exec('kill '.$recording->stream_class_process_id, $output);
                    if(!empty($recording->stream_board_process_id))
                        exec('kill '.$recording->stream_board_process_id, $output);
                    if(!empty($recording->record_class_process_id))
                        exec('kill '.$recording->record_class_process_id, $output);
                    if(!empty($recording->record_board_process_id))
                        exec('kill '.$recording->record_board_process_id, $output);

                    $this->deleteDirectory(storage_path('app/public/lesson/'.$lesson_id));

                    $recording->delete();
                } else {
                    RemoteLearningHelper::endLesson($lesson_id); 
                }
            }
        }

        
    }

    public function deleteDirectory($dir) {
        system('rm -rf -- ' . escapeshellarg($dir), $retval);
        return $retval == 0; // UNIX commands return zero on success
    }
}
