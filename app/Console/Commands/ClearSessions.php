<?php

namespace App\Console\Commands;

use App\Session;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ClearSessions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'session:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear unused sessions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $time = Carbon::now()->subMinute();

        Session::where('last_activity', '<', $time->getTimestamp())->delete();
    }
}
