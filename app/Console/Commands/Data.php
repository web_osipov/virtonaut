<?php

namespace App\Console\Commands;

use DB;
use App\User;
use App\MailFolder;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class Data extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:insert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert initial data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('groups')->truncate();
        DB::table('group_has_permissions')->truncate();
        DB::table('permissions')->truncate();
        DB::table('users')->truncate();
        DB::table('user_has_groups')->truncate();
        DB::table('user_has_permissions')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        MailFolder::firstOrCreate(['slug' => 'inbox'], ['name' => 'Входящие']);
        MailFolder::firstOrCreate(['slug' => 'sent'], ['name' => 'Отправленные']);
        MailFolder::firstOrCreate(['slug' => 'trash'], ['name' => 'Удалённые']);
        MailFolder::firstOrCreate(['slug' => 'spam'], ['name' => 'Спам']);
        MailFolder::firstOrCreate(['slug' => 'draft'], ['name' => 'Черновики']);

       	$user = User::firstOrCreate([
            'id' => 1
        ], [
            'name' => __('Администатор'),
            'email' => 'admin@admin.ru',
            'password' => Hash::make('123456')
        ]);
        */

        $groups = [
            [
                'name' => 'Администраторы',
                'slug' => 'admin',
                'description' => '',
            ],
            [
                'name' => 'Преподаватели',
                'slug' => 'teacher',
                'description' => ''
            ],
            [
                'name' => 'Ученики',
                'slug' => 'student',
                'description' => ''
            ],
            [
                'name' => 'Директор',
                'slug' => 'principal',
                'description' => ''
            ],
            [
                'name' => 'Техподдержка',
                'slug' => 'support',
                'description' => ''
            ],
        ];

        foreach($groups as $group) {
            $groupModel = app(config('acl.models.group'));
            $groupDb = $groupModel->where('slug', $group['slug'])->first();
            if (is_null($groupDb)) {
                $groupDb = $groupModel->create([
                    'name' => $group['name'],
                    'slug' => $group['slug'],
                    'description' => $group['description'],
                ]);     
            }

            if(!empty($group['permissions'])) {
                foreach($group['permissions'] as $permission) {
                    $permissionModel = app(config('acl.models.permission'));
                    $permissionDb = $permissionModel->where('slug', $permission['slug'])->first();
                    if (is_null($permissionDb)) {
                        $permissionDb = $permissionModel->create([
                            'name' => $permission['name'],
                            'slug' => $permission['slug'],
                            'description' => $permission['description'],
                        ]);     
                    }
                    $groupDb->assignPermissions($permissionDb);
                }
            }
        }

        //$user->assignGroup('admin');
    }
}
