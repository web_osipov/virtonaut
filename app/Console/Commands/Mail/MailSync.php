<?php

namespace App\Console\Commands\Mail;

use App\Mail;
use App\MailDomain;
use App\Models\VirtualDomain;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class MailSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync mail accounts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $domains = MailDomain::select('domain')->get()->pluck('domain')->toArray();

        $mails = [];

        $oMail = Mail::with('domain')->get();

        foreach($oMail as $aMail) {
            $mails[$aMail->username.'@'.$aMail->domain->domain] = [
                'username' => $aMail->username,
                'password' => $aMail->password,
                'domain' => $aMail->domain->domain,
            ];
        }

        $old_domains = VirtualDomain::all();

		foreach($old_domains as $old_domain) {
			if(in_array($old_domain->name, $domains)) {
				$domain_users = $old_domain->users;
				foreach($domain_users as $domain_user) {
					if(!empty($mails[$domain_user->email])) {
						unset($mails[$domain_user->email]);
					} else {
						$domain_user->delete();
					}
				}
			} else {
				$old_domain->delete();
			}
		}

		foreach($domains as $domain) {
			$virtual_domain = VirtualDomain::firstOrCreate(['name' => $domain]);
			$mailsByDomain = optional(collect($mails)->groupBy('domain'))[$domain];
			if(empty($mailsByDomain)) continue;
			foreach($mailsByDomain as $mailByDomain) {

				$mail = $mailByDomain['username'].'@'.$domain;
				$pass = $mailByDomain['password'];

				DB::connection('mysql_mail')->insert("INSERT INTO virtual_users (domain_id, password, email) VALUES (".$virtual_domain->id.", sha1('".$pass."'), '".$mail."')");

			}
		}
    }
}
