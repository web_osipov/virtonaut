<?php

namespace App\Console\Commands\Mail;

use Illuminate\Console\Command;
use App\Mail;
use App\MailFolder;
use App\MailDomain;
use App\MailMessage;
use App\MailAttachment;
use Webklex\IMAP\Client;

class MailReceive extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:receive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Receive emails';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $mails = Mail::with('domain')->get();

        $folders = MailFolder::all()->keyBy('slug')->all();

        foreach($mails as $mail) {

            $oClient = new Client([
                'host' => config('mailclient.main_server'),
                'port' => 993,
                'encryption' => 'ssl',
                'validate_cert' => false,
                'username' => $mail->username.'@'.$mail->domain->domain,
                'password' => $mail->password,
            ]);

            try { 
                $oClient->connect(1); 
            } catch (\Throwable $e) {
                continue;
            }

            $oFolder = $oClient->getFolder('INBOX');

            $aMessage = $oFolder->query()->all()->get();

            foreach($aMessage as $oMessage) {

                $from = $to = $cc = $bcc = $reply_to = [];

                if(!empty($aFrom = $oMessage->getFrom())) {
                    foreach($aFrom as $oFrom) {
                        $from[] = $oFrom;
                    }
                }
                $from = collect($from);

                if(!empty($aTo = $oMessage->getTo())) {
                    foreach($aTo as $oTo) {
                        $to[] = $oTo;
                    }
                }
                $to = collect($to);

                if(!empty($aReplyTo = $oMessage->getReplyTo())) {
                    foreach($aReplyTo as $oReplyTo) {
                        $reply_to[] = $oReplyTo;
                    }
                }
                $reply_to = collect($reply_to);

                if(!empty($aCc = $oMessage->getCc())) {
                    foreach($aCc as $oCc) {
                        $cc[] = $oCc;
                    }
                }
                $cc = collect($cc);

                if(!empty($aBcc = $oMessage->getBcc())) {
                    foreach($aBcc as $oBcc) {
                        $bcc[] = $oBcc;
                    }
                }
                $bcc = collect($bcc);

                $subject = $oMessage->getSubject();

                $body = $oMessage->getHTMLBody(true) ?: '';

                if($from->isEmpty() || $to->isEmpty() || $reply_to->isEmpty() || empty($subject)) {
                    $oMessage->delete();
                    continue;
                }

                $message = new MailMessage;

                $message->subject = $subject;

                $message->message = $body;

                $message->from = $from;

                $message->to = $to;

                $message->reply_to = $reply_to;

                $message->cc = $cc;

                $message->bcc = $bcc;

                $message->mail_id = $mail->id;

                $message->mail_folder_id = $folders['inbox']->id;

                try { 
                    $message->save();
                } catch (\Throwable $e) {
                    $oMessage->delete();
                    continue;
                }

                if($oMessage->hasAttachments()) {

                    $aAttachment = $oMessage->getAttachments();

                    $countAttachments = 1;

                    $aAttachment->each(function ($oAttachment) use ($mail, $message, $countAttachments) {

                        $ext = $oAttachment->getExtension();

                        $name = $oAttachment->getName();

                        $type = $oAttachment->getType();

                        $size = $oAttachment->getSize();

                        if(empty($ext) || empty($name) || empty($type) || empty($type)) {
                            return true;
                        }

                        $file_name = $countAttachments.'.'.$ext;

                        $attachment = new MailAttachment;

                        $attachment->name = $name;

                        $attachment->type = $type;

                        $attachment->size = $size;

                        $attachment->user_id = $mail->user->id;

                        $attachment->file = 'attachment/'.$message->id.'/'.$file_name;

                        try {
                            \Storage::disk('local')->put('attachment/'.$message->id.'/'.$file_name, $oAttachment->getContent());
                        } catch (\Throwable $e) {
                            return true;
                        }

                        $message->attachments()->save($attachment);

                        $countAttachments++;

                    });

                }

                $mail->user->notify(new \App\Notifications\NewEmail([
                    'mail_message_id' => $message->id
                ]));

                $oMessage->delete();
            }
        }
    }
}
