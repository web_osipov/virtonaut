<?php

namespace App\Console\Commands\Mail;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\MailAttachment;

class AttachmentClear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:attachment-clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear unused attachments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $attachments = MailAttachment::where('mail_message_id', 0)->where('updated_at', '<=', Carbon::now()->subMinutes(1440)->toDateTimeString())->get();
        foreach($attachments as $attachment) {
            try {
                unlink(storage_path('app/'.$attachment->file));
            } catch (\Throwable $e) {}

            $attachment->delete();
        }

        $path = storage_path('app/attachment/');

        self::removeEmptySubFolders($path);
    }

    public static function removeEmptySubFolders($path)
    {
        $empty = true;
        foreach (glob($path.DIRECTORY_SEPARATOR."*") as $file) {
            if (is_dir($file)) {
               if (!self::removeEmptySubFolders($file)) $empty = false;
            } else {
               $empty = false;
            }
        }
        if ($empty) rmdir($path);
        return $empty;
    }
}
