<?php

namespace App\Console\Commands\Mail;

use Illuminate\Console\Command;
use App\Mail;
use App\MailFolder;
use App\MailDomain;
use App\MailMessage;
use App\MailAttachment;
use Illuminate\Support\Facades\Log;

class MailSend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send emails';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $messages = MailMessage::where('send', 1)->get();

        foreach($messages as $message) {
            $mail = $message->mail;
            $transport = (new \Swift_SmtpTransport(config('mailclient.main_server'), 587, 'tls'))
              ->setUsername($mail->username.'@'.$mail->domain->domain)
              ->setPassword($mail->password);

            $transport->setStreamOptions([
                'ssl' => ['allow_self_signed' => true, 'verify_peer' => false, 'verify_peer_name' => false]
            ]);

            try { 
                $mailer = new \Swift_Mailer($transport);
            } catch (\Throwable $e) {
                Log::error($e->getMessage());
                continue;
            }

            $to = $cc = $bcc = [];

            if($message->to->isNotEmpty())
                foreach($message->to as $email_to) {
                    if (filter_var($email_to['mail'], FILTER_VALIDATE_EMAIL))
                        $to[] = $email_to['mail'];
                }

            if($message->cc->isNotEmpty())
                foreach($message->cc as $email_cc) {
                    if (filter_var($email_cc['mail'], FILTER_VALIDATE_EMAIL))
                        $cc[] = $email_cc['mail'];
                }
            if($message->bcc->isNotEmpty())
                foreach($message->bcc as $email_bcc) {
                    if (filter_var($email_bcc['mail'], FILTER_VALIDATE_EMAIL))
                        $bcc[] = $email_bcc['mail'];
                }

            if(empty($to)) {
                $message->send = 0;
                $message->save();

                continue;
            }

            $swift = new \Swift_Message();
            $swift->setSubject($message->subject);
            $swift->setFrom([$mail->username.'@'.$mail->domain->domain => $mail->user->name]);
            $swift->setTo($to);
            if(!empty($cc)) $swift->setCc($cc);
            if(!empty($bcc)) $swift->setBcc($bcc);
            $swift->setBody($message->message, 'text/html');

            if($message->attachments->isNotEmpty())
                foreach($message->attachments as $attachment) {
                    $attachment_path = storage_path('app/'.$attachment->file);
                    $swift->attach(
                        \Swift_Attachment::fromPath($attachment_path)->setFilename($attachment->name)
                    );
                }
            try { 
                $result = $mailer->send($swift);
            } catch (\Throwable $e) {
                Log::error($e->getMessage());
                continue;
            }

            $message->send = 0;
            $message->save();
        }
    }
}
