<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\Data',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('mail:sync')->everyMinute()->withoutOverlapping();
        $schedule->command('mail:send')->everyMinute()->withoutOverlapping();
        $schedule->command('mail:receive')->everyMinute()->withoutOverlapping();
        $schedule->command('mail:attachment-clear')->everyMinute()->withoutOverlapping();
        $schedule->command('recording:check')->everyMinute()->withoutOverlapping();
        $schedule->command('session:clear')->everyMinute()->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
