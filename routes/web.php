<?php

use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return redirect('/dashboard');
});

Route::get('/dashboard', 'DashboardController@index')->name('home');

Route::resource('dashboard/user', 'UserController');


Route::get('/dashboard/email/a/{id}', 'Mail\AttachmentController@show')->name('mail.attachment');
Route::post('/dashboard/email/a', 'Mail\AttachmentController@store')->name('mail.attachment.store');

Route::get('/dashboard/email/m/{id}', 'Mail\MessageController@message')->name('mail.message');
Route::delete('/dashboard/email/m/{id}', 'Mail\MessageController@deleteMessage');
Route::put('/dashboard/email/m/{id}', 'Mail\MessageController@actionMessage');

Route::get('/dashboard/email/compose/{id?}', 'Mail\MessageController@compose')->name('mail.compose');
Route::post('/dashboard/email/draft', 'Mail\MessageController@draft')->name('mail.draft');
Route::post('/dashboard/email/send', 'Mail\MessageController@send')->name('mail.send');
Route::get('/dashboard/email/f/{folder}', 'Mail\MessageController@index')->name('mail.index');
Route::delete('/dashboard/email/f/{folder}', 'Mail\MessageController@delete');
Route::put('/dashboard/email/f/{folder}', 'Mail\MessageController@action');

// Schedule

Route::get('/dashboard/schedule', 'Schedule\ScheduleController@index')->name('schedule.index');
Route::post('/dashboard/schedule', 'Schedule\ScheduleController@loadAjax');
Route::get('/dashboard/schedule/my', 'Schedule\ScheduleController@mySchedule')->name('schedule.myschedule');
Route::post('/dashboard/schedule/my', 'Schedule\ScheduleController@loadAjaxMySchedule');
Route::get('/dashboard/schedule/lesson', 'Schedule\LessonController@index')->name('schedule.lesson.index');
Route::get('/dashboard/schedule/lesson/create', 'Schedule\LessonController@create')->name('schedule.lesson.create');
Route::post('/dashboard/schedule/lesson/create', 'Schedule\LessonController@store');
Route::get('/dashboard/schedule/lesson/{id}/edit', 'Schedule\LessonController@edit')->name('schedule.lesson.edit');
Route::put('/dashboard/schedule/lesson/{id}/edit', 'Schedule\LessonController@update');
Route::delete('/dashboard/schedule/lesson/{id}', 'Schedule\LessonController@destroy')->name('schedule.lesson.destroy');


Route::get('/dashboard/calendar', 'CalendarController@index')->name('calendar');
Route::post('/dashboard/calendar', 'CalendarController@events');
Route::get('/dashboard/calendar/create', 'CalendarController@create')->name('calendar.create');
Route::post('/dashboard/calendar/create', 'CalendarController@store');

Route::get('/dashboard/calendar/editmode', 'CalendarController@toggleEditMode')->name('calendar.edit_mode');

Route::get('/dashboard/calendar/edit', 'CalendarController@edit')->name('calendar.edit');
Route::put('/dashboard/calendar/{id}', 'CalendarController@update')->name('calendar.update');
Route::post('/dashboard/calendar/{id}', 'CalendarController@show')->name('calendar.show');
Route::delete('/dashboard/calendar/{id}', 'CalendarController@destroy')->name('calendar.destroy');

// Remote learning

Route::get('/dashboard/remotelearning', 'RemoteLearning\RemoteLearningController@index')->name('remotelearning.index');
Route::post('/dashboard/remotelearning', 'RemoteLearning\RemoteLearningController@controller');
Route::post('/dashboard/remotelearning/records', 'RemoteLearning\RemoteLearningController@records');
Route::post('/dashboard/remotelearning/records/modal', 'RemoteLearning\RemoteLearningController@recordModal')->name('remotelearning.record');
Route::post('/dashboard/remotelearning/checkifstarted', 'RemoteLearning\RemoteLearningController@checkIfStreamStarted')->name('remotelearning.status');

// License

Route::get('/dashboard/license', 'License\LicenseController@index')->name('license.index');
Route::get('/dashboard/license/create', 'License\LicenseController@create')->name('license.create');
Route::post('/dashboard/license/create', 'License\LicenseController@store');
Route::get('/dashboard/license/{id}/edit', 'License\LicenseController@edit')->name('license.edit');
Route::put('/dashboard/license/{id}/edit', 'License\LicenseController@update');
Route::delete('/dashboard/license/{id}', 'License\LicenseController@destroy')->name('license.destroy');
Route::post('/dashboard/license/{id}/extend', 'License\LicenseController@extend')->name('license.extend');

//// License Type

Route::get('/dashboard/license/type', 'License\LicenseTypeController@index')->name('license.type.index');
Route::get('/dashboard/license/type/create', 'License\LicenseTypeController@create')->name('license.type.create');
Route::post('/dashboard/license/type/create', 'License\LicenseTypeController@store');
Route::get('/dashboard/license/type/{id}/edit', 'License\LicenseTypeController@edit')->name('license.type.edit');
Route::put('/dashboard/license/type/{id}/edit', 'License\LicenseTypeController@update');
Route::delete('/dashboard/license/type/{id}', 'License\LicenseTypeController@destroy')->name('license.type.destroy');

//// Legal info

Route::get('/dashboard/license/legalinfo', 'License\LegalInfoController@index')->name('license.legalinfo.index');
Route::get('/dashboard/license/legalinfo/create', 'License\LegalInfoController@create')->name('license.legalinfo.create');
Route::post('/dashboard/license/legalinfo/create', 'License\LegalInfoController@store');
Route::get('/dashboard/license/legalinfo/{id}/edit', 'License\LegalInfoController@edit')->name('license.legalinfo.edit');
Route::put('/dashboard/license/legalinfo/{id}/edit', 'License\LegalInfoController@update');
Route::delete('/dashboard/license/legalinfo/{id}', 'License\LegalInfoController@destroy')->name('license.legalinfo.destroy');
Route::post('/dashboard/license/{id}/toggle', 'License\LegalInfoController@toggle')->name('license.legalinfo.toggle');

// Projects

Route::get('/dashboard/project', 'Project\ProjectController@index')->name('project.index');
Route::get('/dashboard/project/create', 'Project\ProjectController@create')->name('project.create');
Route::post('/dashboard/project/create', 'Project\ProjectController@store');
Route::get('/dashboard/project/{id}/edit', 'Project\ProjectController@edit')->name('project.edit');
Route::put('/dashboard/project/{id}/edit', 'Project\ProjectController@update');
Route::delete('/dashboard/project/{id}', 'Project\ProjectController@destroy')->name('project.destroy');

//// Project Platform

Route::get('/dashboard/project/platform', 'Project\ProjectPlatformController@index')->name('project.platform.index');
Route::get('/dashboard/project/platform/create', 'Project\ProjectPlatformController@create')->name('project.platform.create');
Route::post('/dashboard/project/platform/create', 'Project\ProjectPlatformController@store');
Route::get('/dashboard/project/platform/{id}/edit', 'Project\ProjectPlatformController@edit')->name('project.platform.edit');
Route::put('/dashboard/project/platform/{id}/edit', 'Project\ProjectPlatformController@update');
Route::delete('/dashboard/project/platform/{id}', 'Project\ProjectPlatformController@destroy')->name('project.platform.destroy');

//// Project Tag

Route::get('/dashboard/project/tag', 'Project\ProjectTagController@index')->name('project.tag.index');
Route::get('/dashboard/project/tag/create', 'Project\ProjectTagController@create')->name('project.tag.create');
Route::post('/dashboard/project/tag/create', 'Project\ProjectTagController@store');
Route::get('/dashboard/project/tag/{id}/edit', 'Project\ProjectTagController@edit')->name('project.tag.edit');
Route::put('/dashboard/project/tag/{id}/edit', 'Project\ProjectTagController@update');
Route::get('/dashboard/project/tag/{id}', 'Project\ProjectTagController@show')->name('project.tag.show');
Route::post('/dashboard/project/tag/{id}', 'Project\ProjectTagController@participationRequest');
Route::delete('/dashboard/project/tag/{id}', 'Project\ProjectTagController@destroy')->name('project.tag.destroy');

// Email

Route::get('/dashboard/settings/email', 'Mail\SettingController@settings')->name('settings.mail');
Route::post('/dashboard/settings/email/add_domain', 'Mail\SettingController@addDomain')->name('settings.mail.add_domain');
Route::delete('/dashboard/settings/email/del_domain/{id}', 'Mail\SettingController@delDomain')->name('settings.mail.del_domain');

Route::get('/dashboard/settings/class', 'Settings\ClassController@index')->name('settings.class.index');
Route::get('/dashboard/settings/class/create', 'Settings\ClassController@create')->name('settings.class.create');
Route::post('/dashboard/settings/class/create', 'Settings\ClassController@store');
Route::get('/dashboard/settings/class/{id}/edit', 'Settings\ClassController@edit')->name('settings.class.edit');
Route::put('/dashboard/settings/class/{id}/edit', 'Settings\ClassController@update');
Route::delete('/dashboard/settings/class/{id}', 'Settings\ClassController@destroy')->name('settings.class.destroy');


Route::get('/dashboard/settings/classroom', 'Settings\ClassroomController@index')->name('settings.classroom.index');
Route::get('/dashboard/settings/classroom/create', 'Settings\ClassroomController@create')->name('settings.classroom.create');
Route::post('/dashboard/settings/classroom/create', 'Settings\ClassroomController@store');
Route::get('/dashboard/settings/classroom/{id}/edit', 'Settings\ClassroomController@edit')->name('settings.classroom.edit');
Route::put('/dashboard/settings/classroom/{id}/edit', 'Settings\ClassroomController@update');
Route::delete('/dashboard/settings/classroom/{id}', 'Settings\ClassroomController@destroy')->name('settings.classroom.destroy');


Route::get('/dashboard/settings/subject', 'Settings\SubjectController@index')->name('settings.subject.index');
Route::get('/dashboard/settings/subject/create', 'Settings\SubjectController@create')->name('settings.subject.create');
Route::post('/dashboard/settings/subject/create', 'Settings\SubjectController@store');
Route::get('/dashboard/settings/subject/{id}/edit', 'Settings\SubjectController@edit')->name('settings.subject.edit');
Route::put('/dashboard/settings/subject/{id}/edit', 'Settings\SubjectController@update');
Route::delete('/dashboard/settings/subject/{id}', 'Settings\SubjectController@destroy')->name('settings.subject.destroy');


Route::get('/dashboard/settings/design', 'Settings\DesignController@index')->name('settings.design');
Route::post('/dashboard/settings/design', 'Settings\DesignController@store');

// Achivements

Route::get('/dashboard/achivement', 'AchivementController@index')->name('achivement.index');

Route::get('/dashboard/settings/achivement', 'Settings\AchivementController@index')->name('settings.achivement.index');
Route::get('/dashboard/settings/achivement/create', 'Settings\AchivementController@create')->name('settings.achivement.create');
Route::post('/dashboard/settings/achivement/create', 'Settings\AchivementController@store');
Route::get('/dashboard/settings/achivement/{id}/edit', 'Settings\AchivementController@edit')->name('settings.achivement.edit');
Route::put('/dashboard/settings/achivement/{id}/edit', 'Settings\AchivementController@update');
Route::delete('/dashboard/settings/achivement/{id}', 'Settings\AchivementController@destroy')->name('settings.achivement.destroy');

//// Achivement groups

Route::get('/dashboard/settings/achivement/group/', 'Settings\AchivementGroupController@index')->name('settings.achivement.group.index');
Route::get('/dashboard/settings/achivement/group/create', 'Settings\AchivementGroupController@create')->name('settings.achivement.group.create');
Route::post('/dashboard/settings/achivement/group/create', 'Settings\AchivementGroupController@store');
Route::get('/dashboard/settings/achivement/group/{id}/edit', 'Settings\AchivementGroupController@edit')->name('settings.achivement.group.edit');
Route::put('/dashboard/settings/achivement/group/{id}/edit', 'Settings\AchivementGroupController@update');
Route::delete('/dashboard/settings/achivement/group/{id}', 'Settings\AchivementGroupController@destroy')->name('settings.achivement.group.destroy');

// Institution

Route::get('/dashboard/settings/institution', 'Settings\InstitutionController@index')->name('settings.institution.index');
Route::get('/dashboard/settings/institution/create', 'Settings\InstitutionController@create')->name('settings.institution.create');
Route::post('/dashboard/settings/institution/create', 'Settings\InstitutionController@store');
Route::get('/dashboard/settings/institution/{id}/edit', 'Settings\InstitutionController@edit')->name('settings.institution.edit');
Route::put('/dashboard/settings/institution/{id}/edit', 'Settings\InstitutionController@update');
Route::delete('/dashboard/settings/institution/{id}', 'Settings\InstitutionController@destroy')->name('settings.institution.destroy');

// Updates

Route::get('/dashboard/update', 'UpdateController@index')->name('update.index');
Route::get('/dashboard/update/create', 'UpdateController@create')->name('update.create');
Route::post('/dashboard/update/create', 'UpdateController@store');
Route::get('/dashboard/update/{id}/edit', 'UpdateController@edit')->name('update.edit');
Route::put('/dashboard/update/{id}/edit', 'UpdateController@update');
Route::delete('/dashboard/update/{id}', 'UpdateController@destroy')->name('update.destroy');

// Notifications

Route::delete('/notifications', 'NotificationController@clearAllNotifications')->name('notifications');

// Members

Route::get('/dashboard/members', 'MemberController@index')->name('members');

//// Favorites

Route::post('/dashboard/members/favorits', 'MemberController@favorite')->name('favorits');

// Auth

Auth::routes(['register' => false, 'reset' => false]);


Route::post('/update_activity', 'UserController@updateActivity');