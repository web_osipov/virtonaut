<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReferencesToProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("ALTER TABLE `projects` CHANGE `achivement_id` `achivement_id` bigint unsigned NOT NULL AFTER `status`;");

        Schema::table('projects', function (Blueprint $table) {
            $table->foreign('achivement_id')->references('id')->on('achivements')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
