<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstitutionStudyClassTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institution_study_class', function (Blueprint $table) {
            $table->bigInteger('institution_id', false, true);
            $table->bigInteger('study_class_id', false, true);
            $table->foreign('institution_id')
                ->references('id')
                ->on('institutions')
                ->onDelete('cascade');
            $table->foreign('study_class_id')
                ->references('id')
                ->on('study_classes')
                ->onDelete('cascade');
            $table->primary(['study_class_id', 'institution_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institution_study_class');
    }
}
