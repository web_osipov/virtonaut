<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLicenseLicenseTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('license_license_type', function (Blueprint $table) {
            $table->bigInteger('license_id', false, true);
            $table->bigInteger('license_type_id', false, true);
            $table->foreign('license_id')
                ->references('id')
                ->on('licenses')
                ->onDelete('cascade');
            $table->foreign('license_type_id')
                ->references('id')
                ->on('license_types')
                ->onDelete('cascade');
            $table->primary(['license_type_id', 'license_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('license_license_type');
    }
}
