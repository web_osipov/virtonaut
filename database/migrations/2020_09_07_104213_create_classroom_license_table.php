<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassroomLicenseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classroom_license', function (Blueprint $table) {
            $table->bigInteger('classroom_id', false, true);
            $table->bigInteger('license_id', false, true);
            $table->foreign('classroom_id')
                ->references('id')
                ->on('classrooms')
                ->onDelete('cascade');
            $table->foreign('license_id')
                ->references('id')
                ->on('licenses')
                ->onDelete('cascade');
            $table->timestamp('expires');
            $table->primary(['license_id', 'classroom_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classroom_license');
    }
}
