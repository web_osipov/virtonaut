<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupLegalInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_legal_info', function (Blueprint $table) {
            $table->bigInteger('legal_info_id', false, true);
            $table->bigInteger('group_id', false, true);
            $table->foreign('legal_info_id')
                ->references('id')
                ->on('legal_infos')
                ->onDelete('cascade');
            $table->foreign('group_id')
                ->references('id')
                ->on('groups')
                ->onDelete('cascade');
            $table->primary(['legal_info_id', 'group_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_legal_info');
    }
}
