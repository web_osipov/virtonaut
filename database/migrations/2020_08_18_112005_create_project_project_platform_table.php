<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectProjectPlatformTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_project_platform', function (Blueprint $table) {
            $table->bigInteger('project_id', false, true);
            $table->bigInteger('project_platform_id', false, true);
            $table->foreign('project_id')
                ->references('id')
                ->on('projects')
                ->onDelete('cascade');
            $table->foreign('project_platform_id')
                ->references('id')
                ->on('project_platforms')
                ->onDelete('cascade');
            $table->primary(['project_id', 'project_platform_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_project_platform');
    }
}
