<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecordingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recordings', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('classroom_id');
            $table->bigInteger('lesson_id');
            $table->integer('stream_class_process_id')->nullable();
            $table->integer('stream_board_process_id')->nullable();
            $table->integer('record_class_process_id')->nullable();
            $table->integer('record_board_process_id')->nullable();
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recordings');
    }
}
