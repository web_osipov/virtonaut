<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAchivementAchivementGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('achivement_achivement_group', function (Blueprint $table) {
            $table->bigInteger('achivement_id', false, true);
            $table->bigInteger('group_id', false, true);
            $table->foreign('achivement_id')
                ->references('id')
                ->on('achivements')
                ->onDelete('cascade');
            $table->foreign('group_id')
                ->references('id')
                ->on('achivement_groups')
                ->onDelete('cascade');
            $table->primary(['achivement_id', 'group_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('achivement_achivement_group');
    }
}
