<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReferencesToLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("ALTER TABLE `lessons` CHANGE `user_id` `user_id` bigint unsigned NOT NULL AFTER `id`, CHANGE `subject_id` `subject_id` bigint unsigned NOT NULL AFTER `user_id`, CHANGE `study_class_id` `study_class_id` bigint unsigned NOT NULL AFTER `subject_id`;");

        Schema::table('lessons', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('cascade');
            $table->foreign('study_class_id')->references('id')->on('study_classes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
