<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectProjectTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_project_tag', function (Blueprint $table) {
            $table->bigInteger('project_id', false, true);
            $table->bigInteger('project_tag_id', false, true);
            $table->foreign('project_id')
                ->references('id')
                ->on('projects')
                ->onDelete('cascade');
            $table->foreign('project_tag_id')
                ->references('id')
                ->on('project_tags')
                ->onDelete('cascade');
            $table->primary(['project_id', 'project_tag_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_project_tag');
    }
}
