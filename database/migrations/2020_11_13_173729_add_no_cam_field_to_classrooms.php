<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNoCamFieldToClassrooms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("ALTER TABLE `classrooms` CHANGE `class_cam` `class_cam` varchar(255) COLLATE 'utf8mb4_unicode_ci' NULL AFTER `name`, CHANGE `board_cam` `board_cam` varchar(255) COLLATE 'utf8mb4_unicode_ci' NULL AFTER `class_cam`;");
        Schema::table('classrooms', function (Blueprint $table) {
            $table->boolean('no_cam')->default(0)->after('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('classrooms', function (Blueprint $table) {
            $table->dropColumn('no_cam');
        });
    }
}
